library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# checking arguments return errors and warnings.
#-------------------------------------------------------------------------------
l <- list()
d <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))

assert(
  "mutate() errors with non-dataframes.",
  has_error(mutate(l))
)

assert(
  "mutate() warnings with missing x and returns data",
  identical(mutate(d), d)
)

#-------------------------------------------------------------------------------
# mutate errors if using bad evaluation function.
#-------------------------------------------------------------------------------
assert(
  "mutate() errors if using bad evaluation function.",
  has_error(mutate(d, mean = bad_fun(a)))
)

#-------------------------------------------------------------------------------
# mutate errors with wrong result size
#-------------------------------------------------------------------------------
assert(
  "mutate() errors with wrong result size.",
  has_error(mutate(mtcars, z = 1:2))
)

#-------------------------------------------------------------------------------
# mutate errors with wrong column names
#-------------------------------------------------------------------------------
y <- 3:4
df <- data.frame(x = c(1, 2))
df_env <- list2env(df, parent = emptyenv())
assert(
  "mutate() errors with wrong column name.",
  has_error(mutate(df, z = y, .frame = "y")),
  has_error(mutate(df, z = y, .env = df_env))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# mutate works with character vectors.
#-------------------------------------------------------------------------------
d1 <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6), c = c(-1, -2, -3))
d2 <- data.frame(
  a = c(1, 2, 3),
  b = c(4, 5, 6),
  c = c(-1, -2, -3),
  d = c(-4, -5, -6),
  e = c(8, 10, 12)
)
assert(
  "mutate() works with named dots.",
  identical(d1, mutate(d, c = -1*a)),
  identical(d2, mutate(d, c = -1*a, d = -1*b, e = 2*b))
)

#-------------------------------------------------------------------------------
# mutate works with lists.
#-------------------------------------------------------------------------------
d1 <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6), c = c(-1, -2, -3))
d2 <- data.frame(
  a = c(1, 2, 3),
  b = c(4, 5, 6),
  c = c(-1, -2, -3),
  d = c(-4, -5, -6),
  e = c(8, 10, 12)
)
assert(
  "mutate() works with unnamed dots.",
  identical(-1 * d$a, mutate(d, -1*a)$`-1 * a`),
  identical(-1 * d$b, mutate(d, -1*a, -1*b, 2*b)$`-1 * b`)
)

#-------------------------------------------------------------------------------
# mutate CANNOT refer to variables just created if SAME name.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1)
assert(
  "mutate() CANNOT refer to variables just created if SAME name.",
  has_error(mutate(df, z = x + 4, z = x + 1))
)

#-------------------------------------------------------------------------------
# mutate CAN refer to variables just created if DIFFERENT name.
#-------------------------------------------------------------------------------
res <- mutate(mtcars, cyl1 = cyl + 1, cyl2 = cyl1 + 1)
assert(
  "mutate() CAN refer to variables just created if DIFFERENT name.",
  identical(res$cyl2, mtcars$cyl + 2)
)

#-------------------------------------------------------------------------------
# mutate CAN refer to variables just created if DIFFERENT name (group_df).
#-------------------------------------------------------------------------------
gmtcars <- group_by(mtcars, "am")
res <- mutate(gmtcars, cyl1 = cyl + 1, cyl2 = cyl1 + 1)
res <- group_by(res, "am")
res_direct <- mutate(gmtcars, cyl2 = cyl + 2)
assert(
  "mutate() CAN refer to variables just created if DIFFERENT name (group_df).",
  identical(res$cyl2, res_direct$cyl2)
)

#-------------------------------------------------------------------------------
# Two mutates are equivalent to one.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1:10, y = 6:15)
df1 <- mutate(df, x2 = x * 2, y4 = y * 4)
df2 <- mutate(df, x2 = x * 2)
df2 <- mutate(df2, y4 = y * 4)
assert(
  "Two mutate()s are equivalent to one.",
  identical(df1, df2)
)

#-------------------------------------------------------------------------------
# mutate handles logical result
#-------------------------------------------------------------------------------
df <- data.frame(x = 1:10, g = rep(c(1, 2), each = 5))
res <- mutate(group_by(df, "g"), r = x > 5)
assert(
  "mutate() handles logical result.",
  identical(res$r, c(F, F, F, F, F, T, T, T, T, T))
)

#-------------------------------------------------------------------------------
# mutate can rename variables
#-------------------------------------------------------------------------------
out <- mutate(mtcars, cyl2 = cyl)
assert(
  "mutate() can rename variables.",
  identical(out$cyl2, mtcars$cyl)
)

#-------------------------------------------------------------------------------
# mutate recycles output of length one.
#-------------------------------------------------------------------------------
out1 <- mutate(mtcars, z = 1)
out2 <- mutate(mtcars, z = length(mpg))
out3 <- mutate(mtcars, z = TRUE)
out4 <- mutate(mtcars, z = 'abc')
assert(
  "mutate() recycles output of length one.",
  identical(out1$z, rep(1, nrow(mtcars))),
  identical(out2$z, rep(32L, nrow(mtcars))),
  identical(out3$z, rep(TRUE, nrow(mtcars))),
  identical(out4$z, rep("abc", nrow(mtcars)))
)

#-------------------------------------------------------------------------------
# mutate works with zero row data frames.
#-------------------------------------------------------------------------------
df <- data.frame(a = numeric(0), b = character(0))
res <- mutate(df, a2 = a * 2)
assert(
  "mutate() works with zero row data frames.",
  identical(names(res), c("a", "b", "a2"))
)

#-------------------------------------------------------------------------------
# mutate works with frame and env
#-------------------------------------------------------------------------------
df <- data.frame(x = c(1, 2))
df_env <- list2env(df, parent = emptyenv())
match <- structure(list(x = c(1, 2), z = c(1, 2)), .Names = c("x", "z"
), row.names = c(NA, -2L), class = "data.frame")
assert(
  "mutate() works with frame and env.",
  identical(mutate(df, z = x, .frame = "x"), match),
  identical(mutate(df, z = x, .env = df_env), match)
)
