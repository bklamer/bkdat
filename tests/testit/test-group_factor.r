library(testit)

c1 <- c("a", "b")
c2 <- c("b", "a")
i1 <- 1:2
i2 <- 2:1

assert(
  "group_factor() works as expected.",
  identical(group_factor(c1, arrange = TRUE), factor(c("a", "b"))),
  identical(group_factor(c2, arrange = FALSE), factor(c("b", "a"), levels = c("b", "a"))),
  identical(group_factor(i1, arrange = TRUE), factor(1:2)),
  identical(group_factor(i2, arrange = FALSE), factor(2:1, levels = 2:1))
)
