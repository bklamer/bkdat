library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1, 2),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1, 2))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "gather() errors with non-dataframes.",
  has_error(gather(list()))
)

assert(
  "gather() errors with non-character objects for key, value, and cols.",
  has_error(gather(data = df, key = df, value = "value", numeric, integer)),
  has_error(gather(data = df, key = "key", value = df, numeric, integer))#,
  #has_error(gather(data = df, key = "key", value = "value", df))
)

assert(
  "gather() errors with column names not found in data.",
  has_error(gather(data = df, key = "key", value = "value", numeric, zzz, yyy))
)

#-------------------------------------------------------------------------------
# gather warnings on list-columns.
#-------------------------------------------------------------------------------
# assert(
#   "gather() gives warning for a list column.",
#   has_warning(gather(df, key = "key", value = "value", cols = c("list", "POSIXct")))
# )

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# gather works correctly.
# Note: POSIXct and POSIXlt attribute "tzone" gets removed after gathering.
# I don't think this is a major problem? so Just check identical on cols 3:10.
#-------------------------------------------------------------------------------
df_manual_gather <- as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14", "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14", "2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", "2017-08-13", "2017-08-14")),
    character = rep(letters[1:2], 2),
    missing = rep(c(1, NA), 2),
    logical = rep(c(T, F), 2),
    factor = rep(factor(letters[1:2]), 2),
    list = replicate(4, list(a = data.frame(b = c(1, 2)))),
    key = c("numeric", "numeric", "integer", "integer"),
    value = c(1, 2, 3, 4)
  )
)
df_gathered <- gather(df, .key = "key", .value = "value", numeric, integer)

assert(
  "gather() works correctly with numeric/integer.",
  identical(
    df_manual_gather[, 3:10],
    df_gathered[, 3:10]
  )
)
