library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 2, 1),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# arrange errors if not a dataframe.
#-------------------------------------------------------------------------------
assert(
  "arrange() errors if not a dataframe.",
  has_error(arrange(list()))
)

#-------------------------------------------------------------------------------
# arrange error if missing 'dots'.
#-------------------------------------------------------------------------------
assert(
  "arrange() errors if missing 'dots'.",
  has_error(arrange(df))
)

#-------------------------------------------------------------------------------
# arrange doesnt have an error if sorting on duplicate column names.
#-------------------------------------------------------------------------------
df_dup1 <- data.frame(x = 4:1, x = 1:4, check.names = FALSE)
df_dup2 <- data.frame(x = 1:4, x = 1:4, y = 1:4, check.names = FALSE)
df_dup3 <- data.frame(x = 1:4, x = 1:4, y = 1:4, z = 1:4, check.names = FALSE)
df_dup4 <- data.frame(x = 1:4, x = 1:4, y = 1:4, y = 1:4, z = 1:4, check.names = FALSE)

assert(
  "arrange() orders on the first duplicate.",
  identical(arrange(df_dup1, x)[[1]], 1:4),
  identical(df_dup2, arrange(df_dup2, y)),
  identical(df_dup3, arrange(df_dup3, y, z)),
  identical(df_dup3, arrange(df_dup3, x, y)),
  identical(df_dup4, arrange(df_dup4, x, y))
)

#-------------------------------------------------------------------------------
# arrange errors when sorting on a list column.
#-------------------------------------------------------------------------------
assert(
  "arrange() errors when sorting on a list column.",
  has_error(arrange(df, list()))
)

#-------------------------------------------------------------------------------
# arrange has error if sorting on unknown column name.
#-------------------------------------------------------------------------------
assert(
  "arrange() has error if sorting on unknown column name.",
  has_error(arrange(df, z)),
  has_error(arrange(df, numeric, z))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# arrange sorts missing values to end.
#-------------------------------------------------------------------------------
na_last <- function(x) {
  n <- length(x)
  is.na(x[n])
}

assert(
  "arrange() sorts missing values to end.",
  na_last(arrange(df, POSIXct)$POSIXct),
  na_last(arrange(df, POSIXlt)$POSIXlt),
  na_last(arrange(df, Date)$Date),
  na_last(arrange(df, numeric)$numeric),
  na_last(arrange(df, integer)$integer),
  na_last(arrange(df, character)$character),
  na_last(arrange(df, infinite)$infinite),
  na_last(arrange(df, logical)$logical),
  na_last(arrange(df, factor)$factor)
)

assert(
  "arrange() sorts missing values to end with desc().",
  na_last(arrange(df, desc(POSIXct))$POSIXct),
  na_last(arrange(df, desc(POSIXlt))$POSIXlt),
  na_last(arrange(df, desc(Date))$Date),
  na_last(arrange(df, desc(numeric))$numeric),
  na_last(arrange(df, desc(integer))$integer),
  na_last(arrange(df, desc(character))$character),
  na_last(arrange(df, desc(infinite))$infinite),
  na_last(arrange(df, desc(logical))$logical),
  na_last(arrange(df, desc(factor))$factor)
)

#-------------------------------------------------------------------------------
# arrange sorts as expected.
#-------------------------------------------------------------------------------
assert(
  "arrange() sorts as expected.",
  identical(arrange(df, POSIXct)$POSIXct, as.POSIXct(c("2017-08-13 EDT", "2017-08-14 EDT", NA))),
  identical(arrange(df, POSIXlt)$POSIXlt, as.POSIXlt(c("2017-08-13 EDT", "2017-08-14 EDT", NA))),
  identical(arrange(df, Date)$Date, as.Date(c("2017-08-13 EDT", "2017-08-14 EDT", NA))),
  identical(arrange(df, numeric)$numeric, c(1, 2, NA)),
  identical(arrange(df, integer)$integer, c(3L, 4L, NA)),
  identical(arrange(df, character)$character, c("a", "b", NA)),
  identical(arrange(df, infinite)$infinite, c(1, Inf, NA)),
  identical(arrange(df, logical)$logical, c(FALSE, TRUE, NA)),
  identical(arrange(df, factor)$factor, factor(c("a", "b", NA)))
)

assert(
  "arrange() sorts as expected with desc().",
  identical(arrange(df, desc(POSIXct))$POSIXct, as.POSIXct(c("2017-08-14 EDT", "2017-08-13 EDT", NA))),
  identical(arrange(df, desc(POSIXlt))$POSIXlt, as.POSIXlt(c("2017-08-14 EDT", "2017-08-13 EDT", NA))),
  identical(arrange(df, desc(Date))$Date, as.Date(c("2017-08-14 EDT", "2017-08-13 EDT", NA))),
  identical(arrange(df, desc(numeric))$numeric, c(2, 1, NA)),
  identical(arrange(df, desc(integer))$integer, c(4L, 3L, NA)),
  identical(arrange(df, desc(character))$character, c("b", "a", NA)),
  identical(arrange(df, desc(infinite))$infinite, c(Inf, 1, NA)),
  identical(arrange(df, desc(logical))$logical, c(TRUE, FALSE, NA)),
  identical(arrange(df, desc(factor))$factor, factor(c("b", "a", NA)))
)

#-------------------------------------------------------------------------------
# Two arranges are equivalent to one arrange.
#-------------------------------------------------------------------------------
a1 <- arrange(df, numeric, integer)
a2 <- arrange(arrange(df, integer), numeric)

assert(
  "Two arrange() are equivalent to one arrange().",
  identical(a1, a2)
)

#-------------------------------------------------------------------------------
# arrange sorts dataframes containing list columns.
#-------------------------------------------------------------------------------
a <- arrange(df, numeric)
list <- list(list(c = data.frame(d = c(3, 4))), NA, list(a = data.frame(b = c(1, 2))))

assert(
  "arrange() sorts dataframes containing list columns.",
  identical(a$list, list)
)

#-------------------------------------------------------------------------------
# arrange handles 0-rows data frames.
#-------------------------------------------------------------------------------
df_empty <- data.frame()
df_empty_ <- data.frame(a = numeric(0))

assert(
  "arrange() handles 0 row data frames with colnames",
  identical(df_empty_, arrange(df_empty_, a))
)

# This should really be for empty dataframes being passed to arrange with
# no variables to arrange on...
# assert(
#   "arrange() handles empty data frames",
#   identical(nrow(arrange(df_empty, character(0))), 0L),
#   identical(length(arrange(df_empty, character(0))), 0L)
# )

#-------------------------------------------------------------------------------
# arrange handles complex vectors.
#-------------------------------------------------------------------------------
df_complex <- data.frame(x = 1:10, y = 10:1 + 2i)
res <- arrange(df_complex, y)
res2 <- arrange(df_complex, desc(y))

df_complex_2 <- df_complex
df_complex_2$y[c(3, 6)] <- NA
res3 <- arrange(df_complex_2, y)
res4 <- arrange(df_complex_2, desc(y))

assert(
  "arrange() handles complex vectors",
  identical(res$y, rev(df_complex$y)),
  identical(res$x, rev(df_complex$x)),
  identical(res2$y, df_complex$y),
  identical(res2$x, df_complex$x),
  all(is.na(res3$y[9:10])),
  all(is.na(res4$y[9:10]))
)

#-------------------------------------------------------------------------------
# arrange *does not* respect attributes because subsetting with [ drops attributes.
#-------------------------------------------------------------------------------
env <- environment()
Period <- suppressWarnings(setClass("Period", contains = "numeric", where = env))
df_attr <- data.frame(p = Period(c(1, 2, 3)), x = 1:3)

assert(
  "arrange() *does not* respect attributes",
  inherits(df_attr$p, "Period"),
  !inherits(arrange(df_attr, p)$p, "Period"),
  removeClass("Period", where = env)
)

#-------------------------------------------------------------------------------
# arrange respects locale.
#-------------------------------------------------------------------------------
df_locale <- data.frame(
  words = c("casa", "\u00e1rbol", "zona", "\u00f3rgano"),
  stringsAsFactors = FALSE
)

assert(
  "arrange() respects locale",
  identical(arrange(df_locale, words)$words, sort(df_locale$words)),
  identical(arrange(df_locale, desc(words))$words, sort(df_locale$words, decreasing = TRUE))
)

#-------------------------------------------------------------------------------
# arrange sorts descending correctly
#-------------------------------------------------------------------------------
df_desc <- data.frame(a = c(1, 2, 2), b = c(4, 6, 5))

assert(
  "arrange() sorts descending correctly.",
  identical(arrange(df, desc(integer), numeric)$integer, c(4L, 3L, NA)),
  identical(arrange(df, desc(integer), numeric)$numeric, c(1, NA, 2)),
  identical(arrange(df, integer, desc(numeric))$integer, c(3L, 4L, NA)),
  identical(arrange(df, integer, desc(numeric))$numeric, c(NA, 1, 2))
)

#-------------------------------------------------------------------------------
# Grouped arrange ignores group.
#-------------------------------------------------------------------------------
df <- data.frame(g = c(2, 1, 2, 1), x = c(4:1))
out_g <- group_by(df, "g")
out_g1 <- arrange(out_g, x)

assert(
  identical(out_g1$x, 1:4)
)

#-------------------------------------------------------------------------------
# Nested arrange works.
#-------------------------------------------------------------------------------
# df <- data.frame(g = c(2, 1, 2, 1), x = c(4:1))
# out_n <- nest_by(df, "g")
# out_n1 <- arrange(out_n, g)
#
# df2 <- nest_by(mtcars, group_cols = c("cyl", "carb"))
# out_mtcars1 <- arrange(df2, desc(disp))
#
# assert(
#   identical(out_n1$g, c(1, 2)),
#   identical(out_n1$data, list(data.frame(x = c(3L, 1L)), data.frame(x = c(4L, 2L)))),
#   identical(out_mtcars1$data[[1]]$disp, c(167.6, 167.6, 160.0, 160.0))
# )

#-------------------------------------------------------------------------------
# arrange keeps the grouping structure.
# (It does not in this case)
#-------------------------------------------------------------------------------
# test_that("arrange keeps the grouping structure", {
#   dat <- data.frame(g = c(2, 2, 1, 1), x = c(1, 3, 2, 4))
#   res <- dat %>% group_by(g) %>% arrange()
#   expect_is(res, "grouped_df")
#   expect_equal(res$x, dat$x)
#
#   res <- dat %>% group_by(g) %>% arrange(x)
#   expect_is(res, "grouped_df")
#   expect_equal(res$x, 1:4)
#   expect_equal(attr(res, "indices"), list(c(1, 3), c(0, 2)))
# })

#-------------------------------------------------------------------------------
# arrange by group works correctly.
#-------------------------------------------------------------------------------
df <- data.frame(g = c(1, 2), h = c(2, 1))
df <- group_by(df, "g")
df1 <- arrange(df, h, .by_group = TRUE)
df2 <- arrange(df, g, h)

assert(
  identical(df1, df2)
)
