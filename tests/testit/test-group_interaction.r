library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    #POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(T, NA, F),
    factor = factor(c(letters[1:2], NA))
    #list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)
df2 <- data.frame(
  a = c(3, 1, 1),
  b = c(4, 1, 1)
)

assert(
  "group_interaction() works as expected.",
  is.factor(group_interaction(df, arrange = TRUE)),
  identical(levels(group_interaction(df, arrange = TRUE))[1], "2017-08-13|2017-08-14|1|NA|b|1|NA|b"),
  identical(levels(group_interaction(df, arrange = FALSE))[1], "NA|2017-08-13|NA|3|a|NA|TRUE|a"),
  identical(levels(group_interaction(df2, arrange = TRUE))[1], "1|1"),
  identical(levels(group_interaction(df2, arrange = FALSE))[1], "3|4")
)
