library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# checking arguments return errors and warnings.
#-------------------------------------------------------------------------------
d <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))

assert(
  "filter_() errors on non-dataframes.",
  has_error(filter_(list()))
)

assert(
  "filter_() errors when 'x' is missing.",
  has_error(filter_(d))
)

# d$a exists, but is not a string. z does not exist.
assert(
  "filter_() errors when 'x' is not a string.",
  has_error(filter_(d, list(d$a))),
  has_error(filter_(d, z))
)

#-------------------------------------------------------------------------------
# filter_ errors with invalid columns.
#-------------------------------------------------------------------------------
assert(
  "filter_() errors with invalid columns.",
  has_error(filter_(d, "any(c > 0)")),
  has_error(filter_(d, "z"))
)

#-------------------------------------------------------------------------------
# filter_ errors with invalid evaluation function.
#-------------------------------------------------------------------------------
assert(
  "filter_() errors with invalid evaluation.",
  has_error(filter_(d, "mean(b)"))
)

#-------------------------------------------------------------------------------
# filter_ errors if input is wrong length.
#-------------------------------------------------------------------------------
df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
assert(
  "filter_() errors if input is wrong length.",
  has_error(filter_(df, "c(TRUE, TRUE)"))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# filter_ subsets as expected.
#-------------------------------------------------------------------------------
assert(
  "filter_() subsets as expecred.",
  identical(
    d[3, ],
    filter_(d, "a > 2")
    ),
  identical(
    d[c(1, 3), ],
    filter_(d, "a > 2 | b < 5")
  ),
  identical(
    d[3, ],
    filter_(d, "which(a == 3)")
  )
)

#-------------------------------------------------------------------------------
# filter_ drops NAs.
#-------------------------------------------------------------------------------
df <- data.frame(
  i = 1:5,
  x = c(NA, 1L, 1L, 0L, 0L)
)

assert(
  "filter_() drops NAs.",
  identical(nrow(filter_(df, "x == 1")), 2L)
)

#-------------------------------------------------------------------------------
# filter_ works with Dates.
#-------------------------------------------------------------------------------
x1 <- x2 <- data.frame(
  date = seq.Date(as.Date("2013-01-01"), by = "1 days", length.out = 2),
  var = c(5, 8)
)
x1.filter_ <- filter_(x1, "date > as.Date.character('2013-01-01')")
x2$date <- x2$date + 1
x2.filter_ <- filter_(x2, "date > as.Date.character('2013-01-01')")

assert(
  "filter_() works with Dates.",
  identical(class(x1.filter_$date), "Date"),
  identical(class(x2.filter_$date), "Date")
)

#-------------------------------------------------------------------------------
# filter_ group_by works as expected.
#-------------------------------------------------------------------------------
df <- group_by(mtcars, c("carb", "cyl"))

assert(
  "filter_() group_by works as expected.",
  identical(nrow(filter_(df, "any(mpg > 25)")), 11L),
  identical(nrow(filter_(mtcars, "any(mpg > 25)")), 32L)
)

#-------------------------------------------------------------------------------
# filter_ nest_by works as expected.
#-------------------------------------------------------------------------------
df <- nest_by(mtcars, c("carb", "cyl"))
res1 <- filter_(df, "any(mpg > 25)")

assert(
  "filter_() group_by works as expected.",
  identical(unlist(lapply(res1$data, nrow)), c(0L, 5L, 0L, 0L, 0L, 6L, 0L, 0L, 0L))
)

#===============================================================================
# numeric and logical version of filter_.
#===============================================================================
d <- as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1.399, 2.599),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    infinte = rep(c(2, Inf)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1.33, 2))))
  )
)

#-------------------------------------------------------------------------------
# Check errors and warnings.
#-------------------------------------------------------------------------------
assert(
  "filter_() errors with non-dataframes.",
  has_error(filter_(list()))
)

assert(
  "filter_() errors with unknown numeric positions.",
  has_error(filter_(d, 3)),
  has_error(filter_(d, -2:-6))
)

assert(
  "filter_() errors if 'x' is not a numeric or logical vector.",
  has_error(filter_(d, list()))
)

#-------------------------------------------------------------------------------
# filter_ works as expected.
#-------------------------------------------------------------------------------
assert(
  "filter_() works as expected.",
  identical(d[1, , drop = FALSE], filter_(d, 1)),
  identical(d[2, , drop = FALSE], filter_(d, 2)),
  identical(d[1:2, , drop = FALSE], filter_(d, 1:2)),
  identical(d[1, , drop = FALSE], filter_(d, c(TRUE, FALSE))),
  identical(d[2, , drop = FALSE], filter_(d, c(FALSE, TRUE))),
  identical(d[1:2, , drop = FALSE], filter_(d, c(TRUE, TRUE)))
)

assert(
  "filter_() allows length one logical vector.",
  identical(filter_(d, TRUE), d)
)
