library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# Spread with duplicate values for one key is an error
#-------------------------------------------------------------------------------
df <- data.frame(x = c("a", "b", "b"), y = c(1, 2, 2), z = c(1, 2, 2), stringsAsFactors = FALSE)
assert(
  "spread() with duplicate values for one key is an error.",
  has_error(spread(df, key = "x", value = "y"))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# Spread order doesn't matter.
#-------------------------------------------------------------------------------
df1 <- data.frame(x = c("a", "b"), y = 1:2)
df2 <- data.frame(x = c("b", "a"), y = 2:1)
one <- spread(df1, key = "x", value = "y")
two <- spread(df2, key = "x", value = "y")
two <- select(two, c("a", "b"))
two <- arrange_(two, c("a", "b"))
assert(
  "spread() order doesn't matter.",
  identical(one, two)
)

df1 <- data.frame(z = c("b", "a"), x = c("a", "b"), y = 1:2)
df2 <- data.frame(z = c("a", "b"), x = c("b", "a"), y = 2:1)
one <- spread(df1, key = "x", value = "y")
one <- arrange_(one, "z")
two <- spread(df2, key = "x", value = "y")
assert(
  "spread() order doesn't matter.",
  identical(one, two)
)

#-------------------------------------------------------------------------------
# Spread with convert = TRUE converts strings into integers
#-------------------------------------------------------------------------------
df <- data.frame(key = "a", value = "1", stringsAsFactors = FALSE)
out <- spread(df, key = "key", value = "value", convert = TRUE)
assert(
  "spread() with convert = TRUE converts strings into integers.",
  inherits(out$a, "integer")
)

#-------------------------------------------------------------------------------
# Spread spreads factors into columns
#-------------------------------------------------------------------------------
data <- data.frame(
  x = c("a", "a", "b", "b"),
  y = c("c", "d", "c", "d"),
  z = c("w", "x", "y", "z"),
  stringsAsFactors = TRUE
)
out <- spread(data, key = "x", value = "z")
assert(
  "spread() will spread factors into columns.",
  identical(names(out), c("y", "a", "b")),
  all(vapply(out, is.factor, logical(1))),
  identical(levels(out$a), levels(data$z)),
  identical(levels(out$b), levels(data$z))
)

#-------------------------------------------------------------------------------
# Spread with drop = F keeps missing combinations
#-------------------------------------------------------------------------------
df <- data.frame(
  x = factor("a", levels = c("a", "b")),
  y = factor("b", levels = c("a", "b")),
  z = 1
)
out <- spread(df, key = "x", value = "z", drop = FALSE)
assert(
  "spread() with drop = F keeps missing combinations",
  identical(nrow(out), 2L),
  identical(ncol(out), 3L),
  identical(out$a[2], 1)
)

#-------------------------------------------------------------------------------
# Spread with drop = F keeps missing combinations of 0-length factors
#-------------------------------------------------------------------------------
df <- data.frame(
  x = factor(, levels = c("a", "b")),
  y = factor(, levels = c("a", "b")),
  z = logical()
)
out <- spread(df, key = "x", value = "z", drop = FALSE)
assert(
  "spread() with drop = F keeps missing combinations of 0-length factors",
  identical(nrow(out), 2L),
  identical(ncol(out), 3L),
  identical(out$a, c(NA, NA)),
  identical(out$b, c(NA, NA))
)

#-------------------------------------------------------------------------------
# Spread spreads dates into columns.
#-------------------------------------------------------------------------------
df <- data.frame(
  id = c("a", "a", "b", "b"),
  key = c("begin", "end", "begin", "end"),
  date = Sys.Date() + 0:3,
  stringsAsFactors = FALSE
)
out <- spread(df, key = "key", value = "date")
assert(
  "spread() spreads dates into columns.",
  identical(names(out), c("id", "begin", "end")),
  inherits(out$begin, "Date"),
  inherits(out$end, "Date")
)

#-------------------------------------------------------------------------------
# Spread can produce mixed variable types
#-------------------------------------------------------------------------------
df <- data.frame(
  row = rep(1:2, 3),
  column = rep(1:3, each = 2),
  cell_contents = as.character(
    c(rep("Argentina", 2), 62.485, 64.399, 1952, 1957)
  ),
  stringsAsFactors = FALSE
)
out <- spread(df, key = "column", value = "cell_contents", convert = TRUE)
assert(
  "spread can produce mixed variable types",
  identical(
    vapply(out, class, FUN.VALUE = character(1), USE.NAMES = FALSE),
    c("integer", "character", "numeric", "integer")
  )
)

#-------------------------------------------------------------------------------
# Spread with factors can be used with convert = T to produce mixed types
#-------------------------------------------------------------------------------
df <- data.frame(
  row = c(1, 2, 1, 2, 1, 2),
  column = c("f", "f", "g", "g", "h", "h"),
  contents = c("aa", "bb", "1", "2", "TRUE", "FALSE"),
  stringsAsFactors = FALSE
)
out <- spread(df, key = "column", value = "contents", convert = TRUE)
assert(
  "spread() with factors can be used with convert = T to produce mixed types",
  inherits(out$f, "character"),
  inherits(out$g, "integer"),
  inherits(out$h, "logical")
)

#-------------------------------------------------------------------------------
# Spread works with Dates and convert = T
#-------------------------------------------------------------------------------
df <- data.frame(
  id = c("a", "a", "b", "b"),
  key = c("begin", "end", "begin", "end"),
  date = Sys.Date() + 0:3,
  stringsAsFactors = FALSE
)
out <- spread(df, key = "key", value = "date", convert = TRUE)
assert(
  "spread() works with Dates and convert = T",
  inherits(out$begin, "character"),
  inherits(out$end, "character")
)

#-------------------------------------------------------------------------------
# Spread with vars that are all NA are logical if convert = TRUE
#-------------------------------------------------------------------------------
df <- data.frame(
  row = c(1, 2, 1, 2),
  column = c("f", "f", "g", "g"),
  contents = c("aa", "bb", NA, NA),
  stringsAsFactors = FALSE
)
out <- spread(df, key = "column", value = "contents", convert = TRUE)
assert(
  "vars that are all NA are logical if convert = TRUE",
  inherits(out$g, "logical")
)

#-------------------------------------------------------------------------------
# Spread preserves complex values
#-------------------------------------------------------------------------------
df <- expand.grid(
  id = 1:2,
  key = letters[1:2],
  stringsAsFactors = TRUE
)
df <- mutate_(df, c(value = "1:4 + 1i"))
out1 <- spread(df, key = "key", value = "value", convert = FALSE)
out2 <- spread(df, key = "key", value = "value", convert = TRUE)
assert(
  "spread() preserves complex values",
  identical(out1$a, 1:2 + 1i),
  identical(out2$a, 1:2 + 1i),
  identical(out1$b, 3:4 + 1i),
  identical(out2$b, 3:4 + 1i)
)

#-------------------------------------------------------------------------------
# Spread works with nested columns
#-------------------------------------------------------------------------------
df <- as_df(
  list(
    x = c("a", "a"),
    y = 1:2,
    z = list(1:2, 3:5)
  )
)
out <- spread(df, key = "x", value = "y")
assert(
  "spread() works with nested columns",
  identical(out$a, 1:2),
  identical(out$z, df$z)
)

#-------------------------------------------------------------------------------
# Spread gives one row if there are no existing non-spread vars
#-------------------------------------------------------------------------------
df <- data.frame(
  key = c("a", "b", "c"),
  value = c(1, 2, 3),
  stringsAsFactors = FALSE
)
assert(
  "spread gives one row when no existing non-spread vars",
  all.equal(
    spread(df, key = "key", value = "value"),
    data.frame(a = 1, b = 2, c = 3),
    check.attributes = FALSE
  )
)

# #-------------------------------------------------------------------------------
# # Spread grouping variables are kept where possible
# # (They are not in this case)
# #-------------------------------------------------------------------------------
# df <- data.frame(x = 1:2, key = c("a", "b"), value = 1:2)
# out <- group_by(df, "x")
# out <- spread(out, key = "key", value = "value")
# df2 <- data.frame(key = c("a", "b"), value = 1:2)
# out2 <- group_by(df, "key")
# out2 <- spread(out2, key = "key", value = "value")
# test_that("spread() grouping vars are kept where possible", {
#   identical(groups(out), list(quote(x)))
#   identical(out, tibble(a = 1L, b = 2L))
# })

#-------------------------------------------------------------------------------
# Spread col names never contain NA
#-------------------------------------------------------------------------------
df <- data.frame(x = c(1, NA), y = 1:2, stringsAsFactors = FALSE)
df1 <- spread(df, key = "x", value = "y")
df2 <- spread(df, key = "x", value = "y", sep = "_")
assert(
  "spread() col names never contains NA",
  identical(names(df1), c("1", "<NA>")),
  identical(names(df2), c("x_1", "x_NA"))
)

#-------------------------------------------------------------------------------
# spreading empty data frame gives empty data frame
#-------------------------------------------------------------------------------
df <- data.frame(
  x = character(),
  y = numeric(),
  z = character(),
  stringsAsFactors = FALSE
)
rs <- spread(df, "x", "y")
df2 <- data.frame(
  x = character(),
  y = numeric(),
  stringsAsFactors = FALSE
)
rs2 <- spread(df2, "x", "y")
assert(
  "spread() empty dataframes gives empty dataframe",
  identical(nrow(rs), 0L),
  identical(names(rs), "z"),
  identical(nrow(rs2), 0L),
  identical(ncol(rs2), 0L)
)
