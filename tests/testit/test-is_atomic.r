library(testit)

df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 2, 1),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(T, NA, F),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

assert(
  "is_atomic() works as expected.",
  identical(
    vapply(df, is_atomic, FUN.VALUE = logical(1), USE.NAMES = FALSE),
    c(T, F, T, T, T, T, T, T, T, F)
  ),
  !is_atomic(matrix(1)),
  !is_atomic(list())
)
