library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "group_labels() errors on non-dataframes.",
  has_error(group_labels(list()))
)

assert(
  "group_labels() errors if group_cols is not character.",
  has_error(group_labels(data.frame(1), 1))
)


#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# Group_labels works as expected
#-------------------------------------------------------------------------------
df <- data.frame(
  a = 1:5,
  b = c("b", "b", "a", "a", "c"),
  c = c("d", "d", "e", "f", "g"),
  stringsAsFactors = FALSE
)

assert(
  "group_labels() works as expected.",
  identical(
    group_labels(df, "b", arrange = TRUE),
    data.frame(b = c("a", "b", "c"), stringsAsFactors = FALSE)
  ),
  identical(
    group_labels(df, c("b", "c"), arrange = TRUE),
    data.frame(
      b = c("a", "a", "b", "c"),
      c = c("e", "f", "d", "g"),
      stringsAsFactors = FALSE
    )
  ),
  identical(
    group_labels(df, "b", arrange = FALSE),
    data.frame(b = c("b", "a", "c"), stringsAsFactors = FALSE)
  ),
  identical(
    group_labels(df, c("b", "c"), arrange = FALSE),
    data.frame(
      b = c("b", "a", "a", "c"),
      c = c("d", "e", "f", "g"),
      stringsAsFactors = FALSE
    )
  )
)
