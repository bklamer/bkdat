library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# checking arguments return errors and warnings.
#-------------------------------------------------------------------------------
d <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))

assert(
  "filter() errors on non-dataframes.",
  has_error(filter(list()))
)

assert(
  "filter() errors when 'x' is missing.",
  has_error(filter(d))
)

assert(
  "filter() errors when 'x' is a string.",
  has_error(filter(d, "list(d$a)")),
  has_error(filter(d, "b > 5"))
)

#-------------------------------------------------------------------------------
# filter errors with invalid columns.
#-------------------------------------------------------------------------------
assert(
  "filter() errors with invalid columns.",
  has_error(filter(d, any(c > 0))),
  has_error(filter(d, z))
)

#-------------------------------------------------------------------------------
# filter errors with invalid evaluation function.
#-------------------------------------------------------------------------------
assert(
  "filter() errors with invalid evaluation.",
  has_error(filter(d, mean(b)))
)

#-------------------------------------------------------------------------------
# filter errors if input is wrong length.
#-------------------------------------------------------------------------------
df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
assert(
  "filter() errors if input is wrong length.",
  has_error(filter(df, c(TRUE, TRUE)))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# filter subsets as expected.
#-------------------------------------------------------------------------------
assert(
  "filter() subsets as expecred.",
  identical(
    d[3, ],
    filter(d, a > 2)
    ),
  identical(
    d[c(1, 3), ],
    filter(d, a > 2 | b < 5)
  ),
  identical(
    d[3, ],
    filter(d, which(a == 3))
  )
)

#-------------------------------------------------------------------------------
# filter drops NAs.
#-------------------------------------------------------------------------------
df <- data.frame(
  i = 1:5,
  x = c(NA, 1L, 1L, 0L, 0L)
)

assert(
  "filter() drops NAs.",
  identical(nrow(filter(df, x == 1)), 2L)
)

#-------------------------------------------------------------------------------
# filter works with Dates.
#-------------------------------------------------------------------------------
x1 <- x2 <- data.frame(
  date = seq.Date(as.Date("2013-01-01"), by = "1 days", length.out = 2),
  var = c(5, 8)
)
x1.filter <- filter(x1, date > as.Date.character('2013-01-01'))
x2$date <- x2$date + 1
x2.filter <- filter(x2, date > as.Date.character('2013-01-01'))

assert(
  "filter() works with Dates.",
  identical(class(x1.filter$date), "Date"),
  identical(class(x2.filter$date), "Date")
)

#-------------------------------------------------------------------------------
# filter group_by works as expected.
#-------------------------------------------------------------------------------
df <- group_by(mtcars, c("carb", "cyl"))

assert(
  "filter() group_by works as expected.",
  identical(nrow(filter(df, any(mpg > 25))), 11L),
  identical(nrow(filter(mtcars, any(mpg > 25))), 32L)
)

#-------------------------------------------------------------------------------
# filter nest_by works as expected.
#-------------------------------------------------------------------------------
df <- nest_by(mtcars, c("carb", "cyl"))
res1 <- filter(df, any(mpg > 25))

assert(
  "filter() group_by works as expected.",
  identical(unlist(lapply(res1$data, nrow)), c(0L, 5L, 0L, 0L, 0L, 6L, 0L, 0L, 0L))
)

#===============================================================================
# numeric and logical version of filter.
#===============================================================================
d <- as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1.399, 2.599),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    infinte = rep(c(2, Inf)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1.33, 2))))
  )
)

#-------------------------------------------------------------------------------
# Check errors and warnings.
#-------------------------------------------------------------------------------
assert(
  "filter() errors with non-dataframes.",
  has_error(filter(list()))
)

assert(
  "filter() errors with unknown numeric positions.",
  has_error(filter(d, 3))#,
  #has_error(filter(d, -2:-6))
)

# assert(
#   "filter() errors with wrong length of logical vector.",
#   has_error(filter(d, TRUE))
# )

assert(
  "filter() errors if 'x' is not a numeric or logical vector.",
  has_error(filter(d, list()))
)

#-------------------------------------------------------------------------------
# filter works as expected.
#-------------------------------------------------------------------------------
assert(
  "filter() works as expected.",
  identical(d[1, , drop = FALSE], filter(d, 1)),
  identical(d[2, , drop = FALSE], filter(d, 2)),
  identical(d[1:2, , drop = FALSE], filter(d, 1:2)),
  identical(d[1, , drop = FALSE], filter(d, c(TRUE, FALSE))),
  identical(d[2, , drop = FALSE], filter(d, c(FALSE, TRUE))),
  identical(d[1:2, , drop = FALSE], filter(d, c(TRUE, TRUE)))
)
