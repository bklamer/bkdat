library(testit)

df <- data.frame(x = "a", y = "b", stringsAsFactors = FALSE)
out <- unite(df, "z", c("x", "y"))
assert(
  "bkdat::unite() - unite pastes columns together & removes old col",
  identical(names(out), "z"),
  identical(out$z, "a_b")
)

df <- data.frame(x = "a", y = "b", stringsAsFactors = FALSE)
out <- unite(df, "x", c("x", "y"))
assert(
  "bkdat::unite() - unite does not remove new col in case of name clash",
  identical(names(out), "x"),
  identical(out$x, "a_b")
)

# df <- group_by(data.frame(g = 1, x = "a", stringsAsFactors = FALSE), "g")
# rs <- unite(df, "x", "x")
# assert(
#   "bkdat::unite() - unite preserves grouping",
#   identical(df, rs),
#   identical(class(df), class(rs))
# )

df <- group_by(data.frame(g = 1, x = "a", stringsAsFactors = FALSE), "g")
rs <- unite(df, "gx", c("g", "x"))
assert(
  "bkdat::unite() - drops grouping when needed",
  identical(rs$gx, "1_a")
)

df <- data.frame(x = "a", y = "b", stringsAsFactors = FALSE)
assert(
  "bkdat::unite() - empty var has error",
  has_error(unite(df, "z"))
)
