library(testit)

df1 <- data.frame(a = c(1, 1))
df2 <- data.frame(a = c(1, 2))
df3 <- data.frame(a = c(1, 1), b = c(2, 3))
df4 <- data.frame(a = c(1, 1), b = c(2, 2))

assert(
  "group_unique_df() works as expected.",
  identical(group_unique_df(df1), data.frame(a = 1)),
  identical(group_unique_df(df2), data.frame(a = c(1, 2))),
  identical(group_unique_df(df3), data.frame(a = c(1, 1), b = c(2, 3))),
  identical(group_unique_df(df4), data.frame(a = c(1), b = c(2)))
)
