library(testit)

assert(
  "pretty_print() truncates long errors",
  identical(pretty_print(letters, max = 3), "\"'a', 'b', 'c', ...\"")
)
