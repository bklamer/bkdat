library(testit)

df_all <- data.frame(
  a = c(1, 2.5, NA),
  b = c(1:2, NA),
  c = c(T, F, NA),
  d = c("a", "b", NA),
  e = factor(c("a", "b", NA)),
  f = as.Date("2015-12-09") + c(1:2, NA),
  g = as.POSIXct("2015-12-09 10:51:34 UTC") + c(1:2, NA),
  stringsAsFactors = FALSE
)

df_all_new <- add_row(df_all, list(a = 4, b = 3L))
assert(
  "add_row() can add new row",
  identical(colnames(df_all_new), colnames(df_all)),
  identical(nrow(df_all_new), nrow(df_all) + 1L),
  identical(df_all_new$a, c(df_all$a, 4)),
  identical(df_all_new$b, c(df_all$b, 3L)),
  identical(df_all_new$c, c(df_all$c, NA))
)

iris_new <- add_row(iris, list(Species = "unknown"))
assert(
  "add_row() keeps class of object",
  identical(class(iris), class(iris_new))
)

iris_new <- add_row(iris, list(Species = "unknown"), after = 10)
assert(
  "add_row() keeps class of object when adding in the middle",
  identical(class(iris), class(iris_new))
)

iris_new <- add_row(iris, list(Species = "unknown"), after = 0)
assert(
  "add_row() keeps class of object when adding in the beginning",
  identical(class(iris), class(iris_new))
)

assert(
  "add_row() errors if no arguments",
  has_error(add_row(iris))
)

assert(
  "add_row() error if adding row with unknown variables",
  has_error(add_row(data.frame(a = 3), list(xxyzy = "err"))),
  has_error(add_row(data.frame(a = 3), list(b = "err", c = "oops")))
)

df <- data.frame(a = 3L)
df_new <- add_row(df, list(a = 4:5))
assert(
  "add_row() can add multiple rows",
  identical(df_new, data.frame(a = 3:5))
)

iris_new <- add_row(iris, list(Sepal.Length = -1:-2, Species = "unknown"))
assert(
  "add_row() can recycle when adding rows",
  identical(nrow(iris_new), nrow(iris) + 2L),
  identical(iris_new$Sepal.Length, c(iris$Sepal.Length, -1:-2)),
  identical(
    as.character(iris_new$Species),
    c(as.character(iris$Species), "unknown", "unknown")
  )
)

df <- data.frame(a = 3L)
df_new <- add_row(df, list(a = 2L), before = 1)
assert(
  "add_row() can add as first row via before = 1",
  identical(df_new, data.frame(a = 2:3))
)

df <- data.frame(a = 3L)
df_new <- add_row(df, list(a = 2L), after = 0)
assert(
  "add_row() can add as first row via after = 0",
  identical(df_new, data.frame(a = 2:3))
)

df <- data.frame(a = 1:3)
df_new <- add_row(df, list(a = 4:5), after = 2)
assert(
  "add_row() can add row inbetween",
  identical(df_new, data.frame(a = c(1:2, 4:5, 3L)))
)

df <- data.frame(a = factor(letters[1:3]))
assert(
  "add_row() can safely add to factor columns everywhere",
  identical(add_row(df, list(a = "d")), data.frame(a = factor(c(letters[1:4])))),
  identical(add_row(df, list(a = "d"), before = 1), data.frame(a = factor(c("d", letters[1:3])))),
  identical(add_row(df, list(a = "d"), before = 2), data.frame(a = factor(c("a", "d", letters[2:3]))))
)

df <- data.frame(a = 1:3)
assert(
  "add_row() error if both before and after are given",
  has_error(add_row(df, list(a = 4:5), after = 2, before = 3))
)

assert(
  "add_row() missing row names stay missing when adding row",
  !(.row_names_info(iris) > 0),
  !(.row_names_info(add_row(iris, list(Species = "unknown"), after = 0)) > 0),
  !(.row_names_info(add_row(iris, list(Species = "unknown"), after = nrow(iris))) > 0),
  !(.row_names_info(add_row(iris, list(Species = "unknown"), before = 10)) > 0)
)

assert(
  "add_row() adding to a list column adds a NULL value",
  is.null(add_row(data.frame(a = as.list(1:3), b = 1:3), list(b = 4:6))$a[[5]])
)

df_empty <- data.frame(
  a = integer(0),
  b = double(0),
  c = logical(0),
  d = character(0),
  e = factor(integer(0)),
  f = as.Date(character(0)),
  g = as.POSIXct(character(0)),
  h = as.list(double(0)),
  to_be_added = double(0),
  stringsAsFactors = FALSE
)
new_df <- add_row(df_empty, list(to_be_added = 5))
assert(
  "add_row() keeps the class of empty columns",
  identical(sapply(df_empty, class), sapply(new_df, class))
)

assert(
  "add_row() fails nicely for grouped data frames",
  has_error(add_row(group_by(iris, "Species"), list(Petal.Width = 3)))
)
