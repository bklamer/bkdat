library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(T, NA, F),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "nest_by() errors if 'data' is non-dataframe.",
  has_error(nest_by(list()))
)

assert(
  "nest_by() errors if 'group_cols' is non-character.",
  has_error(nest_by(data.frame(), 1))
)

assert(
  "nest_by() errors if 'nest_cols' is non-character.",
  has_error(nest_by(data.frame(), "a", 1))
)

assert(
  "nest_by() errors if 'nest_cols' is non-string.",
  has_error(nest_by(data.frame(), "a", "a", c("a", "b"))),
  has_error(nest_by(data.frame(), "a", "a", 1))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# nest_by works as expected.
#-------------------------------------------------------------------------------
for(var in names(df)) {
  assert(
    "nest_by() works as expected.",
    nrow(nest_by(df, var)) == 3 && ncol(nest_by(df, var)) == 2,
    ncol(nest_by(df, "integer")$data[[1]]) == 9
  )
}

#-------------------------------------------------------------------------------
# nest_by only includes columns specified in argument nest_cols.
#-------------------------------------------------------------------------------
for(var in names(df)[!(names(df) %in% c("integer", "factor"))]) {
  assert(
    "nest_by() only includes columns specified in argument nest_cols.",
    typeof(nest_by(df, var, c("integer", "factor"))$data[[1]]$integer) == "integer",
    class(nest_by(df, var, c("integer", "factor"))$data[[1]]$factor) == "factor",
    ncol(nest_by(df, var, c("integer", "factor"))$data[[1]]) == 2
  )
}

#-------------------------------------------------------------------------------
# nest_by argument nest_name works.
#-------------------------------------------------------------------------------
assert(
  "nest_by() argument nest_name works.",
  names(nest_by(df, "integer", nest_name = "test"))[2] == "test"
)

#-------------------------------------------------------------------------------
# nest_by attributes are set as expected.
#-------------------------------------------------------------------------------
at <- attributes(nest_by(df, "integer"))
assert(
  "nest_by() attributes are set as expected.",
  identical(
    at$nest_cols,
    c("POSIXct", "POSIXlt", "Date", "numeric", "character", "infinite",
      "logical", "factor", "list")
  ),
  identical(at$nest_name, "data"),
  identical(at$group_cols, "integer"),
  identical(at$nest_nrow, c(1L, 1L, 1L)),
  identical(at$group_labels, data.frame(integer = c(3L, NA, 4L)))
)
