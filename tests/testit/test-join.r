library(testit)

# Univariate keys --------------------------------------------------------------

a <- data.frame(x = c(1, 1, 2, 3), y = 1:4)
b <- data.frame(x = c(1, 2, 2, 4), z = 1:4)

ijm <- bkdat:::join_inner_merge(a, b, "x")
ijp <- bkdat:::join_inner_plyr(a, b, "x")
assert(
  "join_inner() - univariate inner join has all columns, repeated matching rows",
  identical(names(ijm), c("x", "y", "z")),
  identical(ijm$y, c(1L, 2L, 3L, 3L)),
  identical(ijm$z, c(1L, 1L, 2L, 3L)),
  identical(names(ijp), c("x", "y", "z")),
  identical(ijp$y, c(1L, 2L, 3L, 3L)),
  identical(ijp$z, c(1L, 1L, 2L, 3L))
)

ljm1 <- bkdat:::join_left_merge(a, b, "x")
ljm2 <- bkdat:::join_left_merge(b, a, "x")
ljp1 <- bkdat:::join_left_plyr(a, b, "x")
ljp2 <- bkdat:::join_left_plyr(b, a, "x")
assert(
  "join_left() - univariate left join has all columns, all rows",
  identical(names(ljm1), c("x", "y", "z")),
  identical(names(ljm2), c("x", "z", "y")),
  identical(ljm1$z, c(1L, 1L, 2L, 3L, NA)),
  identical(ljm2$y, c(1L, 2L, 3L, 3L, NA)),
  identical(names(ljp1), c("x", "y", "z")),
  identical(names(ljp2), c("x", "z", "y")),
  identical(ljp1$z, c(1L, 1L, 2L, 3L, NA)),
  identical(ljp2$y, c(1L, 2L, 3L, 3L, NA))
)

rjm1 <- bkdat:::join_right_merge(a, b, "x")
rjm2 <- bkdat:::join_right_merge(b, a, "x")
rjp1 <- bkdat:::join_right_plyr(a, b, "x")
rjp2 <- bkdat:::join_right_plyr(b, a, "x")
assert(
  "join_right() - univariate right join has all columns, all rows",
  identical(names(rjm1), c("x", "y", "z")),
  identical(names(rjm2), c("x", "z", "y")),
  identical(rjm1$x, c(1, 1, 2, 2, 4)),
  identical(rjm1$y, c(1L, 2L, 3L, 3L, NA)),
  identical(rjm1$z, c(1L, 1L, 2L, 3L, 4L)),
  identical(rjm2$x, c(1, 1, 2, 2, 3)),
  identical(rjm2$y, c(1L, 2L, 3L, 3L, 4L)),
  identical(rjm2$z, c(1L, 1L, 2L, 3L, NA)),
  identical(names(rjp1), c("x", "y", "z")),
  identical(names(rjp2), c("x", "z", "y")),
  identical(rjp1$x, c(1, 1, 2, 2, 4)),
  identical(rjp1$y, c(1L, 2L, 3L, 3L, NA)),
  identical(rjp1$z, c(1L, 1L, 2L, 3L, 4L)),
  identical(rjp2$x, c(1, 1, 2, 2, 3)),
  identical(rjp2$y, c(1L, 2L, 3L, 3L, 4L)),
  identical(rjp2$z, c(1L, 1L, 2L, 3L, NA))
)

# j1 <- semi_join(a, b, "x")
# j2 <- semi_join(b, a, "x")
# assert(
#   "join_*() - univariate semi join has x columns, matching rows",
#   identical(names(j1), c("x", "y")),
#   identical(names(j2), c("x", "z")),
#   identical(j1$y, 1:3),
#   identical(j2$z, 1:3)
# )

# j1 <- anti_join(a, b, "x")
# j2 <- anti_join(b, a, "x")
# assert(
#   "join_*() - univariate anti join has x columns, missing rows",
#   identical(names(j1), c("x", "y")),
#   identical(names(j2), c("x", "z")),
#   identical(j1$y, 4L),
#   identical(j2$z, 4L)
# )

# Bivariate keys ---------------------------------------------------------------

c <- data.frame(
  x = c(1, 1, 2, 3),
  y = c(1, 1, 2, 3),
  a = 1:4
)
d <- data.frame(
  x = c(1, 2, 2, 4),
  y = c(1, 2, 2, 4),
  b = 1:4
)

ijm <- bkdat:::join_inner_merge(c, d, c("x", "y"))
ijp <- bkdat:::join_inner_plyr(c, d, c("x", "y"))
assert(
  "join_inner() - bivariate inner join has all columns, repeated matching rows",
  identical(names(ijm), c("x", "y", "a", "b")),
  identical(ijm$a, c(1L, 2L, 3L, 3L)),
  identical(ijm$b, c(1L, 1L, 2L, 3L)),
  identical(names(ijp), c("x", "y", "a", "b")),
  identical(ijp$a, c(1L, 2L, 3L, 3L)),
  identical(ijp$b, c(1L, 1L, 2L, 3L))
)

ljm1 <- bkdat:::join_left_merge(c, d, c("x", "y"))
ljm2 <- bkdat:::join_left_merge(d, c, c("x", "y"))
ljp1 <- bkdat:::join_left_plyr(c, d, c("x", "y"))
ljp2 <- bkdat:::join_left_plyr(d, c, c("x", "y"))
assert(
  "join_*() - bivariate left join has all columns, all rows",
  identical(names(ljm1), c("x", "y", "a", "b")),
  identical(names(ljm2), c("x", "y", "b", "a")),
  identical(ljm1$b, c(1L, 1L, 2L, 3L, NA)),
  identical(ljm2$a, c(1L, 2L, 3L, 3L, NA)),
  identical(names(ljp1), c("x", "y", "a", "b")),
  identical(names(ljp2), c("x", "y", "b", "a")),
  identical(ljp1$b, c(1L, 1L, 2L, 3L, NA)),
  identical(ljp2$a, c(1L, 2L, 3L, 3L, NA))
)

rjm1 <- bkdat:::join_right_merge(c, d, c("x", "y"))
rjm2 <- bkdat:::join_right_merge(d, c, c("x", "y"))
rjp1 <- bkdat:::join_right_plyr(c, d, c("x", "y"))
rjp2 <- bkdat:::join_right_plyr(d, c, c("x", "y"))
assert(
  "join_*() - bivariate left join has all columns, all rows",
  identical(names(rjm1), c("x", "y", "a", "b")),
  identical(names(rjm2), c("x", "y", "b", "a")),
  identical(rjm1$b, c(1L, 1L, 2L, 3L, 4L)),
  identical(rjm1$a, c(1L, 2L, 3L, 3L, NA)),
  identical(rjm2$b, c(1L, 1L, 2L, 3L, NA)),
  identical(rjm2$a, c(1L, 2L, 3L, 3L, 4L)),
  identical(names(rjp1), c("x", "y", "a", "b")),
  identical(names(rjp2), c("x", "y", "b", "a")),
  identical(rjp1$b, c(1L, 1L, 2L, 3L, 4L)),
  identical(rjp1$a, c(1L, 2L, 3L, 3L, NA)),
  identical(rjp2$b, c(1L, 1L, 2L, 3L, NA)),
  identical(rjp2$a, c(1L, 2L, 3L, 3L, 4L))
)

# j1 <- semi_join(c, d, c("x", "y"))
# j2 <- semi_join(d, c, c("x", "y"))
# assert(
#   "join_*() - bivariate semi join has x columns, matching rows",
#   identical(names(j1), c("x", "y", "a")),
#   identical(names(j2), c("x", "y", "b")),
#   identical(j1$a, 1:3),
#   identical(j2$b, 1:3)
# )

# j1 <- anti_join(c, d, c("x", "y"))
# j2 <- anti_join(d, c, c("x", "y"))
# assert(
#   "join_*() - bivariate anti join has x columns, missing rows",
#   identical(names(j1), c("x", "y", "a")),
#   identical(names(j2), c("x", "y", "b")),
#   identical(j1$a, 4),
#   identical(j2$b, 4)
# )

# Duplicate column names --------------------------------------------------

e <- data.frame(x = c(1, 1, 2, 3), z = 1:4)
f <- data.frame(x = c(1, 2, 2, 4), z = 1:4)

ijm <- bkdat:::join_inner_merge(e, f, "x")
ijp <- bkdat:::join_inner_plyr(e, f, "x")
assert(
  "join_inner() - univariate inner join has all columns, repeated matching rows",
  identical(names(ijm), c("x", "z.x", "z.y")),
  identical(ijm$z.x, c(1L, 2L, 3L, 3L)),
  identical(ijm$z.y, c(1L, 1L, 2L, 3L)),
  identical(names(ijp), c("x", "z.x", "z.y")),
  identical(ijp$z.x, c(1L, 2L, 3L, 3L)),
  identical(ijp$z.y, c(1L, 1L, 2L, 3L))
)

ljm1 <- bkdat:::join_left_merge(e, f, "x")
ljm2 <- bkdat:::join_left_merge(f, e, "x")
ljp1 <- bkdat:::join_left_plyr(e, f, "x")
ljp2 <- bkdat:::join_left_plyr(f, e, "x")
assert(
  "join_left() - univariate left join has all columns, all rows",
  identical(names(ljm1), c("x", "z.x", "z.y")),
  identical(names(ljm2), c("x", "z.x", "z.y")),
  identical(ljm1$z.y, c(1L, 1L, 2L, 3L, NA)),
  identical(ljm2$z.y, c(1L, 2L, 3L, 3L, NA)),
  identical(names(ljm1), c("x", "z.x", "z.y")),
  identical(names(ljm2), c("x", "z.x", "z.y")),
  identical(ljm1$z.y, c(1L, 1L, 2L, 3L, NA)),
  identical(ljm2$z.y, c(1L, 2L, 3L, 3L, NA))
)

jm1 <- bkdat:::join_inner_merge(e, f, "x", suffix = c("1", "2"))
jm2 <- bkdat:::join_left_merge(e, f, "x", suffix = c("1", "2"))
jm3 <- bkdat:::join_right_merge(e, f, "x", suffix = c("1", "2"))
jm4 <- bkdat:::join_full_merge(e, f, "x", suffix = c("1", "2"))
jp1 <- bkdat:::join_inner_plyr(e, f, "x", suffix = c("1", "2"))
jp2 <- bkdat:::join_left_plyr(e, f, "x", suffix = c("1", "2"))
jp3 <- bkdat:::join_right_plyr(e, f, "x", suffix = c("1", "2"))
jp4 <- bkdat:::join_full_plyr(e, f, "x", suffix = c("1", "2"))
assert(
  "join_*() - can control suffixes with suffix argument",
  identical(names(jm1), c("x", "z1", "z2")),
  identical(names(jm2), c("x", "z1", "z2")),
  identical(names(jm3), c("x", "z1", "z2")),
  identical(names(jm4), c("x", "z1", "z2")),
  identical(names(jp1), c("x", "z1", "z2")),
  identical(names(jp2), c("x", "z1", "z2")),
  identical(names(jp3), c("x", "z1", "z2")),
  identical(names(jp4), c("x", "z1", "z2"))
)

jm1 <- bkdat:::join_inner_merge(e, f, "x", suffix = c("", "2"))
jm2 <- bkdat:::join_left_merge(e, f, "x", suffix = c("", "2"))
jm3 <- bkdat:::join_right_merge(e, f, "x", suffix = c("", "2"))
jm4 <- bkdat:::join_full_merge(e, f, "x", suffix = c("", "2"))
jp1 <- bkdat:::join_inner_plyr(e, f, "x", suffix = c("", "2"))
jp2 <- bkdat:::join_left_plyr(e, f, "x", suffix = c("", "2"))
jp3 <- bkdat:::join_right_plyr(e, f, "x", suffix = c("", "2"))
jp4 <- bkdat:::join_full_plyr(e, f, "x", suffix = c("", "2"))
assert(
  "join_*() - can handle empty string in suffix argument, left side",
  identical(names(jm1), c("x", "z", "z2")),
  identical(names(jm2), c("x", "z", "z2")),
  identical(names(jm3), c("x", "z", "z2")),
  identical(names(jm4), c("x", "z", "z2")),
  identical(names(jp1), c("x", "z", "z2")),
  identical(names(jp2), c("x", "z", "z2")),
  identical(names(jp3), c("x", "z", "z2")),
  identical(names(jp4), c("x", "z", "z2"))
)

jm1 <- bkdat:::join_inner_merge(e, f, "x", suffix = c("1", ""))
jm2 <- bkdat:::join_left_merge(e, f, "x", suffix = c("1", ""))
jm3 <- bkdat:::join_right_merge(e, f, "x", suffix = c("1", ""))
jm4 <- bkdat:::join_full_merge(e, f, "x", suffix = c("1", ""))
jp1 <- bkdat:::join_inner_plyr(e, f, "x", suffix = c("1", ""))
jp2 <- bkdat:::join_left_plyr(e, f, "x", suffix = c("1", ""))
jp3 <- bkdat:::join_right_plyr(e, f, "x", suffix = c("1", ""))
jp4 <- bkdat:::join_full_plyr(e, f, "x", suffix = c("1", ""))
assert(
  "join_*() - can handle empty string in suffix argument, right side",
  identical(names(jm1), c("x", "z1", "z")),
  identical(names(jm2), c("x", "z1", "z")),
  identical(names(jm3), c("x", "z1", "z")),
  identical(names(jm4), c("x", "z1", "z")),
  identical(names(jp1), c("x", "z1", "z")),
  identical(names(jp2), c("x", "z1", "z")),
  identical(names(jp3), c("x", "z1", "z")),
  identical(names(jp4), c("x", "z1", "z"))
)

assert(
  "join_*() - disallow empty string in both sides of suffix argument",
  has_error(bkdat:::join_inner_merge(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes"
  has_error(bkdat:::join_left_merge(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes"
  has_error(bkdat:::join_right_merge(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes"
  has_error(bkdat:::join_full_merge(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes",
  has_error(bkdat:::join_inner_plyr(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes"
  has_error(bkdat:::join_left_plyr(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes"
  has_error(bkdat:::join_right_plyr(e, f, "x", suffix = c("", ""))), # "`suffix` can't be empty string for both `x` and `y` suffixes"
  has_error(bkdat:::join_full_plyr(e, f, "x", suffix = c("", ""))) # "`suffix` can't be empty string for both `x` and `y` suffixes",
)

assert(
  "join_*() - disallow NA in any side of suffix argument",
  has_error(bkdat:::join_inner_merge(e, f, "x", suffix = c(".x", NA))), # "`suffix` can't be NA"
  has_error(bkdat:::join_left_merge(e, f, "x", suffix = c(NA, ".y"))), # "`suffix` can't be NA"
  has_error(bkdat:::join_right_merge(e, f, "x", suffix = c(NA_character_, NA))), # "`suffix` can't be NA"
  has_error(bkdat:::join_full_merge(e, f, "x", suffix = c("x", NA))), # "`suffix` can't be NA"
  has_error(bkdat:::join_inner_plyr(e, f, "x", suffix = c(".x", NA))), # "`suffix` can't be NA"
  has_error(bkdat:::join_left_plyr(e, f, "x", suffix = c(NA, ".y"))), # "`suffix` can't be NA"
  has_error(bkdat:::join_right_plyr(e, f, "x", suffix = c(NA_character_, NA))), # "`suffix` can't be NA"
  has_error(bkdat:::join_full_plyr(e, f, "x", suffix = c("x", NA))) # "`suffix` can't be NA"

)

jm1 <- bkdat:::join_inner_merge(e, f, by = c("x" = "z"))
jm2 <- bkdat:::join_left_merge(e, f, by = c("x" = "z"))
jm3 <- bkdat:::join_right_merge(e, f, by = c("x" = "z"))
jm4 <- bkdat:::join_full_merge(e, f, by = c("x" = "z"))
jp1 <- bkdat:::join_inner_plyr(e, f, by = c("x" = "z"))
jp2 <- bkdat:::join_left_plyr(e, f, by = c("x" = "z"))
jp3 <- bkdat:::join_right_plyr(e, f, by = c("x" = "z"))
jp4 <- bkdat:::join_full_plyr(e, f, by = c("x" = "z"))
assert(
  "join_*() - doesn't add suffix to by columns in x",
  identical(names(jm1), c("x", "z", "x.y")),
  identical(names(jm2), c("x", "z", "x.y")),
  identical(names(jm3), c("x", "z", "x.y")),
  identical(names(jm4), c("x", "z", "x.y")),
  identical(names(jp1), c("x", "z", "x.y")),
  identical(names(jp2), c("x", "z", "x.y")),
  identical(names(jp3), c("x", "z", "x.y")),
  identical(names(jp4), c("x", "z", "x.y"))
)

g <- data.frame(A = 1, A.x = 2)
h <- data.frame(B = 3, A.x = 4, A = 5)
jm1 <- bkdat:::join_inner_merge(g, h, "A.x")
jm2 <- bkdat:::join_left_merge(g, h, "A.x")
jm3 <- bkdat:::join_right_merge(g, h, "A.x")
jm4 <- bkdat:::join_full_merge(g, h, "A.x")
jp1 <- bkdat:::join_inner_plyr(g, h, "A.x")
jp2 <- bkdat:::join_left_plyr(g, h, "A.x")
jp3 <- bkdat:::join_right_plyr(g, h, "A.x")
jp4 <- bkdat:::join_full_plyr(g, h, "A.x")
assert(
  "join_*() - can handle 'by' columns with suffix",
  identical(names(jm1), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jm2), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jm3), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jm4), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jp1), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jp2), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jp3), c("A.x.x", "A.x", "B", "A.y")),
  identical(names(jp4), c("A.x.x", "A.x", "B", "A.y"))
)

jm1 <- bkdat:::join_inner_merge(h, g, "A.x")
jm2 <- bkdat:::join_left_merge(h, g, "A.x")
jm3 <- bkdat:::join_right_merge(h, g, "A.x")
jm4 <- bkdat:::join_full_merge(h, g, "A.x")
jp1 <- bkdat:::join_inner_plyr(h, g, "A.x")
jp2 <- bkdat:::join_left_plyr(h, g, "A.x")
jp3 <- bkdat:::join_right_plyr(h, g, "A.x")
jp4 <- bkdat:::join_full_plyr(h, g, "A.x")
assert(
  "join_*() - can handle 'by' columns with suffix, reverse",
  identical(names(jm1), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jm2), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jm3), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jm4), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jp1), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jp2), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jp3), c("B", "A.x", "A.x.x", "A.y")),
  identical(names(jp4), c("B", "A.x", "A.x.x", "A.y"))
)

assert(
  "join_*() - check suffix input",
  has_error(bkdat:::join_inner_merge(e, f, "x", suffix = letters[1:3])), # "`suffix` must be a character vector of length 2, not character of length 3"
  has_error(bkdat:::join_inner_merge(e, f, "x", suffix = letters[1])), # "`suffix` must be a character vector of length 2, not string of length 1"
  has_error(bkdat:::join_inner_merge(e, f, "x", suffix = 1:2)) # "`suffix` must be a character vector of length 2, not integer of length 2"
)


# Misc --------------------------------------------------------------------
a <- data.frame(x = c("p", "q", NA), y = c(1, 2, 3), stringsAsFactors = TRUE)
b <- data.frame(x = c("p", "q", "r"), z = c(4, 5, 6), stringsAsFactors = TRUE)
assert(
  "join_inner() - ignores joining on different levels of factors",
  identical(nrow(bkdat:::join_inner_merge(a, b, "x")), 2L),
  identical(nrow(bkdat:::join_inner_plyr(a, b, "x")), 2L)
)

a <- data.frame(a = 1:3)
b <- data.frame(a = 1:3, b = 1, c = 2, d = 3, e = 4, f = 5)
jm <- bkdat:::join_left_merge(a, b, "a")
jp <- bkdat:::join_left_plyr(a, b, "a")
assert(
  "join_left() - don't reorder columns",
  identical(names(jm), names(b)),
  identical(names(jp), names(b))
)

# df <- data.frame(
#   V1 = c(rep("a", 5), rep("b", 5)),
#   V2 = rep(c(1:5), 2),
#   V3 = c(101:110),
#   stringsAsFactors = FALSE
# )
# match <- data.frame(
#   V1 = c("a", "b"),
#   V2 = c(3.0, 4.0),
#   stringsAsFactors = FALSE
# )
# res <- semi_join(df, match, c("V1", "V2"))
# assert(
#   "join_*() - handles type promotions #123",
#   identical(res$V2, 3:4),
#   identical(res$V3, c(103L, 109L))
# )

a <- data.frame(V1 = c(0, 1, 2), V2 = c("a", "b", "c"), stringsAsFactors = FALSE)
b <- data.frame(V1 = c(0, 1), V3 = c("n", "m"), stringsAsFactors = FALSE)
ijm <- bkdat:::join_inner_merge(a, b, by = "V1")
ijp <- bkdat:::join_inner_plyr(a, b, by = "V1")
assert(
  "join_*() - indices don't get mixed up when nrow(x) > nrow(y).",
  identical(ijm$V1, c(0, 1)),
  identical(ijm$V2, c("a", "b")),
  identical(ijm$V3, c("n", "m")),
  identical(ijp$V1, c(0, 1)),
  identical(ijp$V2, c("a", "b")),
  identical(ijp$V3, c("n", "m"))
)

assert(
  "join_*() - functions error on column not found",
  has_error(bkdat:::join_left_merge(data.frame(x = 1:5), data.frame(y = 1:5), by = "x")), # "`by` can't contain join column `x` which is missing from RHS"
  has_error(bkdat:::join_left_merge(data.frame(x = 1:5), data.frame(y = 1:5), by = "y")), # "`by` can't contain join column `y` which is missing from LHS"
  has_error(bkdat:::join_left_merge(data.frame(x = 1:5), data.frame(y = 1:5))), # "`by` required, because the data sources have no common variables"
  has_error(bkdat:::join_left_merge(data.frame(x = 1:5), data.frame(y = 1:5), by = 1:3)), # "`by` must be a (named) character vector, list, or NULL for natural joins (not recommended in production code), not integer"
  has_error(bkdat:::join_left_plyr(data.frame(x = 1:5), data.frame(y = 1:5), by = "x")), # "`by` can't contain join column `x` which is missing from RHS"
  has_error(bkdat:::join_left_plyr(data.frame(x = 1:5), data.frame(y = 1:5), by = "y")), # "`by` can't contain join column `y` which is missing from LHS"
  has_error(bkdat:::join_left_plyr(data.frame(x = 1:5), data.frame(y = 1:5))), # "`by` required, because the data sources have no common variables"
  has_error(bkdat:::join_left_plyr(data.frame(x = 1:5), data.frame(y = 1:5), by = 1:3)) # "`by` must be a (named) character vector, list, or NULL for natural joins (not recommended in production code), not integer"
)

foo <- data.frame(id = factor(c("a", "b")), var1 = "foo", stringsAsFactors = FALSE)
bar <- data.frame(id = c("a", "b"), var2 = "bar", stringsAsFactors = FALSE)
# the following should really warn or error since the join by columns have different
# class (factor and character.)
ijm1 <- bkdat:::join_inner_merge(foo, bar, by = "id")
ijm2 <- bkdat:::join_inner_merge(bar, foo, by = "id")
ijp1 <- suppressWarnings(bkdat:::join_inner_plyr(foo, bar, by = "id"))
ijp2 <- suppressWarnings(bkdat:::join_inner_plyr(bar, foo, by = "id"))
assert(
  "join_inner() - is symmetric (even when joining on character & factor)",
  identical(class(ijm1$id), "factor"), # should be character
  identical(class(ijm2$id), "character"), # should be character
  identical(names(ijm1), c("id", "var1", "var2")),
  identical(names(ijm2), c("id", "var2", "var1")),
  has_warning(bkdat:::join_inner_plyr(foo, bar, by = "id")),
  has_warning(bkdat:::join_inner_plyr(bar, foo, by = "id")),
  identical(class(ijp1$id), "factor"), # should be character
  identical(class(ijp2$id), "character"), # should be character
  identical(names(ijp1), c("id", "var1", "var2")),
  identical(names(ijp2), c("id", "var2", "var1"))
)

foo <- data.frame(data.frame(id = 1:10, var1 = "foo"), stringsAsFactors = FALSE)
bar <- data.frame(data.frame(id = as.numeric(rep(1:10, 5)), var2 = "bar"), stringsAsFactors = FALSE)
ijm1 <- bkdat:::join_inner_merge(foo, bar, by = "id")
ijm2 <- bkdat:::join_inner_merge(bar, foo, by = "id")
ijp1 <- bkdat:::join_inner_plyr(foo, bar, by = "id")
ijp2 <- bkdat:::join_inner_plyr(bar, foo, by = "id")
assert(
  "join_inner() - is symmetric, even when type of join var is different",
  identical(names(ijm1), c("id", "var1", "var2")),
  identical(names(ijm2), c("id", "var2", "var1")),
  identical(names(ijp1), c("id", "var1", "var2")),
  identical(names(ijp2), c("id", "var2", "var1"))
)

x <- data.frame(x1 = c(1, 3, 2))
y <- data.frame(y1 = c(1, 2, 3), y2 = c("foo", "foo", "bar"), stringsAsFactors = FALSE)
ljm <- bkdat:::join_left_merge(x, y, by = c("x1" = "y1"))
ljp <- bkdat:::join_left_plyr(x, y, by = c("x1" = "y1"))
assert(
  "join_left() - by different variable names",
  identical(names(ljm), c("x1", "y2")),
  identical(ljm$x1, c(1, 3, 2)),
  identical(ljm$y2, c("foo", "bar", "foo")),
  identical(names(ljp), c("x1", "y2")),
  identical(ljp$x1, c(1, 3, 2)),
  identical(ljp$y2, c("foo", "bar", "foo"))
)

a <- data.frame(x = c(1, 1, 2, 3) * 1i, y = 1:4)
b <- data.frame(x = c(1, 2, 2, 4) * 1i, z = 1:4)
ijm <- bkdat:::join_inner_merge(a, b, "x")
ijp <- bkdat:::join_inner_plyr(a, b, "x")
assert(
  "join_*() - support complex vectors",
  identical(names(ijm), c("x", "y", "z")),
  identical(ijm$y, c(1L, 2L, 3L, 3L)),
  identical(ijm$z, c(1L, 1L, 2L, 3L)),
  identical(names(ijp), c("x", "y", "z")),
  identical(ijp$y, c(1L, 2L, 3L, 3L)),
  identical(ijp$z, c(1L, 1L, 2L, 3L))
)

a <- data.frame(x = 1:10, y = 2:11)
b <- data.frame(z = 5:14, x = 3:12) # x from this gets suffixed by .y
ljm1 <- bkdat:::join_left_merge(a, b, by = c("x" = "z"))
ljp1 <- bkdat:::join_left_merge(a, b, by = c("x" = "z"))
a <- data.frame(x = 1:10, z = 2:11)
b <- data.frame(z = 5:14, x = 3:12) # x from this gets suffixed by .y
ljm2 <- bkdat:::join_left_merge(a, b, by = c("x" = "z"))
ljp2 <- bkdat:::join_left_merge(a, b, by = c("x" = "z"))
assert(
  "join_left() - suffix variable names",
  identical(names(ljm1), c("x", "y", "x.y")),
  identical(names(ljm2), c("x", "z", "x.y")),
  identical(names(ljp1), c("x", "y", "x.y")),
  identical(names(ljp2), c("x", "z", "x.y"))
)

a <- data.frame(x = 1:10, y = 2:11)
b <- data.frame(x = 5:14, z = 3:12)
ljm1 <- bkdat:::join_right_merge(a, b) # this is wrong?
ljp1 <- bkdat:::join_right_plyr(a, b)
a <- data.frame(x = 1:10, y = 2:11)
b <- data.frame(z = 5:14, a = 3:12)
ljm2 <- bkdat:::join_right_merge(a, b, by = c("x" = "z"))
ljp2 <- bkdat:::join_right_plyr(a, b, by = c("x" = "z"))
assert(
  "join_right() - gets the column in the right order",
  identical(names(ljm1), c("x", "y", "z")),
  identical(names(ljm2), c("x", "y", "a")),
  identical(names(ljp1), c("x", "y", "z")),
  identical(names(ljp2), c("x", "y", "a"))
)

a <- data.frame(x = 1:3, y = 2:4)
b <- data.frame(x = 3:5, z = 3:5)
# merge method doesnt order rows the same as dplyr
fjm <- bkdat:::join_full_merge(a, b, "x")
fjp <- bkdat:::join_full_plyr(a, b, "x")
assert(
  "join_full() - ",
  identical(fjm$x, c(3L, 1L, 2L, 4L, 5L)),
  identical(fjm$y[1:3], c(4L, 2L, 3L)),
  all(is.na(fjm$y[4:5])),
  all(is.na(fjm$z[2:3])),
  identical(fjm$z[c(1, 4, 5)], 3:5),
  identical(fjp$x, 1:5),
  identical(fjp$y[1:3], 2:4),
  all(is.na(fjp$y[4:5])),
  all(is.na(fjp$z[1:2])),
  identical(fjp$z[3:5], 3:5)
)

# There are still issues with this, but acceptable for now?
x <- data.frame(
  Greek = c("Alpha", "Beta", NA),
  numbers = 1:3,
  stringsAsFactors = TRUE
)
y <- data.frame(
  Greek = c("Alpha", "Beta", "Gamma"),
  Letters = c("C", "B", "C"),
  stringsAsFactors = FALSE
)
ljm1 <- bkdat:::join_left_merge(x, y, by = "Greek")
ljm2 <- bkdat:::join_left_merge(y, x, by = "Greek")
ljp1 <- suppressWarnings(bkdat:::join_left_plyr(x, y, by = "Greek"))
ljp2 <- suppressWarnings(bkdat:::join_left_plyr(y, x, by = "Greek"))
assert(
  "join_*() - JoinStringFactorVisitor and JoinFactorStringVisitor handle NA #688",
  #has_warning(ljm1), #"Column `Greek` joining factor and character vector, coercing into character vector"
  is.na(ljm1$Greek[3]),
  is.na(ljm1$Letters[3]),
  identical(ljm1$numbers, 1:3),
  is.factor(ljm1$Greek),
  #has_warning(ljm2), # "Column `Greek` joining character vector and factor, coercing into character vector"
  identical(ljm2$Greek, y$Greek),
  identical(ljm2$Letters, y$Letters),
  identical(ljm2$numbers[1:2], 1:2),
  is.na(ljm2$numbers[3]),
  is.character(ljm2$Greek),

  has_warning(bkdat:::join_left_plyr(x, y, by = "Greek")), #"Column `Greek` joining factor and character vector, coercing into character vector"
  is.na(ljp1$Greek[3]),
  is.na(ljp1$Letters[3]),
  identical(ljp1$numbers, 1:3),
  is.factor(ljp1$Greek),
  has_warning(bkdat:::join_left_plyr(y, x, by = "Greek")), # "Column `Greek` joining character vector and factor, coercing into character vector"
  identical(ljp2$Greek, y$Greek),
  identical(ljp2$Letters, y$Letters),
  identical(ljp2$numbers[1:2], 1:2),
  is.na(ljp2$numbers[3]),
  is.character(ljp2$Greek)
)

input <- data.frame(g1 = factor(c("A", "B", "C"), levels = c("B", "A", "C")))
output <- data.frame(
  g1 = factor(c("A", "B", "C"), levels = c("B", "A", "C")),
  g2 = factor(c("A", "B", "C"), levels = c("B", "A", "C"))
)
ijm <- bkdat:::join_inner_merge(group_by(input, "g1"), group_by(output, "g1"))
ijp <- bkdat:::join_inner_plyr(group_by(input, "g1"), group_by(output, "g1"))
assert(
  "join_inner() - preserve levels order",
  identical(levels(ijm$g1), levels(input$g1)),
  identical(levels(ijm$g2), levels(output$g2)),
  identical(levels(ijp$g1), levels(input$g1)),
  identical(levels(ijp$g2), levels(output$g2))
)

test <- data.frame(Greek = c("Alpha", "Beta", "Gamma"), Letters = LETTERS[1:3], stringsAsFactors = FALSE)
lookup <- data.frame(Letters = c("C", "B", "C"), stringsAsFactors = FALSE)
ijm <- bkdat:::join_inner_merge(lookup, test)
ijp <- bkdat:::join_inner_plyr(lookup, test)
assert(
  "join_inner() - does not reorder",
  identical(ijm$Letters, c("C", "C", "B")),
  identical(ijp$Letters, c("C", "B", "C"))
)

d1 <- data.frame(a = factor(c("a", "b", "c")), stringsAsFactors = FALSE)
d2 <- data.frame(a = factor(c("a", "e")), stringsAsFactors = FALSE)
ijm1 <- bkdat:::join_inner_merge(d1, d2)
ijp1 <- bkdat:::join_inner_plyr(d1, d2)
d3 <- d1
attr(d3$a, "levels") <- c("c", "b", "a")
ijm2 <- bkdat:::join_inner_merge(d1, d3)
ijp2 <- bkdat:::join_inner_plyr(d1, d3)
assert(
  "join_*() - coerce factors with different levels to character",
  #has_warning(bkdat:::join_inner_merge(d1, d2)),
  #has_warning(bkdat:::join_inner_merge(d1, d3)),
  inherits(ijm1$a, "factor"),
  inherits(ijp1$a, "factor"),
  inherits(ijm2$a, "factor"),
  inherits(ijp2$a, "factor")
)

d1 <- data.frame(a = factor(c("a", "b", "c")), stringsAsFactors = FALSE)
d2 <- data.frame(a = c("a", "e"), stringsAsFactors = FALSE)
ijm1 <- bkdat:::join_inner_merge(d1, d2)
ijp1 <- suppressWarnings(bkdat:::join_inner_plyr(d1, d2))
ijm2 <- bkdat:::join_inner_merge(d2, d1)
ijp2 <- suppressWarnings(bkdat:::join_inner_plyr(d2, d1))
assert(
  "join_*() - between factor and character coerces to character with a warning",
  has_warning(bkdat:::join_inner_plyr(d1, d2)),
  inherits(ijm1$a, "factor"),
  inherits(ijp1$a, "factor"),
  has_warning(bkdat:::join_inner_plyr(d2, d1)),
  inherits(ijm2$a, "character"),
  inherits(ijp2$a, "character")
)

# d1 <- group_by(data.frame(x = 1:5, y = 1:5), c("x", "y"))
# d2 <- data.frame(x = 1:5, y = 1:5)
# ijm <- bkdat:::join_inner_merge(d1, d2, by = "x")
# ijp <- bkdat:::join_inner_plyr(d1, d2, by = "x")
# assert(
#   "join_*() - group column names reflect renamed duplicate columns",
#   expect_groups(d1, c("x", "y")),
#   expect_groups(ijm, c("x", "y.x"))
# )

# assert("join_*() - group column names are null when joined data frames are not grouped",
#   d1 <- data.frame(x = 1:5, y = 1:5)
#   d2 <- data.frame(x = 1:5, y = 1:5)
#   res <- bkdat:::join_inner_merge(d1, d2, by = "x")
#   expect_no_groups(res)
# )

# Guessing variables in x and y ------------------------------------------------

by1 <- bkdat:::by_names(c("x", "y", "z"), c("x", "y", "z"), c("x", "y", "z"))
by2 <- bkdat:::by_names(c("x" = "a", "y", "z"), c("x", "y", "z"), c("a", "y", "z"))
assert(
  "join_*() - unnamed vars are the same in both tables",
  identical(by1$by_x, c("x", "y", "z")),
  identical(by1$by_y, c("x", "y", "z")),
  identical(by2$by_x, c("x", "y", "z")),
  identical(by2$by_y, c("a", "y", "z"))
)

df1 <- data.frame(x = 1, y = 1:5)
df2 <- data.frame(y = 1:5, z = 2)
ljm <- bkdat:::join_left_merge(df1, df2)
ljp <- bkdat:::join_left_plyr(df1, df2)
assert(
  "join_*() - columns are not moved to the left",
  identical(names(ljm), c("x", "y", "z")),
  identical(names(ljp), c("x", "y", "z"))
)

text <- c("\xC9lise", "Pierre", "Fran\xE7ois")
Encoding(text) <- "latin1"
x <- data.frame(name = text, score = c(5, 7, 6), stringsAsFactors = FALSE)
y <- data.frame(name = text, attendance = c(8, 10, 9), stringsAsFactors = FALSE)
ljm1 <- bkdat:::join_left_merge(x, y, by = "name")
ljp1 <- bkdat:::join_left_plyr(x, y, by = "name")
x <- data.frame(name = factor(text), score = c(5, 7, 6), stringsAsFactors = FALSE)
y <- data.frame(name = text, attendance = c(8, 10, 9), stringsAsFactors = FALSE)
ljm2 <- bkdat:::join_left_merge(x, y, by = "name")
ljp2 <- suppressWarnings(bkdat:::join_left_plyr(x, y, by = "name"))
x <- data.frame(name = text, score = c(5, 7, 6), stringsAsFactors = FALSE)
y <- data.frame(name = factor(text), attendance = c(8, 10, 9), stringsAsFactors = FALSE)
ljm3 <- suppressWarnings(bkdat:::join_left_merge(x, y, by = "name"))
ljp3 <- suppressWarnings(bkdat:::join_left_plyr(x, y, by = "name"))
x <- data.frame(name = factor(text), score = c(5, 7, 6), stringsAsFactors = FALSE)
y <- data.frame(name = factor(text), attendance = c(8, 10, 9), stringsAsFactors = FALSE)
ljm4 <- suppressWarnings(bkdat:::join_left_merge(x, y, by = "name"))
ljp4 <- suppressWarnings(bkdat:::join_left_plyr(x, y, by = "name"))
assert(
  "join_*() - can handle multiple encodings",
  identical(nrow(ljm1), 3L),
  identical(ljm1$name, as.character(x$name)),
  identical(nrow(ljp1), 3L),
  identical(ljp1$name, as.character(x$name)),

  identical(nrow(ljm2), 3L),
  identical(ljm2$name, y$name),
  identical(nrow(ljp2), 3L),
  identical(ljp2$name, y$name),

  identical(nrow(ljm3), 3L),
  identical(ljm3$name, as.character(x$name)),
  identical(nrow(ljp3), 3L),
  identical(ljp3$name, as.character(x$name)),

  identical(nrow(ljm4), 3L),
  identical(ljm4$name, x$name),
  identical(nrow(ljp4), 3L),
  identical(ljp4$name, x$name)
)

x <- data.frame(q = c("a", "b", "c"), r = c("d", "e", "f"), s = c("1", "2", "3"))
y <- data.frame(q = c("a", "b", "c"), r = c("d", "e", "f"), t = c("xxx", "xxx", "xxx"))
ljm <- bkdat:::join_left_merge(x, y, by = c("r", "q"))
ljp <- bkdat:::join_left_plyr(x, y, by = c("r", "q"))
assert(
  "join_*() - creates correctly named results",
  identical(names(ljm), c("q", "r", "s", "t")),
  identical(ljm$q, x$q),
  identical(ljm$r, x$r),
  identical(names(ljp), c("q", "r", "s", "t")),
  identical(ljp$q, x$q),
  identical(ljp$r, x$r)
)

set.seed(75)
x <- data.frame(
  cat1 = sample(c("A", "B", NA), 5, 1),
  cat2 = sample(c(1, 2, NA), 5, 1), v = rpois(5, 3),
  stringsAsFactors = FALSE
)
y <- data.frame(
  cat1 = sample(c("A", "B", NA), 5, 1),
  cat2 = sample(c(1, 2, NA), 5, 1), v = rpois(5, 3),
  stringsAsFactors = FALSE
)
ijm <- bkdat:::join_inner_merge(x, y, by = c("cat1", "cat2"))
ijp <- bkdat:::join_inner_plyr(x, y, by = c("cat1", "cat2"))
me <- merge(x, y, by = c("cat1", "cat2"))
assert(
  "join_inner() - gives same result as merge by default",
  identical(ijm, me),
  identical(ijp, me)
)

df1 <- data.frame(x = 1:10, text = letters[1:10], stringsAsFactors = FALSE)
df2 <- data.frame(x = 1:5, text = "", stringsAsFactors = FALSE)
df2$text <- matrix(LETTERS[1:10], nrow = 5)
ljm <- filter_(bkdat:::join_left_merge(df1, df2, by = c("x" = "x")), "x > 5")
ljp <- filter_(bkdat:::join_left_plyr(df1, df2, by = c("x" = "x")), "x > 5")
ljm_text.y <- ljm$text.y
ljp_text.y <- ljp$text.y
assert(
  "join_*() - handles matrices #1230",
  is.matrix(ljm$text.y),
  identical(dim(ljm$text.y), c(5L, 2L)),
  all(is.na(ljm$text.y)),
  is.matrix(ljp$text.y),
  identical(dim(ljp$text.y), c(5L, 2L)),
  all(is.na(ljp$text.y))
)

a <- data.frame(character = c("\u0663"), set = c("arabic_the_language"), stringsAsFactors = F)
b <- data.frame(character = c("3"), set = c("arabic_the_numeral_set"), stringsAsFactors = F)
ijm1 <- bkdat:::join_inner_merge(b, a, by = c("character"))
ijp1 <- bkdat:::join_inner_plyr(b, a, by = c("character"))
ijm2 <- bkdat:::join_inner_merge(a, b, by = c("character"))
ijp2 <- bkdat:::join_inner_plyr(a, b, by = c("character"))
assert(
  "join_*() - ordering of strings is not confused by R's collate order",
  identical(nrow(ijm1), 0L),
  identical(nrow(ijp1), 0L),
  identical(nrow(ijm2), 0L),
  identical(nrow(ijp2), 0L)
)

date1 <- structure(-1735660800, tzone = "America/Chicago", class = c("POSIXct", "POSIXt"))
date2 <- structure(-1735660800, tzone = "UTC", class = c("POSIXct", "POSIXt"))
df1 <- data.frame(date = date1)
df2 <- data.frame(date = date2)
ljm <- attr(bkdat:::join_left_merge(df1, df1)$date, "tzone")
ljp <- attr(bkdat:::join_left_plyr(df1, df1)$date, "tzone")
assert(
  "join_*() - handle tzone differences",
  identical(ljm, "America/Chicago"),
  identical(ljp, "America/Chicago")
)

x <- data.frame(
  id = c(NA_character_, NA_character_),
  stringsAsFactors = F
)
y <- expand.grid(
  id = c(NA_character_, NA_character_),
  LETTER = LETTERS[1:2],
  stringsAsFactors = F
)
ljm <- bkdat:::join_left_merge(x, y, by = "id")
ljp <- bkdat:::join_left_plyr(x, y, by = "id")
assert(
  "join_*() - matches NA in character vector by default",
  all(is.na(ljm$id)),
  identical(ljm$LETTER, rep(rep(c("A", "B"), each = 2), 2)),
  all(is.na(ljp$id)),
  identical(ljp$LETTER, rep(rep(c("A", "B"), each = 2), 2))
)

# avoid repetition
d1 <- data.frame(id = 1:5, foo = rnorm(5))
d2 <- data.frame(id = 1:5, foo = rnorm(5))
d3 <- data.frame(id = 1:5, foo = rnorm(5))
ljm1 <- bkdat:::join_left_merge(d1, d1, by = "id")
ljm2 <- bkdat:::join_left_merge(ljm1, d2, by = "id")
ljm3 <- bkdat:::join_left_merge(ljm2, d3, by = "id")
ljp1 <- bkdat:::join_left_plyr(d1, d1, by = "id")
ljp2 <- bkdat:::join_left_plyr(ljp1, d2, by = "id")
ljp3 <- bkdat:::join_left_plyr(ljp2, d3, by = "id")
assert(
  "join_*() - avoid name repetition",
  identical(names(ljm3), c("id", "foo.x", "foo.y", "foo.x.x", "foo.y.y")),
  identical(names(ljp3), c("id", "foo.x", "foo.y", "foo.x.x", "foo.y.y"))
)

x <- data.frame()
y <- data.frame(a = 1)
assert(
  "join_*() - functions are protected against empty by",
  has_error(bkdat:::join_left_merge(x, y, by = names(x))), # "`by` must specify variables to join by"
  has_error(bkdat:::join_right_merge(x, y, by = names(x))), # "`by` must specify variables to join by"
  has_error(bkdat:::join_full_merge(x, y, by = names(x))), # "`by` must specify variables to join by"
  has_error(bkdat:::join_inner_merge(x, y, by = names(x))), # "`by` must specify variables to join by"

  has_error(bkdat:::join_left_plyr(x, y, by = names(x))), # "`by` must specify variables to join by"
  has_error(bkdat:::join_right_plyr(x, y, by = names(x))), # "`by` must specify variables to join by"
  has_error(bkdat:::join_full_plyr(x, y, by = names(x))), # "`by` must specify variables to join by"
  has_error(bkdat:::join_inner_plyr(x, y, by = names(x))) # "`by` must specify variables to join by"
)

data2 <- data.frame(a = 1:3)
data1 <- data.frame(a = 1:3, c = 3:5)
ljm1 <- bkdat:::join_left_merge(data1, data2, by = c("a", "a"))
ljm2 <- bkdat:::join_left_merge(data1, data2, by = c("a" = "a"))
ljp1 <- bkdat:::join_left_plyr(data1, data2, by = c("a", "a"))
ljp2 <- bkdat:::join_left_plyr(data1, data2, by = c("a" = "a"))
assert(
  "join_*() - takes care of duplicates in by",
  identical(ljm1, ljm2),
  identical(ljp1, ljp2)
)

# Joined columns result in correct type ----------------------------------------

data1 <- data.frame(
  t = seq(as.POSIXct("2015-12-01", tz = "UTC"), length.out = 2, by = "days"),
  x = 1:2
)
ijm <- bkdat:::join_inner_merge(data1, data1, by = "t")
ijp <- bkdat:::join_inner_plyr(data1, data1, by = "t")
assert(
  "join_*() - result of joining POSIXct is POSIXct",
  identical(class(ijm$t), c("POSIXct", "POSIXt")),
  identical(class(ijp$t), c("POSIXct", "POSIXt"))
)

# TODO: warn that attributes get dropped.
tbl_left <- data.frame(
  i = rep(c(1, 2, 3), each = 2),
  x1 = letters[1:6],
  stringsAsFactors = FALSE
)
tbl_right <- data.frame(
  i = c(1, 2, 3),
  x2 = letters[1:3],
  stringsAsFactors = FALSE
)
attr(tbl_left$i, "label") <- "iterator"
attr(tbl_right$i, "label") <- "iterator"
ljm1 <- bkdat:::join_left_merge(tbl_left, tbl_right, by = "i")
ljp1 <- bkdat:::join_left_plyr(tbl_left, tbl_right, by = "i")
attr(tbl_left$i, "foo") <- "bar"
attributes(tbl_right$i) <- NULL
attr(tbl_right$i, "foo") <- "bar"
attr(tbl_right$i, "label") <- "iterator"
ljm2 <- bkdat:::join_left_merge(tbl_left, tbl_right, by = "i")
ljp2 <- bkdat:::join_left_plyr(tbl_left, tbl_right, by = "i")
assert(
  "join_*() - allows extra attributes if they are identical",
  identical(attr(ljm1$i, "label"), NULL),
  identical(attr(ljm2$i, "label"), NULL),
  identical(attr(ljm2$i, "foo"), NULL),
  identical(attr(ljp1$i, "label"), NULL),
  identical(attr(ljp2$i, "label"), NULL),
  identical(attr(ljp2$i, "foo"), NULL)
  #identical(attr(ljm1$i, "label"), "iterator"),
  #identical(attr(ljm2$i, "label"), "iterator"),
  #identical(attr(ljm2$i, "foo"), "bar"),
  #identical(attr(ljp1$i, "label"), "iterator"),
  #identical(attr(ljp2$i, "label"), "iterator"),
  #identical(attr(ljp2$i, "foo"), "bar")
)

# TODO: Warn for factor level coercion.
d1 <- iris[, c("Species", "Sepal.Length")]
d2 <- iris[, c("Species", "Sepal.Width")]
d2$Species <- factor(as.character(d2$Species), levels = rev(levels(d1$Species)))
ljm1 <- bkdat:::join_left_merge(d1, d2, by = "Species")
ljp1 <- bkdat:::join_left_plyr(d1, d2, by = "Species")
d1$Species <- as.character(d1$Species)
d2$Species <- as.character(d2$Species)
ljm2 <- bkdat:::join_left_merge(d1, d2, by = "Species")
ljp2 <- bkdat:::join_left_plyr(d1, d2, by = "Species")
assert(
  "join_*() - work with factors of different levels",
  identical(class(ljm1$Species), "factor"),
  identical(class(ljm2$Species), "character"),
  identical(class(ljp1$Species), "factor"),
  identical(class(ljp2$Species), "character")
)

# assert(
#   "join_*() - anti and semi joins give correct result when by variable is a factor",
#   big <- data.frame(letter = rep(c("a", "b"), each = 2), number = 1:2)
#   small <- data.frame(letter = "b")
#   has_warning(
#     aj_result <- anti_join(big, small, by = "letter"),
#     "Column `letter` joining factors with different levels, coercing to character vector",
#     fixed = TRUE
#   )
#   identical(aj_result$number, 1:2)
#   identical(aj_result$letter, factor(c("a", "a"), levels = c("a", "b")))
#
#   has_warning(
#     sj_result <- semi_join(big, small, by = "letter"),
#     "Column `letter` joining factors with different levels, coercing to character vector",
#     fixed = TRUE
#   )
#   identical(sj_result$number, 1:2)
#   identical(sj_result$letter, factor(c("b", "b"), levels = c("a", "b")))
# )

df3 <- data.frame(
  id = c(102, 102, 102, 121),
  name = c("qwer", "qwer", "qwer", "asdf"),
  k = factor(c("one", "two", "total", "one"), levels = c("one", "two", "total")),
  total = factor(c("tot", "tot", "tot", "tot"), levels = c("tot", "plan", "fact")),
  v = c(NA_real_, NA_real_, NA_real_, NA_real_),
  btm = c(25654.957609, 29375.7547216667, 55030.7123306667, 10469.3523273333),
  top = c(22238.368946, 30341.516924, 52579.88587, 9541.893144),
  stringsAsFactors = FALSE
)
df4 <- data.frame(
  id = c(102, 102, 102, 121),
  name = c("qwer", "qwer", "qwer", "asdf"),
  k = factor(c("one", "two", "total", "one"), levels = c("one", "two", "total")),
  type = factor(c("fact", "fact", "fact", "fact"), levels = c("tot", "plan", "fact")),
  perc = c(0.15363485835208, -0.0318297270618471, 0.0466114830816894, 0.0971986553754823),
  stringsAsFactors = FALSE
)
ijm <- replicate(100, bkdat:::join_inner_merge(df3, df4))
ijp <- replicate(100, bkdat:::join_inner_plyr(df3, df4))
ijm_out <- vector(mode = "logical", length = 99L)
for (i in 1:99) ijm_out[i] <- identical(ijm[, 1], ijm[, i+1])
ijp_out <- vector(mode = "logical", length = 99L)
for (i in 1:99) ijp_out[i] <- identical(ijp[, 1], ijp[, i+1])
assert("join_*() - inner join not crashing",
  # all we want here is to test that this does not crash
  all(ijm_out),
  all(ijp_out)
)


#
# # Misc --------------------------------------------------------------------
#

# assert("join_*() - regroups",
#   df1 <- data.frame(a = 1:3) %>% group_by(a)
#   df2 <- data.frame(a = rep(1:4, 2)) %>% group_by(a)
#
#   expect_grouped <- function(df)
#     expect_true(is_grouped_df(df))
#   }
#
#   expect_grouped(bkdat:::join_inner_merge(df1, df2))
#   expect_grouped(bkdat:::join_left_merge(df1, df2))
#   expect_grouped(bkdat:::join_right_merge(df2, df1))
#   expect_grouped(bkdat:::join_full_merge(df1, df2))
#   expect_grouped(anti_join(df1, df2))
#   expect_grouped(semi_join(df1, df2))
# )
#
#

df1 <- data.frame(a = as.POSIXct("2009-01-01 10:00:00", tz = "Europe/London"))
df2 <- data.frame(a = as.POSIXct("2009-01-01 11:00:00", tz = "Europe/Paris"))
ijm <- bkdat:::join_inner_merge(df1, df2, by = "a")
ijp <- bkdat:::join_inner_plyr(df1, df2, by = "a")
assert(
  "join_*() - accepts tz attributes",
  # It's the same time:
  #identical(nrow(ijm), 1L), # Error found on 2024-02-08 with R 4.3.1
  identical(nrow(ijp), 1L)
)


#
# assert("join_*() - semi- and anti-joins preserve order",
#   identical(
#     data.frame(a = 3:1) %>% semi_join(data.frame(a = 1:3)),
#     data.frame(a = 3:1)
#   )
#   identical(
#     data.frame(a = 3:1) %>% anti_join(data.frame(a = 4:6)),
#     data.frame(a = 3:1)
#   )
# )
#

# TODO: bind_fill_rows (full_merge_plyr()) cannot handle raw
df1 <- data.frame(r = as.raw(1:4), x = 1:4)
df2 <- data.frame(r = as.raw(3:6), y = 3:6)
assert(
  "join_*() - handles raw vectors",
  identical(
    bkdat:::join_left_merge(df1, df2, by = "r"),
    data.frame(r = as.raw(c(3L, 4L, 1L, 2L)), x = c(3L, 4L, 1L, 2L), y = c(3:4, NA, NA))
  ),

  identical(
    bkdat:::join_right_merge(df1, df2, by = "r"),
    data.frame(r = as.raw(3:6), x = c(3:4, NA, NA), y = c(3:6))
  ),

  identical(
    bkdat:::join_full_merge(df1, df2, by = "r"),
    data.frame(r = as.raw(c(3L,4L,1L,2L,5L,6L)), x = c(3L,4L,1L,2L,NA,NA), y = c(3L,4L,NA,NA,5L,6L))
  ),

  identical(
    bkdat:::join_inner_merge(df1, df2, by = "r"),
    data.frame(r = as.raw(3:4), x = c(3:4), y = c(3:4))
  ),

  identical(
    bkdat:::join_left_plyr(df1, df2, by = "r"),
    data.frame(r = as.raw(1:4), x = 1:4, y = c(NA, NA, 3:4))
  ),

  identical(
    bkdat:::join_right_plyr(df1, df2, by = "r"),
    data.frame(r = as.raw(3:6), x = c(3:4, NA, NA), y = c(3:6))
  ),

  # FIXME
  has_error(identical(
    bkdat:::join_full_plyr(df1, df2, by = "r"),
    data.frame(r = as.raw(1:6), x = c(1:4, NA, NA), y = c(NA, NA, 3:6))
  )),

  identical(
    bkdat:::join_inner_plyr(df1, df2, by = "r"),
    data.frame(r = as.raw(3:4), x = c(3:4), y = c(3:4))
  )
)

# df1 <- data.frame(x = c(1, 2), y = c(2, 3))
# df2 <- data.frame(x = c(1, 1), z = c(2, 3))
#  <- nest_join(df1, df2, by = "x")
# assert("join_*() - nest_join works",{
#
#   identical(names(res), c(names(df1), "df2"))
#   identical(res$df2[[1]], select(df2, z))
#   identical(res$df2[[2]], data.frame(z = double()))
# )

# assert("join_*() - nest_join handles multiple matches in x",
#   df1 <- data.frame(x = c(1, 1))
#   df2 <- data.frame(x = 1, y = 1:2)
#
#   tbls <- df1 %>%
#     nest_join(df2) %>%
#     pull()
#
#   identical(tbls[[1]], tbls[[2]])
# )
#

# TODO: check for duplicate column names?
# df1 <- data.frame(x1 = 1:3, x2 = 1:3, y = 1:3)
# names(df1)[1:2] <- "x"
# df2 <- data.frame(x = 2:4, y = 2:4)
# assert("join_*() - reject data frames with duplicate columns",
#   has_error(
#     bkdat:::join_left_plyr(df1, df2, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_left_merge(df2, df1, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_right_merge(df1, df2, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_right_merge(df2, df1, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_inner_merge(df1, df2, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_inner_merge(df2, df1, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_full_merge(df1, df2, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_full_merge(df2, df1, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     semi_join(df1, df2, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   # FIXME: Compatibility, should throw an error eventually
#   has_warning(
#     identical(
#       semi_join(df2, df1, by = c("x", "y")),
#       data.frame(x = 2:3, y = 2:3)
#     ),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   has_error(
#     anti_join(df1, df2, by = c("x", "y")),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
#
#   # FIXME: Compatibility, should throw an error eventually
#   has_warning(
#     identical(
#       anti_join(df2, df1, by = c("x", "y")),
#       data.frame(x = 4L, y = 4L)
#     ),
#     "Column `x` must have a unique name",
#     fixed = TRUE
#   )
# )
#

# TODO: Check for NA missing column names?
# df_a <- data.frame(B = c("a", "b", "c"), AA = 1:3)
# df_b <- data.frame(AA = 2:4, C = c("aa", "bb", "cc"))
# df_aa <- df_a
# names(df_aa) <- c(NA, "AA")
# df_ba <- df_b
# names(df_ba) <- c("AA", NA)
# assert("join_*() - reject data frames with NA columns",
#   has_error(
#     bkdat:::join_left_plyr(df_aa, df_b),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_left_merge(df_aa, df_ba),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_left_merge(df_a, df_ba),
#     "Column `2` cannot have NA as name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_right_merge(df_aa, df_b),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_right_merge(df_aa, df_ba),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_right_merge(df_a, df_ba),
#     "Column `2` cannot have NA as name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_inner_merge(df_aa, df_b),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_inner_merge(df_aa, df_ba),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_inner_merge(df_a, df_ba),
#     "Column `2` cannot have NA as name",
#     fixed = TRUE
#   )
#
#   has_error(
#     bkdat:::join_full_merge(df_aa, df_b),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_full_merge(df_aa, df_ba),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_error(
#     bkdat:::join_full_merge(df_a, df_ba),
#     "Column `2` cannot have NA as name",
#     fixed = TRUE
#   )
#
#   has_warning(
#     semi_join(df_aa, df_b),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_warning(
#     semi_join(df_aa, df_ba),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_warning(
#     semi_join(df_a, df_ba),
#     "Column `2` cannot have NA as name",
#     fixed = TRUE
#   )
#
#   has_warning(
#     anti_join(df_aa, df_b),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_warning(
#     anti_join(df_aa, df_ba),
#     "Column `1` cannot have NA as name",
#     fixed = TRUE
#   )
#   has_warning(
#     anti_join(df_a, df_ba),
#     "Column `2` cannot have NA as name",
#     fixed = TRUE
#   )
# )
