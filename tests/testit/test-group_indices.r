library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "group_indices() errors on non-dataframes.",
  has_error(group_indices(list()))
)

assert(
  "group_indices() errors if group_cols is not character.",
  has_error(group_indices(data.frame(1), 1))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# group_indices works as expected
#-------------------------------------------------------------------------------
df <- data.frame(
  a = 1:5,
  b = c("b", "b", "a", "a", "c"),
  c = c("d", "d", "e", "f", "g"),
  stringsAsFactors = FALSE
)

assert(
  "group_indices() works as expected.",
  identical(
    group_indices(df, "b", arrange = TRUE),
    list(a = c(3L, 4L), b = c(1L, 2L), c = 5L)
  ),
  identical(
    group_indices(df, c("b", "c"), arrange = TRUE),
    list(`a|e` = 3L, `a|f` = 4L, `b|d` = 1:2, `c|g` = 5L)
  ),
  identical(
    group_indices(df, "b", arrange = FALSE),
    list(b = 1:2, a = 3:4, c = 5L)
  ),
  identical(
    group_indices(df, c("b", "c"), arrange = FALSE),
    list(`b|d` = 1:2, `a|e` = 3L, `a|f` = 4L, `c|g` = 5L)
  )
)
