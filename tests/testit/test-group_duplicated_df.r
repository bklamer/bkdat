library(testit)

df1 <- data.frame(a = c(1, 1))
df2 <- data.frame(a = c(1, 2))
df3 <- data.frame(a = c(1, 1), b = c(2, 3))
df4 <- data.frame(a = c(1, 1), b = c(2, 2))

assert(
  "group_duplicated_df() works as expected.",
  identical(group_duplicated_df(df1), c(F, T)),
  identical(group_duplicated_df(df2), c(F, F)),
  identical(group_duplicated_df(df3), c(F, F)),
  identical(group_duplicated_df(df4), c(F, T))
)
