library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "group_by() errors if 'data' is non-dataframe.",
  has_error(group_by(list()))
)

assert(
  "group_by() errors if 'group_cols' is non-character.",
  has_error(group_by(data.frame(), 1))
)

assert(
  "group_by() errors if 'add' is non-logical.",
  has_error(group_by(data.frame(), "a", 1))
)

#-------------------------------------------------------------------------------
# Group_by errors when grouping on variables of type list.
#-------------------------------------------------------------------------------
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(T, NA, F),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

for(var in names(df)[names(df) %in% c("POSIXlt", "list")]) {
  assert(
    "group_by() errors when grouping on variables of type list.",
    has_error(group_by(df, var))
  )
}

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# Group_by works as expected
#-------------------------------------------------------------------------------
df <- data.frame(
  a = c("a", "a", "b"),
  b = c("d", "e", "e"),
  c = 1:3,
  stringsAsFactors = TRUE
)
at <- attributes(group_by(df, c("a", "b")))
assert(
  "group_by() works as expected.",
  "group_df" %in% at$class,
  identical(at$group_cols, c("a", "b")),
  identical(at$group_indices, list(`a|d` = 1L, `a|e` = 2L, `b|e` = 3L)),
  identical(at$group_length, c(1L, 1L, 1L)),
  identical(
    at$group_labels,
    data.frame(
      a = c("a", "a", "b"),
      b = c("d", "e", "e"),
      stringsAsFactors = TRUE
    )
  )
)

#-------------------------------------------------------------------------------
# Group_by adds groups if add = TRUE
#-------------------------------------------------------------------------------
df <- data.frame(x = rep(1:3, each = 10), y = rep(1:6, each = 5))
df1 <- group_by(df, c("x", "y"), add = TRUE)
df2 <- group_by(df, "x")
df2 <- group_by(df2, "y", add = TRUE)
assert(
  "group_by() adds groups if add = TRUE",
  identical(attributes(df1)$group_cols, c("x", "y")),
  identical(attributes(df2)$group_cols, c("x", "y"))
)

#-------------------------------------------------------------------------------
# Group_by works as expected for valid data types.
#-------------------------------------------------------------------------------
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(T, NA, F),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

for(var in names(df)[!(names(df) %in% c("POSIXlt", "list"))]) {
  assert(
    "group_by() works as expected for valid data types.",
    attributes(group_by(df, var))$group_cols == var
  )
}
