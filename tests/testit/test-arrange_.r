library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 2, 1),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# arrange_ errors if not a dataframe.
#-------------------------------------------------------------------------------
assert(
  "arrange_() errors if not a dataframe.",
  has_error(arrange_(list()))
)

#-------------------------------------------------------------------------------
# arrange_ error if missing 'x'.
#-------------------------------------------------------------------------------
assert(
  "arrange_() errors if missing 'x'.",
  has_error(arrange_(df))
)

#-------------------------------------------------------------------------------
# arrange_ errors if 'x' is not a character vector.
#-------------------------------------------------------------------------------
assert(
  "arrange_() errors if 'x' is not a character vector",
  has_error(arrange_(df, 2))
)

#-------------------------------------------------------------------------------
# arrange_ doesnt have an error if sorting on duplicate column names.
#-------------------------------------------------------------------------------
df_dup1 <- data.frame(x = 4:1, x = 1:4, check.names = FALSE)
df_dup2 <- data.frame(x = 1:4, x = 1:4, y = 1:4, check.names = FALSE)
df_dup3 <- data.frame(x = 1:4, x = 1:4, y = 1:4, z = 1:4, check.names = FALSE)
df_dup4 <- data.frame(x = 1:4, x = 1:4, y = 1:4, y = 1:4, z = 1:4, check.names = FALSE)

assert(
  "arrange_() orders on the first duplicate.",
  identical(arrange_(df_dup1, "x")[[1]], 1:4),
  identical(df_dup2, arrange_(df_dup2, "y")),
  identical(df_dup3, arrange_(df_dup3, c("y", "z"))),
  identical(df_dup3, arrange_(df_dup3, c("x", "y"))),
  identical(df_dup4, arrange_(df_dup4, c("x", "y")))
)

#-------------------------------------------------------------------------------
# arrange_ errors when sorting on a list column.
#-------------------------------------------------------------------------------
assert(
  "arrange_() errors when sorting on a list column.",
  has_error(arrange_(df, "list"))
)

#-------------------------------------------------------------------------------
# arrange_ has error if sorting on unknown column name.
#-------------------------------------------------------------------------------
assert(
  "arrange_() has error if sorting on unknown column name.",
  has_error(arrange_(df, "z")),
  has_error(arrange_(df, c("numeric", "z")))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# arrange_ sorts missing values to end.
#-------------------------------------------------------------------------------
na_last <- function(x) {
  n <- length(x)
  is.na(x[n])
}

assert(
  "arrange_() sorts missing values to end.",
  na_last(arrange_(df, "POSIXct")$POSIXct),
  na_last(arrange_(df, "POSIXlt")$POSIXlt),
  na_last(arrange_(df, "Date")$Date),
  na_last(arrange_(df, "numeric")$numeric),
  na_last(arrange_(df, "integer")$integer),
  na_last(arrange_(df, "character")$character),
  na_last(arrange_(df, "infinite")$infinite),
  na_last(arrange_(df, "logical")$logical),
  na_last(arrange_(df, "factor")$factor)
)

assert(
  "arrange_() sorts missing values to end with desc().",
  na_last(arrange_(df, "desc(POSIXct)")$POSIXct),
  na_last(arrange_(df, "desc(POSIXlt)")$POSIXlt),
  na_last(arrange_(df, "desc(Date)")$Date),
  na_last(arrange_(df, "desc(numeric)")$numeric),
  na_last(arrange_(df, "desc(integer)")$integer),
  na_last(arrange_(df, "desc(character)")$character),
  na_last(arrange_(df, "desc(infinite)")$infinite),
  na_last(arrange_(df, "desc(logical)")$logical),
  na_last(arrange_(df, "desc(factor)")$factor)
)

#-------------------------------------------------------------------------------
# arrange_ sorts as expected.
#-------------------------------------------------------------------------------
assert(
  "arrange_() sorts as expected.",
  identical(arrange_(df, "POSIXct")$POSIXct, as.POSIXct(c("2017-08-13 EDT", "2017-08-14 EDT", NA))),
  identical(arrange_(df, "POSIXlt")$POSIXlt, as.POSIXlt(c("2017-08-13 EDT", "2017-08-14 EDT", NA))),
  identical(arrange_(df, "Date")$Date, as.Date(c("2017-08-13 EDT", "2017-08-14 EDT", NA))),
  identical(arrange_(df, "numeric")$numeric, c(1, 2, NA)),
  identical(arrange_(df, "integer")$integer, c(3L, 4L, NA)),
  identical(arrange_(df, "character")$character, c("a", "b", NA)),
  identical(arrange_(df, "infinite")$infinite, c(1, Inf, NA)),
  identical(arrange_(df, "logical")$logical, c(FALSE, TRUE, NA)),
  identical(arrange_(df, "factor")$factor, factor(c("a", "b", NA)))
)

assert(
  "arrange_() sorts as expected with desc().",
  identical(arrange_(df, "desc(POSIXct)")$POSIXct, as.POSIXct(c("2017-08-14 EDT", "2017-08-13 EDT", NA))),
  identical(arrange_(df, "desc(POSIXlt)")$POSIXlt, as.POSIXlt(c("2017-08-14 EDT", "2017-08-13 EDT", NA))),
  identical(arrange_(df, "desc(Date)")$Date, as.Date(c("2017-08-14 EDT", "2017-08-13 EDT", NA))),
  identical(arrange_(df, "desc(numeric)")$numeric, c(2, 1, NA)),
  identical(arrange_(df, "desc(integer)")$integer, c(4L, 3L, NA)),
  identical(arrange_(df, "desc(character)")$character, c("b", "a", NA)),
  identical(arrange_(df, "desc(infinite)")$infinite, c(Inf, 1, NA)),
  identical(arrange_(df, "desc(logical)")$logical, c(TRUE, FALSE, NA)),
  identical(arrange_(df, "desc(factor)")$factor, factor(c("b", "a", NA)))
)

#-------------------------------------------------------------------------------
# Two arrange_s are equivalent to one arrange_.
#-------------------------------------------------------------------------------
a1 <- arrange_(df, c("numeric", "integer"))
a2 <- arrange_(arrange_(df, "integer"), "numeric")

assert(
  "Two arrange_() are equivalent to one arrange_().",
  identical(a1, a2)
)

#-------------------------------------------------------------------------------
# arrange_ sorts dataframes containing list columns.
#-------------------------------------------------------------------------------
a <- arrange_(df, "numeric")
list <- list(list(c = data.frame(d = c(3, 4))), NA, list(a = data.frame(b = c(1, 2))))

assert(
  "arrange_() sorts dataframes containing list columns.",
  identical(a$list, list)
)

#-------------------------------------------------------------------------------
# arrange_ handles 0-rows data frames.
#-------------------------------------------------------------------------------
df_empty <- data.frame()
df_empty_ <- data.frame(a = numeric(0))

assert(
  "arrange_() handles 0 row data frames with colnames",
  identical(df_empty_, arrange_(df_empty_, "a"))
)

# This should really be for empty dataframes being passed to arrange_ with
# no variables to arrange_ on...
assert(
  "arrange_() handles empty data frames",
  identical(nrow(arrange_(df_empty, character(0))), 0L),
  identical(length(arrange_(df_empty, character(0))), 0L)
)

#-------------------------------------------------------------------------------
# arrange_ handles complex vectors.
#-------------------------------------------------------------------------------
df_complex <- data.frame(x = 1:10, y = 10:1 + 2i)
res <- arrange_(df_complex, "y")
res2 <- arrange_(df_complex, "desc(y)")

df_complex_2 <- df_complex
df_complex_2$y[c(3, 6)] <- NA
res3 <- arrange_(df_complex_2, "y")
res4 <- arrange_(df_complex_2, "desc(y)")

assert(
  "arrange_() handles complex vectors",
  identical(res$y, rev(df_complex$y)),
  identical(res$x, rev(df_complex$x)),
  identical(res2$y, df_complex$y),
  identical(res2$x, df_complex$x),
  all(is.na(res3$y[9:10])),
  all(is.na(res4$y[9:10]))
)

#-------------------------------------------------------------------------------
# arrange_ *does not* respect attributes because subsetting with [ drops attributes.
#-------------------------------------------------------------------------------
env <- environment()
Period <- suppressWarnings(setClass("Period", contains = "numeric", where = env))
df_attr <- data.frame(p = Period(c(1, 2, 3)), x = 1:3)

assert(
  "arrange_() *does not* respect attributes",
  inherits(df_attr$p, "Period"),
  !inherits(arrange_(df_attr, "p")$p, "Period"),
  removeClass("Period", where = env)
)

#-------------------------------------------------------------------------------
# arrange_ respects locale.
#-------------------------------------------------------------------------------
df_locale <- data.frame(
  words = c("casa", "\u00e1rbol", "zona", "\u00f3rgano"),
  stringsAsFactors = FALSE
)

assert(
  "arrange_() respects locale",
  identical(arrange_(df_locale, "words")$words, sort(df_locale$words)),
  identical(arrange_(df_locale, "desc(words)")$words, sort(df_locale$words, decreasing = TRUE))
)

#-------------------------------------------------------------------------------
# arrange_ sorts descending correctly
#-------------------------------------------------------------------------------
df_desc <- data.frame(a = c(1, 2, 2), b = c(4, 6, 5))

assert(
  "arrange_() sorts descending correctly.",
  identical(arrange_(df, c("desc(integer)", "numeric"))$integer, c(4L, 3L, NA)),
  identical(arrange_(df, c("desc(integer)", "numeric"))$numeric, c(1, NA, 2)),
  identical(arrange_(df, c("integer", "desc(numeric)"))$integer, c(3L, 4L, NA)),
  identical(arrange_(df, c("integer", "desc(numeric)"))$numeric, c(NA, 1, 2))
)

#-------------------------------------------------------------------------------
# Grouped arrange_ ignores group.
#-------------------------------------------------------------------------------
df <- data.frame(g = c(2, 1, 2, 1), x = c(4:1))
out_g <- group_by(df, "g")
out_g1 <- arrange_(out_g, "x")

assert(
  identical(out_g1$x, 1:4)
)

#-------------------------------------------------------------------------------
# Nested arrange_ works.
#-------------------------------------------------------------------------------
df <- data.frame(g = c(2, 1, 2, 1), x = c(4:1))
out_n <- nest_by(df, "g")
out_n1 <- arrange_(out_n, "g")

df2 <- nest_by(mtcars, group_cols = c("cyl", "carb"))
out_mtcars1 <- arrange_(df2, c("desc(disp)"))

assert(
  identical(out_n1$g, c(1, 2)),
  identical(out_n1$data, list(data.frame(x = c(3L, 1L)), data.frame(x = c(4L, 2L)))),
  identical(out_mtcars1$data[[1]]$disp, c(167.6, 167.6, 160.0, 160.0))
)

#-------------------------------------------------------------------------------
# arrange_ keeps the grouping structure.
# (It does not in this case)
#-------------------------------------------------------------------------------
# test_that("arrange_ keeps the grouping structure", {
#   dat <- data.frame(g = c(2, 2, 1, 1), x = c(1, 3, 2, 4))
#   res <- dat %>% group_by(g) %>% arrange_()
#   expect_is(res, "grouped_df")
#   expect_equal(res$x, dat$x)
#
#   res <- dat %>% group_by(g) %>% arrange_(x)
#   expect_is(res, "grouped_df")
#   expect_equal(res$x, 1:4)
#   expect_equal(attr(res, "indices"), list(c(1, 3), c(0, 2)))
# })

#-------------------------------------------------------------------------------
# arrange_ by group works correctly.
#-------------------------------------------------------------------------------
df <- data.frame(g = c(1, 2), x = c(2, 1))
df <- group_by(df, "g")
df1 <- arrange_(df, "x", by_group = TRUE)
df2 <- arrange_(df, c("g", "x"))

assert(
  identical(df1, df2)
)
