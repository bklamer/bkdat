library(testit)

assert(
  "is_string() works as expected.",
  !is_string(c("a", "b")),
  is_string("a"),
  !is_string(1L),
  !is_string(TRUE),
  !is_string(Inf)
)
