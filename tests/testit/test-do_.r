library(testit)

# Grouped data frames ----------------------------------------------------------

# TODO: args must be all named or all unnamed?
df <- data.frame(
  g = c(1, 2, 2, 3, 3, 3),
  x = 1:6,
  y = 6:1
)
df <- group_by(df, "g")
assert(
  "bkdat::do_() - can have named and unnamed args",
  !has_error(do_(df, c(x = "1", "2")))
)

assert(
  "unnamed elements must return data frames",
  has_error(do_(df, "1")), # "Result must be a data frame, not numeric"
  has_error(do_(df, "1")), # "Results 1, 2, 3 must be data frames, not numeric"
  has_error(do_(df, "a")) # "Results 1, 2, 3 must be data frames, not character"
)

first <- do_(df, "head(., 1)")
assert(
  "unnamed results bound together by row",
  identical(nrow(first), 3L),
  identical(first$g, c(1, 2, 3)),
  identical(first$x, c(1L, 2L, 4L))
)

# TODO: only supply a single unnamed argument, not two?
assert(
  "can only use single unnamed argument",
  !has_error(do_(df, c("head(.)", "tail(.)"))) # "Can only supply one unnamed argument, not 2"
)

out <- do_(df, c(nrow = "nrow(.)", ncol = "ncol(.)"))
assert(
  "named argument become list columns",
  identical(out$nrow, list(1L, 2L, 3L)),
  # includes grouping columns
  identical(out$ncol, list(3L, 3L, 3L))
)

out <- do_(group_by(data.frame(a = 1), "a"), c(g = "nrow(.)", h = "nrow(.)"))
assert(
  "multiple outputs can access data",
  identical(names(out), c("a", "g", "h")),
  identical(out$g, list(1L)),
  identical(out$h, list(1L))
)

# TDO: Should this apply?
out <- do_(df, "data.frame(g = 1)")
assert(
  "colums in output override columns in input",
  !identical(names(out), "g"),
  identical(out[[2]], c(1, 1, 1))
)

# blankdf <- function(x) data.frame(blank = numeric(0))
# dat <- data.frame(a = 1:2, b = factor(1:2))
# assert(
#   "empty results preserved",
#   identical(
#     do_(group_by(dat, "b"), "blankdf(.)"),
#     data.frame(b = factor(integer(), levels = 1:2), blank = numeric())
#   )
# )

out1 <- data.frame(a = numeric(), b = factor())
out1 <- group_by(out1, "b")
out1 <- suppressWarnings(do_(out1, "data.frame(.)"))
out2 <- data.frame(a = numeric(), b = character(), stringsAsFactors = FALSE)
out2 <- group_by(out2, "b")
out2 <- suppressWarnings(do_(out2, "data.frame(.)"))
assert(
  "empty inputs give empty outputs",
  identical(out1, data.frame(b = factor())),
  identical(out2, data.frame(b = character(), stringsAsFactors = FALSE))
)

a <- 10
f <- function(a) do_(group_by(mtcars, "cyl"), c(a = "a"))
assert(
  "grouped do_ evaluates args in correct environment",
  identical(f(100)$a, list(100, 100, 100))
)

# # Ungrouped data frames --------------------------------------------------------
#
# out <- do_(mtcars, "head(.)")
# assert(
#   "ungrouped data frame with unnamed argument returns data frame",
#   inherits(out, "data.frame")
#   identical(dim(out), c(6, 11))
# )
#
# assert("ungrouped data frame with named argument returns list data frame",
#   out <- mtcars %>% do_(x = 1, y = 2:10)
#   inherits(out, "tbl_df")
#   identical(out$x, list(1))
#   identical(out$y, list(2:10))
# )
#
# assert("ungrouped do_ evaluates args in correct environment",
#   a <- 10
#   f <- function(a)
#     mtcars %>% do_(a = a)
#   }
#   identical(f(100)$a, list(100))
# )

# Zero row inputs --------------------------------------------------------------

# dat <- data.frame(x = numeric(0), g = character(0))
# grp <- group_by(dat, "g")
# emt <- slice(grp, "FALSE")
# assert(
#   "empty data frames give consistent outputs",
#
#
#   dat %>%
#     do_(grp, "data.frame()") %>%
#     vapply(do_(grp, "data.frame()"), type_sum, character(1)) %>%
#     length() %>%
#     identical(0)
#   dat %>%
#     do_(data.frame(y = integer(0))) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(y = "int"))
#   dat %>%
#     do_(data.frame(.)) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(x = "dbl", g = "chr"))
#   dat %>%
#     do_(data.frame(., y = integer(0))) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(x = "dbl", g = "chr", y = "int"))
#   dat %>%
#     do_(y = ncol(.)) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(y = "list"))
#
#   # Grouped data frame should have same col types as ungrouped, with addition
#   # of grouping variable
#   grp %>%
#     do_(data.frame()) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(g = "chr"))
#   grp %>%
#     do_(data.frame(y = integer(0))) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(g = "chr", y = "int"))
#   grp %>%
#     do_(data.frame(.)) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(x = "dbl", g = "chr"))
#   grp %>%
#     do_(data.frame(., y = integer(0))) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(x = "dbl", g = "chr", y = "int"))
#   grp %>%
#     do_(y = ncol(.)) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(g = "chr", y = "list"))
#
#   # A empty grouped dataset should have same types as grp
#   emt %>%
#     do_(data.frame()) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(g = "chr"))
#   emt %>%
#     do_(data.frame(y = integer(0))) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(g = "chr", y = "int"))
#   emt %>%
#     do_(data.frame(.)) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(x = "dbl", g = "chr"))
#   emt %>%
#     do_(data.frame(., y = integer(0))) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(x = "dbl", g = "chr", y = "int"))
#   emt %>%
#     do_(y = ncol(.)) %>%
#     vapply(type_sum, character(1)) %>%
#     identical(c(g = "chr", y = "list"))
# )
#
# blankdf <- function(x) data.frame(blank = numeric(0))
# dat <- data.frame(a = 1:2, b = factor(1:2))
# res <- do_(group_by(dat, "b"), "blankdf(.)")
# assert(
#   "handling of empty data frames in do_",
#   identical(names(res), c("b", "blank"))
# )
