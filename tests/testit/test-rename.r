library(testit)

df <- data.frame(a = 1, b = 2, c = 3, 4)
assert(
  "bkdat::rename() - errors if no match.",
  has_error(rename(df, c("e" = "d")))
)

df <- data.frame(a = 1, b = 2)
y <- rename(df, c(c = "b"))
assert(
  "bkdat::rename() - single name match makes change",
  identical(names(y), c("a", "c"))
)

df <- data.frame(a = 1, b = 2, c = 3)
y <- rename(df, c("f" = "c", "e" = "b", "d" = "a"))
assert(
  "bkdat::rename() - Multiple names correctly changed",
  identical(names(y), c("f", "e", "d"))
)

assert(
  "bkdat::rename() - Empty vectors and lists",
  has_error(rename(data.frame(), c("c" = "f")))
)

# This batch tests the typical renaming scenarios
df <- data.frame(a = 1, b = 2, c = 3)
replace_list <- c("f" = "c", "e" = "b", "f" = "a")
assert(
  "bkdat::rename() - Renaming list with one conflicting variable name",
  has_error(rename(df, x = replace_list))
)

df <- data.frame(a = 1, b = 2, c = 3, d = 4, e = 5)
replace_list <- c("f" = "c", "e" = "b", "g" = "c")
assert(
  "bkdat::rename() - Renaming list with two conflicting variable names - default",
  has_warning(rename(df, replace_list)),
  identical(names(rename(df, replace_list)), c("a", "f", "e", "d", "e"))
)

df <- data.frame(a = 1, b = 2, c = 3)
replace_list <- c("a" = "a")
assert(
  "bkdat::rename() - Renaming to the same value",
  identical(names(rename(df, replace_list)), letters[1:3])
)

assert(
  "bkdat::rename() - Renaming list with an empty renaming vector",
  has_error(rename(df, ""))
)

df <- data.frame(a = 1, b = 2, c = 3)
replace_list <- c("f" = "c", "e" = "b", "c" = "a")
expected_value <- data.frame(f = 1, e = 2, c = 3)
actual_value <- rename(df, replace_list)
assert(
  "bkdat::rename() - Single Swapping (shouldn't cause problems)",
  identical(actual_value, expected_value)
)

df <- data.frame(a = 1, b = 2, c = 3)
replace_list <- c("a" = "c", "z" = "b", "c" = "a")
expected_value <- data.frame(a = 1, z = 2, c = 3)
actual_value <- rename(df, replace_list)
assert(
  "bkdat::rename() - Double Swapping (shouldn't cause problems)",
  identical(actual_value, expected_value)
)

x <- data.frame(a = 1, b = 2, c = 3)
replace_list <- c("d" = "a", "e" = "a", "f" = "a")
expected_value <- data.frame(a = 1, a = 2, a = 3)
assert(
  "bkdat::rename() - Multiple assignments for the same element",
  identical(expected_value, data.frame(a = 1, a.1 = 2, a.2 = 3))
)
