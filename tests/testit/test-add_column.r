library(testit)

df_all <- data.frame(
  a = c(1, 2.5, NA),
  b = c(1:2, NA),
  c = c(T, F, NA),
  d = c("a", "b", NA),
  e = factor(c("a", "b", NA)),
  f = as.Date("2015-12-09") + c(1:2, NA),
  g = as.POSIXct("2015-12-09 10:51:34 UTC") + c(1:2, NA),
  stringsAsFactors = FALSE
)

df_all_new <- add_column(df_all, list(j = 1:3, k = 3:1))
assert(
  "add_column() can add new column",
  identical(nrow(df_all_new), nrow(df_all)),
  identical(df_all_new[seq_along(df_all)], df_all),
  identical(df_all_new$j, 1:3),
  identical(df_all_new$k, 3:1)
)

iris_new <- add_column(iris, list(x = 1:150))
assert(
  "add_column() keeps class of object",
  identical(class(iris), class(iris_new))
)

iris_new <- add_column(iris, list(x = 1:150), after = 3)
assert(
  "add_column() keeps class of object when adding in the middle",
  identical(class(iris), class(iris_new))
)

iris_new <- add_column(iris, list(x = 1:150), after = 0)
assert(
  "add_column() keeps class of object when adding in the beginning",
  identical(class(iris), class(iris_new))
)

assert(
  "add_column() errors if no arguments",
  has_error(add_column(iris))
)

assert("add_column() errors if adding existing columns",
  has_error(add_column(data.frame(a = 3), a = 5))
)

assert("add_column() errors if adding wrong number of rows",
  has_error(add_column(data.frame(a = 3), list(b = 4:5)))
)

df <- data.frame(a = 1:3)
df_new <- add_column(df, list(b = 4:6, c = 3:1))
assert(
  "add_column() can add multiple columns",
  identical(df_new, data.frame(a = 1:3, b = 4:6, c = 3:1))
)

df <- data.frame(a = 1:3)
df_new <- add_column(df, list(b = 4, c = 3:1))
assert(
  "add_column() can recycle when adding columns",
  identical(df_new, data.frame(a = 1:3, b = rep(4, 3), c = 3:1))
)

df <- data.frame(a = 1:3)
df_new <- add_column(df, list(b = 4))
assert(
  "add_column() can recycle when adding a column of length 1",
  identical(df_new, data.frame(a = 1:3, b = rep(4, 3)))
)

df <- data.frame(a = 1:3)
df_new <- add_column(df, list(b = 4, c = 5))
assert(
  "add_column() can recyle when adding multiple columns of length 1",
  identical(df_new, data.frame(a = 1:3, b = rep(4, 3), c = rep(5, 3)))
)

df <- data.frame(a = 1:3)[0, , drop = FALSE]
df_new <- add_column(df, list(b = 4, c = character()))
assert(
  "add_column() can recyle for zero-row data frame",
  identical(df_new, data.frame(a = integer(), b = numeric(), c = character(), stringsAsFactors = FALSE))
)

df <- data.frame(a = 3L)
df_new <- add_column(df, list(b = 2L), before = 1)
assert(
  "add_column() can add as first column via before = 1",
  identical(df_new, data.frame(b = 2L, a = 3L))
)

df <- data.frame(a = 3L)
df_new <- add_column(df, list(b = 2L), after = 0)
assert(
  "add_column() can add as first column via after = 0",
  identical(df_new, data.frame(b = 2L, a = 3L))
)

df <- data.frame(a = 1:3, c = 4:6)
df_new <- add_column(df, list(b = -1:1), after = 1)
assert(
  "add_column() can add column inbetween",
  identical(df_new, data.frame(a = 1:3, b = -1:1, c = 4:6))
)

df <- data.frame(a = 1:3, c = 4:6)
df_new <- add_column(df, list(b = -1:1), before = "c")
assert(
  "add_column() can add column relative to named column",
  identical(df_new, data.frame(a = 1:3, b = -1:1, c = 4:6))
)

df <- data.frame(a = 1:3)
assert(
  "add_column() errors if both before and after are given",
  has_error(add_column(df, list(b = 4:6), after = 2, before = 3))
)

df <- data.frame(a = 1:3)
assert(
  "add_column() errors if column named by before or after not found",
  has_error(add_column(df, list(b = 4:6), after = "x")),
  has_error(add_column(df, list(b = 4:6), before = "x"))
)

assert(
  "add_column() missing row names stay missing when adding column",
  !(.row_names_info(iris) > 0L),
  !(.row_names_info(add_column(iris, list(x = 1:150), after = 0)) > 0L ),
  !(.row_names_info(add_column(iris, list(x = 1:150), after = ncol(iris))) > 0L ),
  !(.row_names_info(add_column(iris, list(x = 1:150), before = 2)) > 0L )
)
