library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
newcars <- mtcars
newcars <- reset_row_names(newcars)
newcars$order <- 1:nrow(newcars)
col_names <- names(newcars)

df <- nest_by(newcars, c("cyl", "carb"))

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "unnest() errors on non-dataframes.",
  has_error(unnest(list()))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# unnest works as expected
#-------------------------------------------------------------------------------
assert(
  "unnest() works as expected.",
  identical(newcars, arrange_(unnest(df)[col_names], "order"))
)
