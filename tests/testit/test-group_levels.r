library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "group_levels() errors on non-dataframes.",
  has_error(group_levels(list()))
)

assert(
  "group_levels() errors if group_cols is not character.",
  has_error(group_levels(data.frame(1), 1))
)


#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# group_levels works as expected
#-------------------------------------------------------------------------------
df <- data.frame(
  a = 1:5,
  b = c("b", "b", "a", "a", "c"),
  c = c("d", "d", "e", "f", "g"),
  stringsAsFactors = FALSE
)

assert(
  "group_levels() works as expected.",
  identical(
    group_levels(df, "b", arrange = TRUE),
    factor(c("b", "b", "a", "a", "c"))
  ),
  identical(
    group_levels(df, c("b", "c"), arrange = TRUE),
    factor(c("b|d", "b|d", "a|e", "a|f", "c|g"))
  ),
  identical(
    group_levels(df, "b", arrange = FALSE),
    factor(c("b", "b", "a", "a", "c"), levels = c("b", "a", "c"))
  ),
  identical(
    group_levels(df, c("b", "c"), arrange = FALSE),
    factor(c("b|d", "b|d", "a|e", "a|f", "c|g"), levels = c("b|d", "a|e", "a|f", "c|g"))
  )
)
