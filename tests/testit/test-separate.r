library(testit)

df <- data.frame(x = c(NA, "a b"), stringsAsFactors = FALSE)
out <- separate(df, "x", c("x", "y"), sep = " ")
assert(
  "separate() missing values in input are missing in output",
  identical(out$x, c(NA, "a")),
  identical(out$y, c(NA, "b"))
)

df <- data.frame(x = c(NA, "ab", "cd"), stringsAsFactors = FALSE)
out <- separate(df, "x", c("x", "y"), 1)
assert(
  "positive integer values specific position between characters",
  identical(out$x, c(NA, "a", "c")),
  identical(out$y, c(NA, "b", "d"))
)

df <- data.frame(x = c(NA, "ab", "cd"), stringsAsFactors = FALSE)
out <- separate(df, "x", c("x", "y"), -1)
assert(
  "negative integer values specific position between characters",
  identical(out$x, c(NA, "a", "c")),
  identical(out$y, c(NA, "b", "d"))
)

df <- data.frame(x = c(NA, "a", "bc", "def"), stringsAsFactors = FALSE)
out <- separate(df, "x", c("x", "y"), 3)
out2 <- separate(df, "x", c("x", "y"), -3)
assert(
  "extreme integer values handled sensibly",
  identical(out$x, c(NA, "a", "bc", "def")),
  identical(out$y, c(NA, "", "", "")),
  identical(out2$x, c(NA, "", "", "")),
  identical(out2$y, c(NA, "a", "bc", "def"))
)

df <- data.frame(x = "1-1.5-FALSE", stringsAsFactors = FALSE)
out <- separate(df, "x", c("x", "y", "z"), "-", convert = TRUE)
assert(
  "convert produces integers etc",
  identical(out$x, 1L),
  identical(out$y, 1.5),
  identical(out$z, FALSE)
)

df <- data.frame(x = "X-1", stringsAsFactors = FALSE)
out <- separate(df, "x", c("x", "y"), "-", convert = TRUE)
assert(
  "convert keeps characters as character",
  identical(out$x, "X"),
  identical(out$y, 1L)
)

df <- data.frame(x = c("a b", "a b c"), stringsAsFactors = FALSE)
merge <- separate(df, "x", c("x", "y"), extra = "merge")
drop <- separate(df, "x", c("x", "y"), extra = "drop")
assert(
  "too many pieces dealt with as requested",
  has_error(separate(df, "x", c("x", "y"))), # Values not split into 2 pieces at 2
  identical(merge[[1]], c("a", "a")),
  identical(merge[[2]], c("b", "b c")),
  identical(drop[[1]], c("a", "a")),
  identical(drop[[2]], c("b", "b"))
)

# df <- data.frame(x = c("a b", "a b c"), stringsAsFactors = FALSE)
# left <- separate(df, "x", c("x", "y", "z"), fill = "left")
# right <- separate(df, "x", c("x", "y", "z"), fill = "right")
# assert(
#   "too few pieces dealt with as requested",
#   has_warning(separate(df, "x", c("x", "y", "z"))),
#   identical(left$x, c(NA, "a")),
#   identical(left$y, c("a", "b")),
#   identical(left$z, c("b", "c")),
#   identical(right$z, c(NA, "c"))
# )

df <- group_by(data.frame(g = 1, x = "a:b", stringsAsFactors = FALSE), "g")
rs <- separate(df, "x", c("a", "b"))
assert(
  "does not preserve grouping",
  !identical(class(df), class(rs))
)

df <- group_by(data.frame(x = "a:b", stringsAsFactors = FALSE), "x")
rs <- separate(df, "x", c("a", "b"))
assert(
  "drops grouping when needed",
  identical(rs$a, "a"),
  identical(class(rs), "data.frame")
)

df <- data.frame(x = "a:b", stringsAsFactors = FALSE)
rs <- separate(df, "x", c("x", "y"))
assert(
  "overwrites existing columns",
  identical(names(rs), c("x", "y")),
  identical(rs$x, "a")
)

df <- data.frame(x = "a:b", stringsAsFactors = FALSE)
assert(
  "checks type of `into` and `sep`",
  has_error(separate(df, "x", "x", FALSE)), # must be either numeric or character
  has_error(separate(df, "x", FALSE)) # must be a character vector
)
