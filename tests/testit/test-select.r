library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
d <- as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1.399, 2.599),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    infinte = rep(c(2, Inf)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1.33, 2))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "select() errors with non-dataframes.",
  has_error(select(list()))
)

assert(
  "select() errors with unknown column names.",
  has_error(select(d, "h")),
  has_error(select(d, c("h", "k")))
)

assert(
  "select() errors with unknown column positions.",
  has_error(select(d, 10:14)),
  has_error(select(d, -10:-14))
)

assert(
  "select() errors with wrong length of logical vector.",
  has_error(select(d, TRUE))
)

assert(
  "select() errors if 'x' is not a character, numeric, or logical vector.",
  has_error(select(d, list()))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# select works as expected.
#-------------------------------------------------------------------------------
assert(
  "select() works as expected.",
  identical(d[, 1, drop = FALSE], select(d, 1)),
  identical(d[, "integer", drop = FALSE], select(d, "integer")),
  identical(d[, "integer", drop = FALSE], select(d, c(F, F, F, F, T, F, F, F, F, F, F)))
)
