library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 2, 1),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "desc() errors with lists.",
  has_error(desc(df$list))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# desc works as expected.
#-------------------------------------------------------------------------------
assert(
  "desc() works as expected.",
  identical(desc(df$POSIXct), c(NA, -1502596800, -1502683200)),
  identical(desc(df$POSIXlt), c(-1502596800, NA, -1502683200)),
  identical(desc(df$Date), c(-17391, -17392, NA)),
  identical(desc(df$numeric), c(NA, -2, -1)),
  identical(desc(df$integer), c(-3L, NA, -4L)),
  identical(desc(df$character), c(-1L, -2L, NA)),
  identical(desc(df$infinite), c(NA, -1, -Inf)),
  identical(desc(df$logical), c(-2L, NA, -1L)),
  identical(desc(df$factor), c(-1L, -2L, NA))
)
