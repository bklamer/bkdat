library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# checking arguments return errors and warnings.
#-------------------------------------------------------------------------------
assert(
  "summarise_() argument 'x' errors with non-character vector.",
  has_error(summarise_(data.frame(), 1))
)

assert(
  "summarise_() argument 'x' errors with non-named character vector.",
  has_error(summarise_(data.frame(), c("a")))
)

#-------------------------------------------------------------------------------
# summarise_ errors with wrong result size
#-------------------------------------------------------------------------------
assert(
  "summarise_() errors with wrong result size.",
  has_error(summarise_(mtcars, c(z = "1:2")))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# summarise_ works with character vectors.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1)
assert(
  "summarise_() works with character vectors.",
  identical(data.frame(z = 2), summarise_(df, c(z = "x + 1")))
)

#-------------------------------------------------------------------------------
# summarise_ works with lists.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1)
assert(
  "summarise_() works with character vectors.",
  identical(data.frame(z = 2), summarise_(df, list(z = "x + 1")))
)

#-------------------------------------------------------------------------------
# summarise_ CANNOT refer to newly created variables if SAME name.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1)
assert(
  "summarise_() CANNOT refer to variables just created if SAME name.",
  has_error(summarise_(df, c(z = "x + 1", z = "z + 1")))
)

#-------------------------------------------------------------------------------
# summarise_ CANNOT refer to newly created variables even if DIFFERENT name.
#-------------------------------------------------------------------------------
assert(
  "summarise_() CAN refer to variables just created if DIFFERENT name.",
  has_error(summarise_(mtcars, c(cyl1 = "cyl + 1", cyl2 = "cyl1 + 1")))
)

#-------------------------------------------------------------------------------
# Two summarise_s are equivalent to one.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1:10, y = 6:15)
df1 <- summarise_(df, c(x2 = "mean(x)", y4 = "mean(y)"))
df2 <- summarise_(df, c(x2 = "mean(x)"))
df3 <- summarise_(df, c(y4 = "mean(y)"))
df4 <- bind_cols(list(df2, df3))
assert(
  "Two summarise_()s are equivalent to one.",
  identical(df1, df4)
)

#-------------------------------------------------------------------------------
# summarise_ handles logical result
#-------------------------------------------------------------------------------
df <- data.frame(x = 1:10, g = rep(c(1, 2), each = 5))
res <- summarise_(group_by(df, "g"), c(r = "any(x > 5)"))
assert(
  "summarise_() handles logical result.",
  identical(res$r, c(F, T))
)

#-------------------------------------------------------------------------------
# summarise_ works with zero row data frames.
#-------------------------------------------------------------------------------
df <- data.frame(a = numeric(0), b = character(0))
res <- summarise_(df, c(a2 = "a * 2"))
assert(
  "summarise_() works with zero row data frames.",
  identical(names(res), c("a2"))
)

#-------------------------------------------------------------------------------
# summarise_ handles order of operations
#-------------------------------------------------------------------------------
df <- data.frame(A = c(2, 2), B = c(1, 1))
res1 <- summarise_(df, c(A = "sum(A)", B = "sum(B)", D = "sum(A)-sum(B)"))
res2 <- summarise_(df, c(A = "sum(A)", B = "sum(B)", D = "sum(A-B)"))
assert(
  "summarise_() handles order of operations.",
  identical(res1, res2)
)
