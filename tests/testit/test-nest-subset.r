library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- nest_by(mtcars, c("cyl", "carb"))

#===============================================================================
# Check errors and warnings.
#===============================================================================

#===============================================================================
# Check other function properties.
#===============================================================================
assert(
  "[.nest_by() works as expected",
  identical(df[1, 1]$cyl, 6),
  identical(df[1, 1, drop = TRUE], 6),
  identical(names(attributes(df[1, 1])), c("names", "row.names", "class")),
  identical(df[], df),
  identical(df[1]$cyl, df$cyl),
  identical(names(attributes(df[1])), c("names", "row.names", "class"))
)

df[1, 1] <- 100
df[2] <- df[2] + 2
assert(
  "[<-.nest_by() works as expected",
  identical(df[1, 1], 100),
  identical(df[1, 2], 6),
  identical(names(attributes(df)), c("names", "row.names", "class")),
  identical(df[], df),
  identical(df[1]$cyl, df$cyl),
  identical(attributes(df[1, 1]), NULL)
)

df <- nest_by(mtcars, c("cyl", "carb"))
df[["cyl"]] <- rep(1, nrow(df))
df[[2]] <- rep(2, nrow(df))
assert(
  "[[<-.nest_by() works as expected",
  identical(df[["cyl"]][1], 1),
  identical(df[[2]][1], 2),
  identical(names(attributes(df)), c("names", "row.names", "class"))
)

df <- nest_by(mtcars, c("cyl", "carb"))
df$cyl <- rep(1, nrow(df))
df$carb <- rep(2, nrow(df))
assert(
  "$<-.nest_by() works as expected",
  identical(df$cyl[1], 1),
  identical(df$carb[1], 2),
  identical(names(attributes(df)), c("names", "row.names", "class"))
)
