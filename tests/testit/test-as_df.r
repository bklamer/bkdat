library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# as_df errors if converting list with different lengths.
#-------------------------------------------------------------------------------
l1 <- list(1, 1:2)

assert(
  "as_df() errors if converting list with different lengths.",
  has_error(as_df(l1))
)

#-------------------------------------------------------------------------------
# as_df errors if converting non-matrix, non-list, non-group_df, or non-nest_df.
#-------------------------------------------------------------------------------
array <- array(data = c(1, 2, 3, 4, 5, 6), dim = c(1, 2, 3))
assert(
  "as_df() errors on non-matrix and non-list objects.",
  has_error(as_df(1L)),
  has_error(as_df("a")),
  has_error(as_df(array))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# as_df handles NA colnames as expected.
#-------------------------------------------------------------------------------
col_names <- c("numeric", NA)
new_col_names <- c("numeric", "V2")
l1 <- list(
  numeric = 1,
  replace = 1L
)
names(l1) <- col_names
m1 <- matrix(1:6, ncol = 2, dimnames = list(NULL, col_names))
ndf <- nest_by(mtcars[, c("carb", "mpg")], c("carb"))
names(ndf) <- col_names
gdf <- group_by(mtcars[, c("carb", "mpg")], c("carb"))
names(gdf) <- col_names

assert(
  "as_df() handles NA colnames as expected.",
  identical(
    new_col_names,
    names(as_df(l1))
  ),
  identical(
    new_col_names,
    names(as_df(m1))
  ),
  identical(
    new_col_names,
    names(as_df(ndf))
  ),
  identical(
    new_col_names,
    names(as_df(gdf))
  )
)

#-------------------------------------------------------------------------------
# as_df handles matrix dimnames as expected.
#-------------------------------------------------------------------------------
m1 <- matrix(1:6, ncol = 2, dimnames = list(1:3, 1:2))
m2 <- matrix(1:6, ncol = 2, dimnames = list(1:3, NULL))
m3 <- matrix(1:6, ncol = 2, dimnames = list(NULL, c("a", "b")))

df1 <- as.data.frame(m1); rownames(df1) <- 1:3
df2 <- as.data.frame(m2); rownames(df2) <- 1:3
df3 <- as.data.frame(m3); rownames(df3) <- 1:3

assert(
  "as_df() handles matrix dimnames as expected.",

  # rownames and colnames
  identical(
    df1,
    as_df(m1)
  ),

  # only rownames
  identical(
    df2,
    as_df(m2)
  ),

  # only colnames
  identical(
    df3,
    as_df(m3)
  )
)

#-------------------------------------------------------------------------------
# as_df handles matrix data types as expected.
#-------------------------------------------------------------------------------
m1 <- matrix(c(NA, 2, 3, 4, 5, 6), ncol = 2)
m2 <- matrix(c(NA, 2:6), ncol = 2)
m3 <- matrix(c(NA, letters[2:6]), ncol = 2)
m4 <- matrix(c(NA, TRUE, FALSE, TRUE, FALSE, TRUE), ncol = 2)
m5 <- matrix(factor(c("a", "b", "c", "d")), ncol = 2)

df1 <- as.data.frame(m1); rownames(df1) <- 1:3
df2 <- as.data.frame(m2); rownames(df2) <- 1:3
df3 <- as.data.frame(m3, stringsAsFactors = FALSE); rownames(df3) <- 1:3
df4 <- as.data.frame(m4); rownames(df3) <- 1:3

assert(
  "as_df() handles matrix data types as expected.",

  # Numeric
  identical(
    df1,
    as_df(m1)
  ),

  # Integer
  identical(
    df2,
    as_df(m2)
  ),

  # Character
  identical(
    df3,
    as_df(m3)
  ),

  # Logical
  identical(
    df4,
    as_df(m4)
  )
)

#-------------------------------------------------------------------------------
# as_df handles list names as expected.
#-------------------------------------------------------------------------------
l1 <- list(
  numeric = c(1, 2),
  integer = 1:2,
  character = c("a", "b"),
  missing = c(NA, NA),
  logical = c(TRUE, FALSE),
  factor = factor(1:2)
)
l2 <- list(
  c(1, 2),
  1:2,
  c("a", "b"),
  c(NA, NA),
  c(TRUE, FALSE),
  factor(1:2)
)
l3 <- list(
  c(1, 2),
  1:2,
  character = c("a", "b"),
  c(NA, NA),
  c(TRUE, FALSE),
  factor(1:2)
)

df1 <- as.data.frame(l1, stringsAsFactors = FALSE); rownames(df1) <- 1:2
df2 <- as.data.frame(l2, stringsAsFactors = FALSE); rownames(df2) <- 1:2; colnames(df2) <- paste0("V", 1:6)
df3 <- as.data.frame(l3, stringsAsFactors = FALSE); rownames(df3) <- 1:2; colnames(df3) <- c("V1", "V2", "character", "V4", "V5", "V6")

assert(
  "as_df() handles list names as expected.",

  # as_df() keeps all list names.
  identical(
    df1,
    as_df(l1)
  ),

  # as_df() generates all new names.
  identical(
    df2,
    as_df(l2)
  ),

  # as_df() puts named element in right spot and generates logical missing names.
  identical(
    df3,
    as_df(l3)
  )
)

#-------------------------------------------------------------------------------
# as_df handles list-columns as expected.
#-------------------------------------------------------------------------------
l1 <- list(
  numeric = 1,
  integer = 1L,
  character = "a",
  missing = NA,
  logical = T,
  factor = factor(1),
  list = list(a = list(data.frame(a = c(1, 2, 3)))), # must have 1 list element.
  data.frame = data.frame(a = c(1), b = c(2)) # must have 1 element per column.
)

assert(
  "as_df() handles list-columns as expected.",

  identical(
    "list",
    class(as_df(l1)[["list"]])
  ),

  identical(
    "data.frame",
    class(as_df(l1)[["data.frame"]])
  )
)

#-------------------------------------------------------------------------------
# as_df is same as as.data.frame without list-columns.
#-------------------------------------------------------------------------------
l <- list(
  POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
  POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
  Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
  numeric = c(NA, 1, 2),
  integer = c(3L, NA, 4L),
  character = c(letters[1:2], NA),
  infinite = c(NA, 1, Inf),
  logical = c(TRUE, NA, FALSE),
  factor = factor(c(letters[1:2], NA))
  #list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
)

d <- as.data.frame(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA))
  ),
  row.names = NULL,
  stringsAsFactors = FALSE
)

assert(
  "as_df() is same as as.data.frame without list-columns.",
  all.equal(d, as_df(l))
)

#-------------------------------------------------------------------------------
# as_df converts matrix-like objects to data.frames
# see implementation in R/spread.r
#-------------------------------------------------------------------------------
x <- factor(c("a", "b", "c", "d"))
dim(x) <- c(2, 2)
colnames(x) <- c("a", "b")
x_df <- data.frame(
  a = factor(c("a", "b"), levels = c("a", "b", "c", "d")),
  b = factor(c("c", "d"), levels = c("a", "b", "c", "d"))
  )
assert(
  "as_df() converts matrix-like objects to data.frames.",
  identical(x_df, as_df(x))
)

#-------------------------------------------------------------------------------
# as_df works as expected with nest_df and group_df
#-------------------------------------------------------------------------------
ndf <- nest_by(mtcars[, c("carb", "mpg")], c("carb"))
gdf <- group_by(mtcars[, c("carb", "mpg")], c("carb"))

assert(
  "as_df() works as expected with nest_df and group_df.",
  identical(6L, nrow(as_df(ndf))),
  identical(32L, nrow(as_df(gdf))),
  identical("data.frame", class(as_df(ndf))),
  identical("data.frame", class(as_df(gdf))),
  identical(c("names", "row.names", "class"), names(attributes(as_df(ndf)))),
  identical(c("names", "row.names", "class"), names(attributes(as_df(gdf))))
)

#-------------------------------------------------------------------------------
# create_col_names works as expected
#-------------------------------------------------------------------------------
col_names <- c("a", "", "b")

assert(
  "create_col_names() works as expected.",
  identical(bkdat:::create_col_names(col_names, 3, "V"), c("a", "V2", "b"))
)
