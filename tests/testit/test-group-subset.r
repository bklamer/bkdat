library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- group_by(mtcars, c("cyl", "carb"))

#===============================================================================
# Check errors and warnings.
#===============================================================================

#===============================================================================
# Check other function properties.
#===============================================================================
assert(
  "[.group_by() works as expected",
  identical(df[1, 1]$mpg, 21),
  identical(df[1, 1, drop = TRUE], 21),
  identical(names(attributes(df[1, 1])), c("names", "row.names", "class")),
  identical(df[], df),
  identical(df[1]$mpg, df$mpg),
  identical(names(attributes(df[1])), c("names", "row.names", "class"))
)

df[1, 1] <- 100
df[2] <- df[2] + 2
assert(
  "[<-.group_by() works as expected",
  identical(df[1, 1], 100),
  identical(df[1, 2], 8),
  identical(names(attributes(df)), c("names", "row.names", "class")),
  identical(df[], df),
  identical(df[1]$mpg, df$mpg),
  identical(attributes(df[1, 1]), NULL)
)

df <- group_by(mtcars, c("cyl", "carb"))
df[["mpg"]] <- rep(1, nrow(df))
df[[2]] <- rep(2, nrow(df))
assert(
  "[[<-.group_by() works as expected",
  identical(df[["mpg"]][1], 1),
  identical(df[[2]][1], 2),
  identical(names(attributes(df)), c("names", "row.names", "class"))
)

df <- group_by(mtcars, c("cyl", "carb"))
df$mpg <- rep(1, nrow(df))
df$cyl <- rep(2, nrow(df))
assert(
  "$<-.group_by() works as expected",
  identical(df$mpg[1], 1),
  identical(df$cyl[1], 2),
  identical(names(attributes(df)), c("names", "row.names", "class"))
)
