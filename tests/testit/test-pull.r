library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1, 2),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1, 2))))
  )
)
df_classes <- list(
  POSIXct = c("POSIXct", "POSIXt"),
  POSIXlt = c("POSIXlt", "POSIXt"),
  Date = "Date",
  numeric = "numeric",
  integer = "integer",
  character = "character",
  missing = "numeric",
  logical = "logical",
  factor = "factor",
  list = "list"
)
col_names <- names(df)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "pull() errors when pulling non-existing column name.",
  has_error(pull(df, "sdjfk"))
)

assert(
  "pull() errors for non-dataframes.",
  has_error(pull(list()))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# pull works as expected and keeps classes.
#-------------------------------------------------------------------------------
# identical also infers class, but will keep as a seperate check for now.
are_identical <- sapply(col_names, function(x) {
  identical(df[[x]], pull(df, x))
  })
pull_classes <- sapply(col_names, function(x) {
  class(pull(df, x))
})

assert(
  "pull() works as expected and keeps classes.",
  all(are_identical),
  identical(pull_classes, df_classes)
)

#-------------------------------------------------------------------------------
# pull returns the last column by default
#-------------------------------------------------------------------------------
assert(
  "pull() returns the last column by default.",
  identical(pull(df), df$list)
)
