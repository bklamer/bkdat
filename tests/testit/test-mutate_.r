library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# checking arguments return errors and warnings.
#-------------------------------------------------------------------------------
l <- list()
d <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))

assert(
  "mutate_() errors with non-dataframes.",
  has_error(mutate_(l))
)

assert(
  "mutate_() warnings with missing x and returns data",
  has_error(identical(mutate_(d), d))
)

assert(
  "mutate_() errors if 'x' is the wrong type",
  has_error(mutate_(d, -1 * d$a)),
  has_error(mutate_(d, list(-1 * d$a)))
)

assert(
  "mutate_() errors if 'x' is not named",
  has_error(mutate_(d, "-1*a")),
  has_error(mutate_(d, c(c = "-1*a", d = "-1*b", "2*b"))),
  has_error(mutate_(d, list("-1*a"))),
  has_error(mutate_(d, list(c = "-1*a", d = "-1*b", "2*b")))
)

#-------------------------------------------------------------------------------
# mutate_ errors if using bad evaluation function.
#-------------------------------------------------------------------------------
assert(
  "mutate_() errors if using bad evaluation function.",
  has_error(mutate_(d, c(mean = "bad_fun(a)")))
)

#-------------------------------------------------------------------------------
# mutate_ errors with wrong result size
#-------------------------------------------------------------------------------
assert(
  "mutate_() errors with wrong result size.",
  has_error(mutate_(mtcars, c(z = "1:2")))
)

#-------------------------------------------------------------------------------
# mutate_ errors with wrong column names
#-------------------------------------------------------------------------------
y <- 3:4
df <- data.frame(x = c(1, 2))
df_env <- list2env(df, parent = emptyenv())
assert(
  "mutate_() errors with wrong column name.",
  has_error(mutate_(df, c(z = "y"), frame = "y")),
  has_error(mutate_(df, c(z = "y"), env = df_env))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# mutate_ works with character vectors.
#-------------------------------------------------------------------------------
d1 <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6), c = c(-1, -2, -3))
d2 <- data.frame(
  a = c(1, 2, 3),
  b = c(4, 5, 6),
  c = c(-1, -2, -3),
  d = c(-4, -5, -6),
  e = c(8, 10, 12)
)
assert(
  "mutate_() works with character vectors.",
  identical(d1, mutate_(d, c(c = "-1*a"))),
  identical(d2, mutate_(d, c(c = "-1*a", d = "-1*b", e = "2*b")))
)

#-------------------------------------------------------------------------------
# mutate_ works with lists.
#-------------------------------------------------------------------------------
d1 <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6), c = c(-1, -2, -3))
d2 <- data.frame(
  a = c(1, 2, 3),
  b = c(4, 5, 6),
  c = c(-1, -2, -3),
  d = c(-4, -5, -6),
  e = c(8, 10, 12)
)
assert(
  "mutate_() works with character vectors.",
  identical(d1, mutate_(d, list(c = "-1*a"))),
  identical(d2, mutate_(d, list(c = "-1*a", d = "-1*b", e = "2*b")))
)

#-------------------------------------------------------------------------------
# mutate_ CANNOT refer to variables just created if SAME name.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1)
out <- mutate_(df, c(z = "x + 1", z = "z + 1"))
assert(
  "mutate_() CANNOT refer to variables just created if SAME name.",
  identical(nrow(out), 1L),
  identical(ncol(out), 2L),
  identical(out$z, 2)
)

#-------------------------------------------------------------------------------
# mutate_ CAN refer to variables just created if DIFFERENT name.
#-------------------------------------------------------------------------------
res <- mutate_(mtcars, c(cyl1 = "cyl + 1", cyl2 = "cyl1 + 1"))
assert(
  "mutate_() CAN refer to variables just created if DIFFERENT name.",
  identical(res$cyl2, mtcars$cyl + 2)
)

#-------------------------------------------------------------------------------
# mutate_ CAN refer to variables just created if DIFFERENT name (group_df).
#-------------------------------------------------------------------------------
gmtcars <- group_by(mtcars, "am")
res <- mutate_(gmtcars, c(cyl1 = "cyl + 1", cyl2 = "cyl1 + 1"))
res <- group_by(res, "am")
res_direct <- mutate_(gmtcars, c(cyl2 = "cyl + 2"))
assert(
  "mutate_() CAN refer to variables just created if DIFFERENT name (group_df).",
  identical(res$cyl2, res_direct$cyl2)
)

#-------------------------------------------------------------------------------
# Two mutate_s are equivalent to one.
#-------------------------------------------------------------------------------
df <- data.frame(x = 1:10, y = 6:15)
df1 <- mutate_(df, c(x2 = "x * 2", y4 = "y * 4"))
df2 <- mutate_(df, c(x2 = "x * 2"))
df2 <- mutate_(df2, c(y4 = "y * 4"))
assert(
  "Two mutate_()s are equivalent to one.",
  identical(df1, df2)
)

#-------------------------------------------------------------------------------
# mutate_ handles logical result
#-------------------------------------------------------------------------------
df <- data.frame(x = 1:10, g = rep(c(1, 2), each = 5))
res <- mutate_(group_by(df, "g"), c(r = "x > 5"))
assert(
  "mutate_() handles logical result.",
  identical(res$r, c(F, F, F, F, F, T, T, T, T, T))
)

#-------------------------------------------------------------------------------
# mutate_ can rename variables
#-------------------------------------------------------------------------------
out <- mutate_(mtcars, c(cyl2 = "cyl"))
assert(
  "mutate_() can rename variables.",
  identical(out$cyl2, mtcars$cyl)
)

#-------------------------------------------------------------------------------
# mutate_ recycles output of length one.
#-------------------------------------------------------------------------------
out1 <- mutate_(mtcars, c(z = "1"))
out2 <- mutate_(mtcars, c(z = "length(mpg)"))
out3 <- mutate_(mtcars, c(z = "TRUE"))
out4 <- mutate_(mtcars, c(z = "'abc'"))
assert(
  "mutate_() recycles output of length one.",
  identical(out1$z, rep(1, nrow(mtcars))),
  identical(out2$z, rep(32L, nrow(mtcars))),
  identical(out3$z, rep(TRUE, nrow(mtcars))),
  identical(out4$z, rep("abc", nrow(mtcars)))
)

#-------------------------------------------------------------------------------
# mutate_ works with zero row data frames.
#-------------------------------------------------------------------------------
df <- data.frame(a = numeric(0), b = character(0))
res <- mutate_(df, c(a2 = "a * 2"))
assert(
  "mutate_() works with zero row data frames.",
  identical(names(res), c("a", "b", "a2"))
)

#-------------------------------------------------------------------------------
# mutate_ works with frame and env
#-------------------------------------------------------------------------------
df <- data.frame(x = c(1, 2))
df_env <- list2env(df, parent = emptyenv())
match <- structure(list(x = c(1, 2), z = c(1, 2)), .Names = c("x", "z"
), row.names = c(NA, -2L), class = "data.frame")
assert(
  "mutate_() works with frame and env.",
  identical(mutate_(df, c(z = "x"), frame = "x"), match),
  identical(mutate_(df, c(z = "x"), env = df_env), match)
)
