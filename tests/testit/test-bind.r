library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# bind returns error on non-dataframes. This checks both the container
# list and it's elements.
#-------------------------------------------------------------------------------
l1 <- list(
  data.frame(a = 1),
  structure(list(a = 1), class = "not_a_dataframe")
)
l2 <- data.frame()

assert(
  "bind_rows() returns error on non-dataframes.",
  has_error(bind_rows(l1)),
  has_error(bind_rows(l2))
)

assert(
  "bind_fill_rows() returns error on non-dataframes.",
  has_error(bind_fill_rows(l1)),
  has_error(bind_fill_rows(l2))
)

assert(
  "bind_cols() returns error on non-dataframes.",
  has_error(bind_cols(l1)),
  has_error(bind_cols(l2))
)

#-------------------------------------------------------------------------------
# bind returns warning if passed an empty list.
#-------------------------------------------------------------------------------
l1 <- list()

assert(
  "bind_rows() returns warning if passed an empty list.",
  has_warning(bind_rows(l1))
)

assert(
  "bind_fill_rows() returns warning if passed an empty list.",
  has_warning(bind_fill_rows(l1))
)

assert(
  "bind_cols() returns warning if passed an empty list.",
  has_warning(bind_cols(l1))
)

#-------------------------------------------------------------------------------
# bind_rows errors if non-equal column names.
#-------------------------------------------------------------------------------
l1 <- list(data.frame(a = 1, b = 2), data.frame(a = 2))
l2 <- list(data.frame(a = 1), data.frame(a = 2, b = 1))

assert(
  "bind_rows() returns error for non-equal dataframes.",
  has_error(bind_rows(l1)),
  has_error(bind_rows(l2))
)

#-------------------------------------------------------------------------------
# bind_cols returns error if cols have different lengths.
#-------------------------------------------------------------------------------
l <- list(data.frame(a = 1), data.frame(b = c(1, 2)))

assert(
  "bind_cols() returns error if cols have different lengths.",
  has_error(bind_cols(l))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# bind_rows() and bind_fill_rows() return same dataframe.
#-------------------------------------------------------------------------------
l <- list(
  as_df(
    list(
      numeric = c(1, 2),
      integer = 1:2,
      character = c("a", "b"),
      missing = c(1, NA),
      logical = c(TRUE, FALSE)
    )
  ),
  as_df(
    list(
      numeric = c(3, 4),
      integer = 3:4,
      character = c("c", "d"),
      missing = c(NA, 2),
      logical = c(FALSE, TRUE)
    )
  )
)

assert(
  "bind_rows() and bind_fill_rows() return same dataframe.",
  identical(
    bind_rows(l),
    bind_fill_rows(l)
  )
)

#-------------------------------------------------------------------------------
# bind_rows() and bind_fill_rows() keep data types.
#-------------------------------------------------------------------------------
types <- unlist(lapply(l[[1]], class), use.names = FALSE)

assert(
  "bind_rows() and bind_fill_rows() keep data types.",
  identical(
    types,
    unlist(lapply(bind_rows(l), class), use.names = FALSE)
  ),
  identical(
    types,
    unlist(lapply(bind_fill_rows(l), class), use.names = FALSE)
  )
)

#-------------------------------------------------------------------------------
# bind_rows and bind_fill_rows work with list-columns.
#-------------------------------------------------------------------------------
l_fl <- list(
  as_df(
    list(
      numeric = c(1, 2),
      integer = 1:2,
      character = c("a", "b"),
      missing = c(1, NA),
      logical = c(TRUE, FALSE),
      factor = factor(c(1, 2)),
      list = list(a = c(8, 10), b = c(8, 10))
    )
  ),
  as_df(
    list(
      numeric = c(3, 4),
      integer = 3:4,
      character = c("c", "d"),
      missing = c(NA, 2),
      logical = c(FALSE, TRUE),
      factor = factor(c("a", "b")),
      list = list(a = c(8, 10), b = c(8, 10))
    )
  )
)

assert(
  "bind_rows() and bind_fill_rows() work with list-column.",
  inherits(bind_rows(l_fl)[["list"]], "list"),
  inherits(bind_fill_rows(l_fl)[["list"]], "list")
)

#-------------------------------------------------------------------------------
# bind_rows works with factors.
#-------------------------------------------------------------------------------
# use as_df(list()) to create list-column dataframes.
l_f <- list(
  as_df(
    list(
      numeric = c(1, 2),
      integer = 1:2,
      character = c("a", "b"),
      missing = c(1, NA),
      logical = c(TRUE, FALSE),
      factor = factor(c(1, 2))
    )
  ),
  as_df(
    list(
      numeric = c(3, 4),
      integer = 3:4,
      character = c("c", "d"),
      missing = c(NA, 2),
      logical = c(FALSE, TRUE),
      factor = factor(c("a", "b"))
    )
  )
)

assert(
  "bind_rows() works with factors.",
  identical(
    bind_rows(l_f)[["factor"]],
    factor(c(1, 2, "a", "b"), levels = c(1, 2, "a", "b"))
  )
)

#-------------------------------------------------------------------------------
# bind_fill_rows converts factors to integers.
#-------------------------------------------------------------------------------
assert(
  "bind_fill_rows() converts factors to integers.",
  identical(
    bind_fill_rows(l_f)[["factor"]],
    c(1L, 2L, 1L, 2L)
  )
)

#-------------------------------------------------------------------------------
# bind_rows works with all data types.
#-------------------------------------------------------------------------------
l <- list(
  as_df(
    list(
      POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
      POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
      Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
      numeric = c(NA, 1, 2),
      integer = c(3L, NA, 4L),
      character = c(letters[1:2], NA),
      infinite = c(NA, 1, Inf),
      logical = c(TRUE, NA, FALSE),
      factor = factor(c(letters[1:2], NA)),
      list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
    )
  ),
  as_df(
    list(
      POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
      POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
      Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
      numeric = c(NA, 1, 2),
      integer = c(3L, NA, 4L),
      character = c(letters[1:2], NA),
      infinite = c(NA, 1, Inf),
      logical = c(TRUE, NA, FALSE),
      factor = factor(c(letters[1:2], NA)),
      list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
    )
  )
)

l2 <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14", NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14", "2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA, "2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2, NA, 1, 2),
    integer = c(3L, NA, 4L, 3L, NA, 4L),
    character = c(letters[1:2], NA, letters[1:2], NA),
    infinite = c(NA, 1, Inf, NA, 1, Inf),
    logical = c(TRUE, NA, FALSE, TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA, letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))), list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

assert(
  "bind_rows() works with all data types.",
  all.equal(
    bind_rows(l),
    l2
  )
)

#-------------------------------------------------------------------------------
# bind_cols works with all data types.
#-------------------------------------------------------------------------------
l <- list(
  data.frame(POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14"))),
  data.frame(POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14"))),
  data.frame(Date = as.Date(c("2017-08-13", "2017-08-14", NA))),
  data.frame(numeric = c(NA, 1, 2)),
  data.frame(integer = c(3L, NA, 4L)),
  data.frame(character = c(letters[1:2], NA), stringsAsFactors = FALSE),
  data.frame(infinite = c(NA, 1, Inf)),
  data.frame(logical = c(TRUE, NA, FALSE)),
  data.frame(factor = factor(c(letters[1:2], NA))),
  as_df(list(list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))))
)

d <- as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 1, 2),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

assert(
  "bind_rows() works with all data types.",
  all.equal(bind_cols(l), d)
)
