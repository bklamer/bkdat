% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/reset_row_names.r
\name{reset_row_names}
\alias{reset_row_names}
\title{Reset data frame row names}
\usage{
reset_row_names(data)
}
\arguments{
\item{data}{A data frame.}
}
\value{
\code{data.frame}
}
\description{
Quickly reset data frame row names to integers.
}
\examples{
#----------------------------------------------------------------------------
# reset_row_names() examples
#----------------------------------------------------------------------------
library(bkdat)

df <- data.frame(a = 1:5)
rownames(df) <- 5:1
rownames(df)

df <- reset_row_names(df)
rownames(df)

}
\seealso{
\code{\link[tibble:rownames]{tibble::remove_rownames()}}
}
