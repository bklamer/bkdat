#' Rename column names
#'
#' Rename column names by name or position.
#'
#' @param data A data frame.
#' @param x A named character vector or named integer vector. For example:
#' `c("new_var" = "old_var")` or `c("new_var" = 1)`.
#'
#' @return `data.frame`
#'
#' @seealso [dplyr::rename()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # rename() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
#' rename(df, c("new_a" = "a"))
#' rename(df, c("new_b" = 2))
#'
#' @export
rename <- function(data, x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  if(is.list(x)) {
    unlist(x, use.names = TRUE)
  }
  unique_x <- unique(names(x))
  if(length(unique_x) != length(x)) {
    stop(
      "Check the 'x' argument in function bkdat::rename(). 'x' has either missing names or duplicated names.",
      call. = FALSE
    )
  }
  is_type_ok <- is.character(x) || is.numeric(x)
  if(!is_type_ok) {
    stop(
      "Check the 'x' argument in function bkdat::rename(). 'x' must be a named character vector or named integer vector",
      call. = FALSE
    )
  }
  if(is.numeric(x) && any(x < 1 || x > ncol(data))) {
    stop(
      "Check the 'x' argument in function bkdat::rename(). 'x' has integer values out of range for the data frame.",
      call. = FALSE
    )
  }
  if(is.character(x) && !all(x %in% names(data))) {
    stop(
      "Check the 'x' argument in function bkdat::rename(). 'x' has original names not found in the data frame.",
      call. = FALSE
    )
  }

  #-----------------------------------------------------------------------------
  # Rename columns
  #-----------------------------------------------------------------------------
  if(is.numeric(x)) {
    names(data)[x] <- names(x)
  } else {
    name_match <- match(names(data), x)
    names(data)[!is.na(name_match)] <- names(x)
  }

  #-----------------------------------------------------------------------------
  # Return data
  #-----------------------------------------------------------------------------
  data
}
