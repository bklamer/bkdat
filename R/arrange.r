#' Arrange data frame rows by columns
#'
#' Arrange data frame rows by columns. Ties can be arranged by adding another
#' column name. Use [bkdat::desc()] to arrange a column in descending
#' order. [bkdat::arrange_()] allows passing variable names as a character vector.
#'
#' \itemize{
#'  \item `NA`s are placed at the end of the data frame.
#'  \item Data frame attributes may be dropped.
#'  \item There are no checks for duplicate column names. The first occurrence
#'  will be sorted on if so.
#'  \item List-columns cannot be arranged on. If you are arranging on a
#'  `nest_df`, then data frame columns inside the list-column can also be
#'  arranged on.
#'  \item If no variables are supplied to arrange on, then it will return an
#'  error.
#' }
#'
#' To limit unwanted namespace conflicts, the expression is evaluated in an
#' environment that only contains the data frame and the functions
#' [base::order()] and [bkdat::desc()].
#'
#' @param .data,data A data frame.
#' @param ... Unquoted column names to sort the data by. Use [bkdat::desc()]
#' to arrange in descending order. Or further arguments passed to or from other methods.
#' @param x A character vector of column names to sort the data by. Use
#' [bkdat::desc()] to arrange in descending order.
#' @param .by_group,by_group `TRUE` or `FALSE`. Applies to `group_df` data frames.
#' If `TRUE`, then the data frame will be arranged by the grouping
#' variables first.
#'
#' @return data.frame
#'
#' @seealso [base::order()],
#'          [dplyr::arrange()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # arrange() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df1 <- data.frame(a = c(1, 2, 3), b = c(6, 5, 4))
#' df2 <- data.frame(a = c(1, 2, 2), b = c(4, 6, 5))
#'
#' # Arrange by 'a' and then by descending 'a'.
#' arrange_(df1, "a")
#' arrange_(df1, "desc(a)")
#'
#' arrange(df1, a)
#' arrange(df1, desc(a))
#'
#' # Arrange by descending 'a' while sorting ties with 'b'.
#' arrange_(df2, c("desc(a)", "b"))
#' arrange(df2, desc(a), b)
#'
#' @export
arrange <- function(.data, ...) {
  UseMethod("arrange")
}

#' @export
#' @rdname arrange
arrange.data.frame <- function(.data, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------

  #-----------------------------------------------------------------------------
  # Create row ordering vector by evaluating the dots.
  #-----------------------------------------------------------------------------
  row_order <- withCallingHandlers(
    eval(
      expr = substitute(order(...)),
      envir = .data,
      enclos = arrange_env
    ),
    error = function(e) stop(
      "Check the dots argument in bkdat::arrange(). An error occured while evaluating the expression '",
      paste0("order(", paste0(dots_char(...), collapse = ", "), ")"),
      "'. Check for wrong column names, sorting on list-columns, or other typos.",
      call. = FALSE
    )
  )

  if(length(row_order) != nrow_df(.data)) {
    stop(
      "Check the dots argument in function bkdat::arrange(). After evaluating '",
      paste0("order(", paste0(dots_char(...), collapse = ", "), ")"),
      "', the length of the ordering vector didn't match the data frame row length."
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  .data <- .data[row_order, , drop = FALSE]
  .data <- reset_row_names(.data)
  .data
}

#' @export
#' @rdname arrange
arrange.group_df <- function(.data, ..., .by_group = FALSE) {
  if(.by_group) {
    dots <- dots_char(...)
    x <- c(attr(.data, "group_cols"), dots)
    arrange_.data.frame(.data, x)
  } else {
    NextMethod()
  }
}

#' @export
#' @rdname arrange
arrange.nest_df <- function(.data, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------

  # fixme

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  .data
}
