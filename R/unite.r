#' Unite multiple columns into one
#'
#' Unites multiple columns into one character column.
#'
#' The new column will be inserted into the data frame where the first `old`
#' column is located.
#'
#' @param data A data frame.
#' @param new A string for the new column name.
#' @param old A character vector of column names to unite.
#' @param sep A string for the separator between values.
#' @param remove `TRUE` or `FALSE`. If `TRUE`, old columns will be removed.
#'
#' @return `data.frame`
#'
#' @seealso [tidyr::unite()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # unite() examples.
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(
#'   a = letters[1:3],
#'   b = letters[4:6],
#'   c = letters[7:9],
#'   stringsAsFactors = FALSE
#' )
#' unite(df, "new_col", c("a", "b"), sep = "_", remove = TRUE)
#' unite(df, "new_col", c("c", "b"), sep = "-", remove = FALSE)
#'
#' @export
unite <- function(data, new, old, sep = "_", remove = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_string(new)
  check_character(old)
  check_string(sep)
  check_logical(remove)

  if(any(new %in% names(data)) && !remove) {
   stop(
     "Check the 'new' argument in function bkdat::unite(). The name \"",
     paste0(new),
     "\" is already located in the data frame and cannot be used for 'new'.",
     call. = FALSE
   )
  }
  #-----------------------------------------------------------------------------
  # Unite columns
  #-----------------------------------------------------------------------------
  # find a better way to do this
  if(remove && any(new %in% old)) {
    replace <- match(old, names(data))
    replace <- replace[!is.na(replace)]
    old <- paste0(old, "!!!deleteme!!!")
    names(data)[replace] <- old
  }

  new_col <- list(do.call(paste, c(data[old], sep = sep)))
  names(new_col) <- new
  data <- add_column(data, new_col, before = old[1])

  if(remove) {
    remove <- match(old[old != new], names(data))
    remove <- remove[!is.na(remove)]
    data <- data[names(data)[-remove]]
  }

  #-----------------------------------------------------------------------------
  # Return data
  #-----------------------------------------------------------------------------
  data
}
