#' Nested Data Frames
#'
#' Create a data frame with a list-column.
#'
#' @param data A data frame.
#' @param group_cols A character vector of variable names to group by (variables kept that won't be included in the list-column).
#' @param nest_cols A character vector of variables names to nest (variables kept that will be included in the list-column). Defaults to every variable not being grouped on.
#' @param nest_name A character string for the list-column name. Defaults to "data".
#'
#' @return `nest_df`
#'
#' @seealso [tidyr::nest()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # nest_by() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- nest_by(mtcars, group_cols = c("cyl", "carb"))
#' df
#'
#' @export
nest_by <- function(data, group_cols, nest_cols = NULL, nest_name = "data") {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_character(group_cols)

  if(!is.null(nest_cols) && !inherits(nest_cols, "character")) {
    stop(
      "Check the 'nest_cols' argument in function bkdat::nest_by(). 'nest_cols' has class ",
      pretty_print(class(nest_cols)),
      ", but needs to be a character vector."
    )
  }

  check_string(nest_name)

  #-----------------------------------------------------------------------------
  # Create list-column data frame
  #-----------------------------------------------------------------------------
  if(is.null(nest_cols)) {
    nest_cols <- setdiff(names(data), group_cols)
  }
  group_labels <- group_labels(data, group_cols, arrange = FALSE)
  res <- group_labels
  idx <- group_levels(data = data, group_cols = group_cols, arrange = FALSE)
  # If there are NAs in idx, convert to character, otherwise split will exclude
  # those rows.
  if(anyNA(attr(idx, "levels"))) {
    idx <- paste0(idx)
  }
  res[[nest_name]] <- unname(split(data[nest_cols], idx, drop = TRUE, lex.order = FALSE))
  # reset rownames inside each dataframe within the list column
  res[[nest_name]] <- lapply(res[[nest_name]], reset_row_names)

  #-----------------------------------------------------------------------------
  # Set attributes
  #-----------------------------------------------------------------------------
  ## Create needed values
  nest_nrow <- vapply(res[[nest_name]], nrow, FUN.VALUE = integer(1), USE.NAMES = FALSE)
  ## Set nest specific attributes
  attr(res, which = "nest_cols") <- nest_cols
  attr(res, which = "nest_name") <- nest_name
  attr(res, which = "group_cols") <- group_cols
  attr(res, which = "nest_nrow") <- nest_nrow
  attr(res, which = "group_labels") <- group_labels

  # Set other nest_by attributes
  res <- reset_row_names(res)
  attr(res, "class") <- unique(c("nest_df", class(res)))

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' Unnest
#'
#' Returns an unnested data frame, i.e. each element of the list-column is
#' converted back into a separated column data frame.
#'
#' @param data A nested data frame.
#' @param cols A character vector of list-column names. Used when the data frame
#' with list-columns was not created with `nest_by()` or lost it's `nest_df` class.
#'
#' @return `data.frame`
#'
#' @seealso [tidyr::unnest()]
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # unnest() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' @export
unnest <- function(data, cols = NULL){
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  if(is.null(cols)) {
    check_nest_df(data)
  } else {
    check_data_frame(data)
    check_character(cols)
    check_name_in_df(name = cols, df = data)
  }

  #-----------------------------------------------------------------------------
  # Re-build data frame
  #-----------------------------------------------------------------------------
  # Find list-column
  if(is.null(cols)) {
    is_list_col <- names(data) %in% attr(data, "nest_name")
  } else (
    is_list_col <- names(data) %in% cols
  )

  # Set data as data.frame so data.frame S3 methods are used.
  attr(data, "class") <- "data.frame"
  # Subset to just non list-columns. (group labels plus maybe others that were
  # added)
  all_labels <- data[!is_list_col]
  # Expand all_labels to final nrow size by filling in values as needed.
  if(is.null(cols)) {
    nest_nrow <- attr(data, "nest_nrow")
  } else {
    nest_nrow <- vapply(X = data[[cols]], FUN = nrow_df, FUN.VALUE = NA_integer_, USE.NAMES = FALSE)
  }

  res <- as_df(lapply(all_labels, function(x){rep(x, nest_nrow)}))
  # Convert list-column to data.frame and append to result.
  df_cols <- bind_rows(data[is_list_col][[1]])
  res <- bind_cols(list(res, df_cols))

  #-----------------------------------------------------------------------------
  # Reset attributes
  #-----------------------------------------------------------------------------
  names <- names(res)
  row_names <- .set_row_names(nrow_df(res))
  attributes(res) <- NULL
  attr(res, "names") <- names
  attr(res, "row.names") <- row_names
  attr(res, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' Verify attributes of a nested data frame
#'
#' Verify the attributes of a nested data frame. Returns TRUE if attributes are
#' ok. Returns FALSE if any attributes are wrong.
#'
#' @param data A nested data frame.
#'
#' @return `TRUE` or `FALSE`
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' #  examples
#' #----------------------------------------------------------------------------
#'
#' @noRd
is_valid_nest_df <- function(data) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_nest_df(data)

  #-----------------------------------------------------------------------------
  # Check nest_df attributes
  #-----------------------------------------------------------------------------
  # check for a single list-column
  is_list_col <- vapply(
    data,
    FUN = function(x) {inherits(x, what = "list")},
    FUN.VALUE = logical(1),
    USE.NAMES = FALSE
  )
  if(sum(is_list_col) != 1) {
    return(FALSE)
  }

  # check if grouping variables were removed.
  col_names <- names(data)
  attr_group_cols <- attr(data, "group_cols")
  if(!all(attr_group_cols %in% col_names)) {
    return(FALSE)
  }

  # check if nest name was changed
  nest_name <- col_names[is_list_col]
  attr_nest_name <- attr(data, "nest_name")
  if(nest_name != attr_nest_name) {
    return(FALSE)
  }

  # check if nest elements are valid
  ## data.frames
  are_df <- vapply(
    data[[col_names[is_list_col]]],
    FUN = function(x) {inherits(x, "data.frame")},
    FUN.VALUE = logical(1),
    USE.NAMES = FALSE
  )
  if(!all(are_df)) {
    return(FALSE)
  }

  ## names
  nest_df_names <- unique(
    unlist(
      lapply(data[[col_names[is_list_col]]], names)
    )
  )
  ### or use identical() here, check which is faster
  if(length(nest_df_names) != length(attr(data, "nest_cols"))) {
    return(FALSE)
  }
  if(!all(attr(data, "nest_cols") == unique(c(nest_df_names)))) {
    return(FALSE)
  }

  ## nest data frame nrows
  nest_df_nrow <- vapply(
    data[[col_names[is_list_col]]],
    nrow,
    FUN.VALUE = integer(1),
    USE.NAMES = FALSE
  )
  if(!identical(nest_df_nrow, attr(data, "nest_nrow"))) {
    return(FALSE)
  }

  # check if nest labels are ok
  label_df <- unique(as_df(lapply(attr_group_cols, function(x) {data[[x]]})))
  names(label_df) <- attr_group_cols
  label_df <- arrange(label_df, attr_group_cols)
  label_df <- reset_row_names(label_df)
  if(!identical(label_df, attr(data, "group_labels"))) {
    return(FALSE)
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  TRUE
}

#' @export
`[.nest_df` <- function(x, i, j, drop = FALSE) {
  #-----------------------------------------------------------------------------
  # `[` does not drop class attributes (but does drop everything else), so we
  # need to reset class to just "data.frame" before (not after) calling
  # NextMethod() since depending on 'drop', the result could be a data.frame or
  # vector.
  #
  # nargs() returns 2 for x[i] and greater than two for x[i, j], x[, j], etc.
  # This is needed as you don't want drop for x[i], and x[] shouldn't drop
  # anything.
  #-----------------------------------------------------------------------------
  if(nargs() > 2) {
    attr(x, "class") <- "data.frame"
    # Return
    NextMethod(drop = drop)
  } else {
    if(missing(i)) {
      # Return
      x
    } else {
      attr(x, "class") <- "data.frame"
      # Return
      NextMethod()
    }
  }
}

#' @export
`[<-.nest_df` <- function(x, i, j, value) {
  #-----------------------------------------------------------------------------
  # `[<-` does not drop any attributes, so we will need to do that manually.
  #-----------------------------------------------------------------------------
  attr(x, "class") <- "data.frame"
  attributes(x) <- attributes(x)[c("names", "row.names", "class")]

  # Return
  NextMethod()
}

#' @export
`[[<-.nest_df` <- function(x, i, j, value) {
  #-----------------------------------------------------------------------------
  # `[[<-` does not drop any attributes, so we will need to do that manually.
  #-----------------------------------------------------------------------------
  attr(x, "class") <- "data.frame"
  attributes(x) <- attributes(x)[c("names", "row.names", "class")]

  # Return
  NextMethod()
}

#' @export
`$<-.nest_df` <- function(x, name, value) {
  #-----------------------------------------------------------------------------
  # `$<-` does not drop any attributes, so we will need to do that manually.
  #-----------------------------------------------------------------------------
  attr(x, "class") <- "data.frame"
  attributes(x) <- attributes(x)[c("names", "row.names", "class")]

  # Return
  NextMethod()
}
