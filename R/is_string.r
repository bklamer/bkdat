#' Is object a string vector
#'
#' Returns `TRUE` if vector is a string (character vector of length 1).
#'
#' @param x An atomic vector.
#'
#' @return `TRUE` or `FALSE`
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # is_string() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' c <- c("a", "b")
#' s <- "a"
#' is_string(c)
#' is_string(s)
#'
is_string <- function(x) {
  is.character(x) && length(x) == 1
}
