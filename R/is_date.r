#' Is object a date vector
#'
#' Returns `TRUE` if vector is a date.
#'
#' @param x An object.
#'
#' @return `TRUE` or `FALSE`
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # is_date() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' d <- as.Date("2019-01-01")
#' is_date(d)
#'
is_date <- function(x) {
  inherits(x, 'Date') || inherits(x, 'POSIXct')
}
