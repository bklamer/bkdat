#' Apply functions to elements of a vector
#'
#' Apply functions to elements of a vector
#'
#' @param x A data object.
#' @param fun A function.
#' @param ... Additional arguments to pass to `fun`.
#' @param .if A logical vector the same length as `x` or a function that will evaluate to a logical vector the same length as `x`. Elements that are `TRUE` will have `fun` applied to them.
#' @param .at A character vector of names to apply `fun` to.
#'
#' @return `data.frame`
#'
#' @seealso [base::lapply()],
#'          [purrr::map()],
#'          [base::Control]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # map*() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' @name map
NULL

#' @export
#' @rdname map
map <- function(x, fun, ..., .if, .at) {
  lapply(X = x, FUN = fun, ...)
}

#' @export
#' @rdname map
map_chr <- function(x, fun, ..., .if, .at) {
  vapply(X = x, FUN = fun, ..., FUN.VALUE = character(1), USE.NAMES = TRUE)
}

#' @rdname map
#' @export
map_int <- function(x, fun, ..., .if, .at) {
  vapply(X = x, FUN = fun, ..., FUN.VALUE = integer(1), USE.NAMES = TRUE)
}

#' @rdname map
#' @export
map_dbl <- function(x, fun, ..., .if, .at) {
  vapply(X = x, FUN = fun, ..., FUN.VALUE = double(1), USE.NAMES = TRUE)
}

#' @rdname map
#' @export
map_df <- function(x, fun, ..., .if, .at) {
  out <- lapply(X = x, FUN = fun, ...)
  bind_rows(out)
}
