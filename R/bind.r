#' Bind multiple data frames by row or column
#'
#' Bind multiple data frames by row or column.
#' These functions are similar to `do.call("rbind", list)` or
#' `do.call("cbind", list)`. See details for further usage.
#'
#' `bind_rows()` is used for a list of data frames that have the same
#' column names. `bind_fill_rows()` is used for a list of data frames that
#' have different column names (filling in missing rows with NA). See below for
#' how each handles column types:
#'
#' \tabular{llllll}{
#' \bold{Function}       \tab\bold{Factor}       \tab\bold{List}  \tab\bold{Date}         \tab\bold{POSIXct}      \tab\bold{POSIXlt} \cr
#' \code{bind_rows}      \tab Supported          \tab Supported   \tab Supported          \tab Supported          \tab Supported     \cr
#' \code{bind_fill_rows} \tab Coerced to integer \tab Supported   \tab Coerced to integer \tab Coerced to integer \tab Error         \cr
#' }
#'
#' `bind_rows()` is faster than `do.call("rbind", list)`, but much
#' slower than [dplyr::bind_rows()] and [data.table::rbindlist()] for
#' large lists. `bind_fill_rows()` is slower.
#'
#' `bind_cols()` produces the same results as `do.call("cbind", list)`,
#' but requires that each data frame have the same number of rows.
#'
#' Before use, ensure data types are compatible per column and that each column
#' has a proper name. `bind_rows()` will convert factors to character if
#' columns of the same name have mixed character/factor types. If the same
#' column names have different factor levels or level ordering then test output
#' before using.
#'
#' @param x A list of data frames.
#'
#' @return `data.frame`
#'
#' @seealso [base::do.call()],
#'          [base::rbind()],
#'          [base::cbind()],
#'          [data.table::rbindlist()],
#'          [dplyr::bind_rows()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # bind() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' l1 <- list(
#'   data.frame(a = c(1, 2)),
#'   data.frame(a = c(3, 4))
#' )
#' l2 <- list(
#'   data.frame(a = c(1, 2)),
#'   data.frame(b = c(3, 4))
#' )
#' l3 <- list(
#'   as_df(
#'     list(
#'       POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
#'       POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
#'       Date = as.Date(c("2017-08-13", "2017-08-14")),
#'       numeric = c(1, 2),
#'       integer = 3:4,
#'       character = letters[1:2],
#'       missing = rep(c(1, NA)),
#'       logical = rep(c(TRUE, FALSE)),
#'       factor = factor(letters[1:2]),
#'       list = replicate(2, list(a=data.frame(b = c(1, 2))))
#'     )
#'   ),
#'   as_df(
#'     list(
#'       POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
#'       POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
#'       Date = as.Date(c("2017-08-13", "2017-08-14")),
#'       numeric = c(1, 2),
#'       integer = 3:4,
#'       character = letters[1:2],
#'       missing = rep(c(1, NA)),
#'       logical = rep(c(TRUE, FALSE)),
#'       factor = factor(letters[1:2]),
#'       list = replicate(2, list(a=data.frame(b = c(1, 2))))
#'     )
#'   )
#' )
#'
#' bind_rows(l1)
#' bind_fill_rows(l2)
#' bind_rows(l3)
#'
#' @name bind
NULL

#' @export
#' @rdname bind
bind_rows <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments.
  #-----------------------------------------------------------------------------
  check_list(x)

  x_length <- length(x)
  if(x_length == 0) {
    warning("Check the 'x' argument in function bkdat::bind_rows(). 'x' was empty and the empty list was returned.")
    return(x)
  }
  # First clean the list (remove NULLs)
  x <- clean_list(x)
  # Then check if there are any non-dataframes in the list.
  are_dataframes <- vapply(x, FUN = function(x) {inherits(x, "data.frame")}, FUN.VALUE = logical(1L), USE.NAMES = FALSE)
  if(any(!are_dataframes)) {
    stop("Check the 'x' argument in function bkdat::bind_rows(). All elements of 'x' need to be a data frame.")
  }
  # Check if all dataframes have same names. see setequal().
  first_df_colnames <- names(x[[1]])
  for(i in seq_len(x_length)) {
    if(
      anyNA(match(first_df_colnames, names(x[[i]]))) ||
      anyNA(match(names(x[[i]]), first_df_colnames))
    ) {
      stop("Check the 'x' argument in function bkdat::bind_rows(). The data frames in 'x' have different column names. Try using bkdat::bind_fill_rows() instead.")
    }
  }

  #-----------------------------------------------------------------------------
  # Map() will apply this function to every column name and return a column
  # vector inside a named list. as_df() will then convert this list to a
  # data.frame.
  #-----------------------------------------------------------------------------
  # To make this work with dates, factors and list columns, we will need to
  # 'smartly' combine with c() or unlist(). The obvious option:
  # unlist(lapply(list, `[[`, col_name), use.names = TRUE)
  # fails with dates and list-columns but works with factors.
  # c(, recursive = FALSE) fails with factors but works with dates and
  # list-columns.
  extract_cols <- function(list) {
    function(col_name) {
      list_of_extracts <- lapply(list, `[[`, col_name)
      # Fix character/factor coercion
      vector_of_classes <- unlist(lapply(list_of_extracts, class), use.names = FALSE)
      if(any(vector_of_classes == "factor") && any(vector_of_classes == "character")) {
        list_of_extracts <- lapply(list_of_extracts, as.character)
        warning(
          "There was factor to character conversion while binding rows.",
          call. = FALSE
        )
      }
      # Fix combine strategy for different data types
      c_types <- c("Date", "POSIXct", "POSIXlt", "list")
      if(any(match(c_types, vector_of_classes, nomatch = 0L) > 0L)) {
        vector_of_extracts <- do.call(what = "c", args = list_of_extracts)
      } else {
        vector_of_extracts <- unlist(list_of_extracts, use.names = TRUE)
      }
    }
  }

  #-----------------------------------------------------------------------------
  # Bind rows, convert to dataframe, and return.
  #-----------------------------------------------------------------------------
  as_df(Map(f = extract_cols(list = x), first_df_colnames))
}

#' @export
#' @rdname bind
bind_fill_rows <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments.
  #-----------------------------------------------------------------------------
  check_list(x)

  if(length(x) == 0) {
    warning("Check the 'x' argument in function bkdat::bind_fill_rows(). 'x' was empty and the empty list was returned.")
    return(x)
  }
  # First clean the list (remove NULLs)
  x <- clean_list(x)
  # Then check if there are any non-dataframes in the list.
  are_dataframes <- vapply(x, FUN = function(x) {inherits(x, "data.frame")}, FUN.VALUE = logical(1L), USE.NAMES = FALSE)
  if(any(!are_dataframes)) {
    stop("Check the 'x' argument in function bkdat::bind_fill_rows(). All elements of 'x' need to be a data frame.")
  }

  #-----------------------------------------------------------------------------
  # Create pre-allocated matrix of correct size and convert to dataframe.
  #-----------------------------------------------------------------------------
  # Column names of dataframes.
  col_names_list <- lapply(x, function(x) names(x))
  unique_col_names  <- unique(unlist(col_names_list, use.names = FALSE))
  # Number of rows in each dataframe and the number of rows of the final
  # dataframe.
  n_rows <- unlist(lapply(x, nrow_df), use.names = FALSE)
  N_rows <- sum(n_rows)
  # pre-allocated dataframe.
  df <- as_df(matrix(NA, nrow = N_rows, ncol = length(unique_col_names)))
  colnames(df) <- unique_col_names

  #-----------------------------------------------------------------------------
  # Place each individual dataframe where it belongs in combined dataframe.
  #-----------------------------------------------------------------------------
  # starting position and length for each dataframe.
  start_stop <- matrix(c(cumsum(n_rows) - n_rows + 1, n_rows), ncol = 2)
  for (i in seq_along(n_rows)) {
    row_index <- seq(start_stop[i, 1], length.out = start_stop[i, 2])
    df[row_index, col_names_list[[i]]] <- x[[i]]
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  df
}

#' @export
#' @rdname bind
bind_cols <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments.
  #-----------------------------------------------------------------------------
  check_list(x)

  if(length(x) == 0) {
    warning("Check the 'x' argument in bkdat::bind_cols(). 'x' was empty and the empty list was returned.")
    return(x)
  }
  # First clean the list (remove NULLs)
  x <- clean_list(x)
  # Then check if there are any non-dataframes in the list.
  are_dataframes <- vapply(x, FUN = function(x) {inherits(x, "data.frame")}, FUN.VALUE = logical(1L), USE.NAMES = FALSE)
  if(any(!are_dataframes)) {
    stop("Check the 'x' argument in function bkdat::bind_cols(). All elements of 'x' need to be a data frame.")
  }
  # Check if the dataframes in 'x' are all of the same size. If they aren't,
  # then do.call("cbind") will fill in missing spaces, which is unwanted
  # behaviour. Stop that scenario with this check.
  df_nrow <- vapply(x, FUN = nrow_df, FUN.VALUE = integer(1L))
  if(length(unique(df_nrow)) != 1L) {
    stop("Check the 'x' argument in function bkdat::bind_cols(). The dataframes in 'x' do not all have the same number of rows.")
  }

  #-----------------------------------------------------------------------------
  # Bind cols, convert to dataframe, and return.
  #-----------------------------------------------------------------------------
  do.call("cbind", x)
}
