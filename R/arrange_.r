#' @export
#' @rdname arrange
arrange_ <- function(data, x, ...) {
  UseMethod("arrange_")
}

#' @export
#' @rdname arrange
arrange_.data.frame <- function(data, x, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  # First check if x is character
  check_character(x)

  # Clean the character vector of "desc()" so we can get raw column names.
  is_start <- startsWith(x, "desc(")
  is_end <- endsWith(x, ")")
  is_both <- is_start & is_end
  x_no_desc <- x
  x_no_desc[is_both] <- gsub("^desc\\(|\\)$", "", x[is_both])

  # Last, check if column names are in the dataframe
  x_tmp <- x
  x <- x_no_desc
  check_name_in_df(name = x, df = data)
  x <- x_tmp

  #-----------------------------------------------------------------------------
  # Create row ordering vector by evaluating the 'x' argument.
  #-----------------------------------------------------------------------------
  row_order <- withCallingHandlers(
    eval(
      expr = parse(
        file = stdin(),
        n = NULL,
        text = paste0("order(", paste0(x, collapse = ", "), ")"),
        prompt = "?",
        keep.source = FALSE,
        srcfile = NULL,
        encoding = "unknown"
      ),
      envir = data,
      enclos = arrange_env
    ),
    error = function(e) stop(
      "Check the 'x' argument in bkdat::arrange_(). An error occured while evaluating the expression '",
      paste0("order(", paste0(x, collapse = ", "), ")"),
      "'. Check for wrong column names, sorting on list-columns, or other typos.",
      call. = FALSE
    )
  )

  if(length(row_order) != nrow_df(data)) {
    stop(
      "Check the 'x' argument in function bkdat::arrange_(). After evaluating '",
      paste0("order(", paste0(x, collapse = ", "), ")"),
      "', the length of the ordering vector didn't match the data frame row length."
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data <- data[row_order, , drop = FALSE]
  data <- reset_row_names(data)
  data
}

#' @export
#' @rdname arrange
arrange_.group_df <- function(data, x, by_group = FALSE, ...) {
  if(by_group) {
    x <- c(attr(data, "group_cols"), x)
    arrange_.data.frame(data, x)
  } else {
    NextMethod()
  }
}

#' @export
#' @rdname arrange
arrange_.nest_df <- function(data, x, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  # Since x may reference nest-cols, just check for character vector
  check_character(x)

  # Clean the character vector of "desc()" so we can get raw column names
  # and evaluate for special nest cases.
  is_start <- startsWith(x, "desc(")
  is_end <- endsWith(x, ")")
  is_both <- is_start & is_end
  x_no_desc <- x
  x_no_desc[is_both] <- gsub("^desc\\(|\\)$", "", x[is_both])

  #-----------------------------------------------------------------------------
  # Arrange can be applied inside or outside the list-column.
  #-----------------------------------------------------------------------------
  nest_names <- attr(data, "nest_cols")
  non_nest_names <- attr(data, "group_cols")
  inside_names <- x[x_no_desc %in% nest_names]
  outside_names <- x[x_no_desc %in% non_nest_names]

  #-----------------------------------------------------------------------------
  # Apply arrange_() to columns outside the list-column.
  #-----------------------------------------------------------------------------
  if(length(outside_names) > 0) {
    data <- arrange_.data.frame(data, outside_names)
  }

  #-----------------------------------------------------------------------------
  # Apply arrange_() inside the list-column on each element.
  # '...' is needed as a list-column could contain data.frames or group_dfs.
  #-----------------------------------------------------------------------------
  if(length(inside_names) > 0) {
    nest_name <- attr(data, "nest_name")
    data[[nest_name]] <- lapply(
      data[[nest_name]],
      arrange_,
      x = inside_names,
      ...
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}
