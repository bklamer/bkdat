#' @export
#' @rdname mutate
mutate_ <- function(data, x, frame = NULL, env = parent.frame(), ...) {
UseMethod("mutate_")
}

#' @export
#' @rdname mutate
mutate_.data.frame <- function(data, x, frame = NULL, env = parent.frame(), ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_named_character(x, check_missing = TRUE)
  check_frame(data, frame)

  #-----------------------------------------------------------------------------
  # Perform mutation
  #-----------------------------------------------------------------------------
  data_nrow <- nrow_df(data)
  for(i in names(x)) {
    new_col <- withCallingHandlers(
      eval(
        expr = parse(
          file = stdin(),
          n = NULL,
          text = x[[i]],
          prompt = "?",
          keep.source = FALSE,
          srcfile = NULL,
          encoding = "unknown"
        ),
        # Looping over subset operation is slow, but I don't have a faster solution.
        envir = if(is.null(frame)) {data} else {data[, frame, drop = FALSE]},
        enclos = env
      ),
      error = function(e) stop(
        "Check the 'x' argument in function bkdat::mutate_(). An error occured while evaluating the expression '",
        x[[i]],
        "'. You might have used non-existing column names or wrongly-formed functions.",
        call. = FALSE
      )
    )
    # We want to propogate constants, but not anything else that is not the
    # length of the dataframe.
    new_col_length <- length(new_col)
    if(new_col_length != data_nrow && new_col_length != 1) {
      stop(
        "Check the 'x' argument in function bkdat::mutate_(). After evaluating '",
        x[[i]],
        "', the length of this new column doesn't match the number of rows in the data frame."
      )
    }
    data[[i]] <- new_col
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}

#' @export
#' @rdname mutate
mutate_.group_df <- function(data, x, frame = NULL, env = parent.frame(), ...) {
  NextMethod()
}

#' @export
#' @rdname mutate
mutate_.nest_df <- function(data, x, frame = NULL, env = parent.frame(), list_column = TRUE, ...) {
  if(list_column) {
    #-----------------------------------------------------------------------------
    # apply mutate_.data.frame() to each list-column element.
    #-----------------------------------------------------------------------------
    nest_name <- attr(data, "nest_name")
    label_df <- attr(data, "group_labels")
    list_column <- bind_rows(
      lapply(
        data[[nest_name]],
        mutate_,
        x = x,
        frame = frame,
        env = env
      )
    )

    # Return
    cbind(label_df, list_column)
  } else {
    NextMethod()
  }
}
