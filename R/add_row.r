#' Add rows to a data frame
#'
#' Insert one or more rows into an existing data frame.
#'
#' @param data A data frame.
#' @param x A named list of values to insert. Unused column names will be `NA`.
#' @param before,after An integer vector for the column index. Defaults
#' to after last row.
#'
#' @return `data.frame`
#'
#' @seealso [tibble::add_row()]
#'
#' @references Kirill Muller, Hadley Wickham, and Romain francois (Rstudio) (2017). tibble: Simple Data Frames. R package version 1.4.2. https://CRAN.R-project.org/package=tibble. Git commit 294c3cffad38f8f9f277c848742a90984fe239e3
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # add_row() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(x = 1:3, y = 3:1)
#'
#' add_row(df, list(x = 4, y = 0))
#'
#' # You can specify where to add the new rows
#' add_row(df, list(x = 4, y = 0), before = 2)
#'
#' # You can supply vectors, to add multiple rows (this isn't
#' # recommended because it's a bit hard to read)
#' add_row(df, list(x = 4:5, y = 0:-1))
#'
#' # Absent variables get missing values
#' add_row(df, list(x = 4))
#'
#' # You can't create new variables
#' \dontrun{
#' add_row(df, list(z = 10))
#' }
#'
#' @export
add_row <- function(data, x, before = NULL, after = NULL) {
  # based on Git commit 294c3cffad38f8f9f277c848742a90984fe239e3 (C) RStudio, 2017, MIT
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data, check_missing = TRUE)
  check_named_vector(x, check_missing = TRUE)
  if(inherits(data, "group_df")) {
    stop(
      "Check the 'data' argument in function bkdat::add_row(). Can't add rows to grouped data frames.",
      call. = FALSE
    )
  }

  #-----------------------------------------------------------------------------
  # add rows
  #-----------------------------------------------------------------------------
  x_data <- as_df_rep(x)
  attr(x_data, "row.names") <- .set_row_names(max(1L, nrow_df(x_data)))

  extra_vars <- setdiff(names(x_data), names(data))
  if (length(extra_vars) > 0L) {
    stop(
      "Check the 'x' argument in function bkdat::add_row(). Can't add row with new variables ",
      pretty_print(extra_vars),
      ".",
      call. = FALSE
    )
  }

  missing_vars <- setdiff(names(data), names(x_data))
  x_data[missing_vars] <- lapply(data[missing_vars], na_value)
  x_data <- x_data[names(data)]

  # function defined in add_column.r
  pos <- pos_from_before_after(before, after, nrow_df(data))
  out <- rbind_at(data, x_data, pos)

  class(out) <- class(data)
  out <- reset_row_names(out)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}

na_value <- function(x) {
  if (is.list(x)) {
    list(NULL)
  } else {
    NA
  }
}

rbind_at <- function(old, new, pos) {
  if (nrow(old) == 0L) {
    old <- old[1L, ]
    out <- rbind(old, new)[-1L, ]
  } else {
    out <- rbind(old, new)
    if (pos < nrow(old)) {
      pos <- max(pos, 0L)
      idx <- c(
        seq2(1L, pos),
        seq2(nrow(old) + 1L, nrow(old) + nrow(new)),
        seq2(pos + 1L, nrow(old))
      )
      out <- out[idx, , drop = FALSE]
    }
  }
  out
}
