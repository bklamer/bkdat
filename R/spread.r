#' Converts tall data frames into wide data frames
#'
#' Converts tall data frames into wide data frames.
#'
#' @param data A data frame.
#' @param key A character string for the name of the column which contains the
#' values that will be 'spread' across as new column names.
#' @param value A character string for the name of the column which contains
#' the values that will be placed into the new columns.
#' @param fill If set, missing values will be replaced with this value. Note
#' that there are two types of missingness in the input: explicit missing values
#' (i.e. `NA`), and implicit missings, rows that simply aren't present. Both
#' types of missing value will be replaced by fill.
#' @param convert This is useful if the value column was a mix of variables that
#' was coerced to a string. If the class of the value column was factor or date,
#' note that will not be true of the new columns that are produced, which are
#' coerced to character before type conversion.
#' @param drop If FALSE, will keep factor levels that don't appear in the data,
#' filling in missing combinations with fill.
#' @param sep If NULL, the column names will be taken from the values of key
#' variable. If non-NULL, the column names will be given by
#' `<key_name><sep><key_value>`.
#'
#' @return `data.frame`
#'
#' @seealso [tidyr::spread()],
#'          [stats::reshape()],
#'          [reshape2::dcast()],
#'          [data.table::dcast()]
#'
#' @references Hadley Wickham and Lionel Henry (2017). tidyr: Easily Tidy Data with 'spread()' and 'gather()' Functions. R package version 0.8.1. https://CRAN.R-project.org/package=tidyr. Git commit 74bd48f781e793d2c1b85a93f216e595e910c5d9
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # spread() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' stocks <- data.frame(
#'   time = as.Date('2009-01-01') + 0:9,
#'   X = rnorm(10, 0, 1),
#'   Y = rnorm(10, 0, 2),
#'   Z = rnorm(10, 0, 4),
#'   ignore = letters[1:10],
#'   stringsAsFactors = FALSE
#' )
#'
#' df1 <- gather_(data = stocks, key = "key", value = "value", cols = c("X", "Y", "Z"))
#' df2 <- df1
#' df2[30, 2] <- "ZZZ"
#'
#' df1
#' df2
#'
#' spread(data = df1, key = "key", value = "value")
#' spread(data = df2, key = "key", value = "value")
#'
#' @export
spread <- function(data, key, value, fill = NA, convert = FALSE, drop = TRUE,
                   sep = NULL) {
  # Git commit 74bd48f781e793d2c1b85a93f216e595e910c5d9 (C) Hadley Wickham and RStudio, 2014, MIT
  #-----------------------------------------------------------------------------
  # TODO
  #-----------------------------------------------------------------------------
  # append string to spread keys

  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_string(key)
  check_string(value)

  #-----------------------------------------------------------------------------
  # spread
  #-----------------------------------------------------------------------------
  col <- data[key]
  col_id <- id(col, drop = drop)
  col_labels <- split_labels(col, col_id, drop = drop)

  rows <- data[setdiff(names(data), c(key, value))]
  if(ncol(rows) == 0 && nrow_df(rows) > 0) {
    # Special case when there's only one row
    row_id <- structure(1L, n = 1L)
    row_labels <- as.data.frame(matrix(nrow = 1, ncol = 0))
  } else {
    row_id <- id(rows, drop = drop)
    row_labels <- split_labels(rows, row_id, drop = drop)
    rownames(row_labels) <- NULL
  }

  overall <- id(list(col_id, row_id), drop = FALSE)
  n <- attr(overall, "n")
  # Check that each output value occurs in unique location
  if(anyDuplicated(overall)) {
    groups <- split(seq_along(overall), overall)
    groups <- groups[vapply(groups, length, integer(1)) > 1]
    str <- vapply(
      groups,
      function(x) paste0("(", paste0(x, collapse = ", "), ")"),
      character(1)
    )
    stop(
      "Check the 'key' argument in function bkdat::spread(). There are duplicate identifiers for rows ",
      pretty_print(str),
      call. = FALSE
    )
  }

  # Add in missing values, if necessary
  if(length(overall) < n) {
    overall <- match(seq_len(n), overall, nomatch = NA)
  } else {
    overall <- order(overall)
  }

  value_vec <- data[[value]]
  ordered <- value_vec[overall]

  if(!is.na(fill)) {
    ordered[is.na(ordered)] <- fill
  }

  if(convert && !is.character(ordered)) {
    ordered <- as.character(ordered)
  }

  dim(ordered) <- c(attr(row_id, "n"), attr(col_id, "n"))
  colnames(ordered) <- enc2utf8(col_names(col_labels, sep = sep))
  ordered <- as_df(ordered)

  if(convert) {
    ordered[] <- lapply(ordered, utils::type.convert, as.is = TRUE)
  }

  append_df(row_labels, ordered)
}

col_names <- function(x, sep = NULL) {
  names <- as.character(x[[1]])

  if(is.null(sep)) {
    if(length(names) == 0) {
      # ifelse will return logical()
      character()
    } else {
      ifelse(is.na(names), "<NA>", names)
    }
  } else {
    paste(names(x)[[1]], names, sep = sep)
  }
}

split_labels <- function(df, id, drop = TRUE) {
  if(length(df) == 0) {
    return(df)
  }

  if(drop) {
    representative <- match(sort(unique(id)), id)
    res <- df[representative, , drop = FALSE]
    res <- reset_row_names(res)
    res
  } else {
    unique_values <- lapply(df, ulevels)
    rev(expand.grid(rev(unique_values), stringsAsFactors = FALSE))
  }
}

ulevels <- function(x) {
  if(is.factor(x)) {
    x <- addNA(x, ifany = TRUE)
    levs <- levels(x)
    factor(levs, levels = levs, ordered = is.ordered(x))
  } else {
    # preserve NAs
    sort(unique(x), na.last = TRUE)
  }
}

append_df <- function(x, values, after = length(x)) {
  y <- append(x, values, after = after)
  class(y) <- class(x)
  attr(y, "row.names") <- attr(x, "row.names")

  y
}

#-------------------------------------------------------------------------------
# id() and id_var() are in ./utils.r
#-------------------------------------------------------------------------------
