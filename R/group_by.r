#' Grouped data frames
#'
#' Create a data frame with grouping attributes.
#'
#' Subset operations \code{\link[base]{$<-}}, \code{\link[base]{[[<-}},
#' \code{\link[base]{[<-}}, and \code{\link[base]{[}}
#' on a `group_df` will drop the grouping attributes. List-columns cannot
#' be grouped on.
#'
#' @param data A data frame.
#' @param group_cols A character vector of column names to group by.
#' @param add `TRUE` or `FALSE`. Defaults to `FALSE`, overwriting any existing groupings. If
#' `TRUE` variables are added to the groupings.
#' @param drop `TRUE` or `FALSE`. Whether or not to drop empty groups. Defaults to
#' `TRUE`.
#'
#' @return group_df
#'
#' @seealso [dplyr::group_by()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # group_by() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(1, 1, 2), c = c(1, 2, 3))
#' group_by(df, "a")
#' group_by(df, "b")
#' group_by(df, c("a", "b"))
#'
#' summarise(group_by(df, "b"), c(mean = "mean(a)"))
#'
#' @export
group_by <- function(data, group_cols, add = FALSE, drop = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_character(group_cols)
  check_name_in_df(name = group_cols, df = data)
  check_logical(add)

  # Make sure the grouping variable is not a list
  type <- vapply(data, typeof, FUN.VALUE = character(1), USE.NAMES = TRUE)
  if(any(group_cols %in% names(type[type == "list"]))) {
    bad_cols <- group_cols[group_cols %in% names(type[type == "list"])]
    stop(
      "Check the 'group_cols' argument in function bkdat::group_by(). The following variables in 'group_cols' are of class list: ",
      pretty_print(bad_cols),
      ", and lists cannot be used as grouping columns."
    )
  }

  #-----------------------------------------------------------------------------
  # Set attributes
  #-----------------------------------------------------------------------------
  # group_cols
  if(add) {
    group_cols <- unique(c(attr(data, "group_cols"), group_cols))
  }
  attr(data, which = "group_cols") <- group_cols

  # indices
  indices <- group_indices(data, group_cols)
  attr(data, which = "group_indices") <- indices

  # group length
  group_length <- vapply(indices, length, FUN.VALUE = integer(1), USE.NAMES = FALSE)
  attr(data, which = "group_length") <- group_length

  # group labels
  labels <- group_labels(data, group_cols, arrange = TRUE)
  attr(data, which = "group_labels") <- labels

  # class
  class(data) <- unique(c("group_df", class(data)))

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}

#' Ungroup
#'
#' Returns a data.frame stripped of its grouping attributes. A convenience
#' function that calls [bkdat::as_df()].
#'
#' @param data A grouped data frame.
#'
#' @return `data.frame`
#'
#' @seealso [dplyr::ungroup()]
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # ungroup() examples
#' #----------------------------------------------------------------------------
#' group_indices(mtcars, "cyl")
#' group_indices(mtcars, c("cyl", "carb"))
#'
#' @export
ungroup <- function(data) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_group_df(data)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  as_df(data)
}

#' Group Labels
#'
#' Returns a data frame of unique grouping combinations.
#'
#' @param data A data frame.
#' @param group_cols A character vector of column names to group by.
#' @param arrange `TRUE` or `FALSE`. If `TRUE`, then labels will be arranged in
#' ascending order starting with the last column and finishing with the first
#' column.
#'
#' @return `data.frame`
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # group_levels() examples
#' #----------------------------------------------------------------------------
#' group_labels(mtcars, "cyl")
#' group_labels(mtcars, c("cyl", "carb"))
#'
#' @export
group_labels <- function(data, group_cols, arrange = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_character(group_cols)

  #-----------------------------------------------------------------------------
  # Create labels
  #-----------------------------------------------------------------------------
  res <- group_unique_df(data[group_cols])
  if(arrange) {
    res <- arrange_(res, group_cols)
  }
  res <- reset_row_names(res)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' Group Levels
#'
#' Returns a factor vector of grouping combinations. Return vector is same
#' length as the number of rows in the data frame. levels of the factor can be
#' lexically ordered or unordered.
#'
#' @param data A data frame.
#' @param group_cols A character vector of column names to group by.
#' @param arrange `TRUE` or `FALSE`. If `TRUE`, then the factor levels are arranged
#' lexically in ascending order. If false, factor levels are not ordered.
#'
#' @return `factor`
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # group_levels() examples
#' #----------------------------------------------------------------------------
#' group_levels(mtcars, "cyl")
#' group_levels(mtcars, c("cyl", "carb"))
#'
#' @export
group_levels <- function(data, group_cols, arrange = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_character(group_cols)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  group_interaction(data[group_cols], drop = TRUE, sep = "|", arrange = arrange)
}

#' Group Indices
#'
#' Returns a list of indices for each level of a grouped data frame.
#'
#' @param data A data frame.
#' @param group_cols A character vector of column names to group by.
#' @param arrange `TRUE` or `FALSE`. If `TRUE`, then the list elements are lexically
#' and ascending in order based on the group_levels. If `FALSE`, the list elements
#' are not ordered.
#'
#' @return `list`
#'
#' @seealso [dplyr::group_indices()]
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # group_indices() examples
#' #----------------------------------------------------------------------------
#' group_indices(mtcars, "cyl")
#' group_indices(mtcars, c("cyl", "carb"))
#'
#' @export
group_indices <- function(data, group_cols, arrange = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_character(group_cols)

  #-----------------------------------------------------------------------------
  # Split across indices
  #-----------------------------------------------------------------------------
  group_levels <- group_levels(data, group_cols, arrange)
  idx <- split(seq_along(group_levels), group_levels)
  names(idx) <- attr(group_levels, "levels")

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  idx
}

#-------------------------------------------------------------------------------
# The below are designed to remove grouping attributes and return clean
# data.frame objects.
#-------------------------------------------------------------------------------

#' @export
`[.group_df` <- function(x, i, j, drop = FALSE) {
  #-----------------------------------------------------------------------------
  # `[` does not drop class attributes (but does drop everything else), so we
  # need to reset class to just "data.frame" before (not after) calling
  # NextMethod() since depending on 'drop', the result could be a data.frame or
  # vector.
  #
  # nargs() returns 2 for x[i] and greater than two for x[i, j], x[, j], etc.
  # This is needed as you don't want drop for x[i], and x[] shouldn't drop
  # anything.
  #-----------------------------------------------------------------------------
  if(nargs() > 2) {
    attr(x, "class") <- "data.frame"
    # Return
    NextMethod(drop = drop)
  } else {
    if(missing(i)) {
      # Return
      x
    } else {
      attr(x, "class") <- "data.frame"
      # Return
      NextMethod()
    }
  }
}

#' @export
`[<-.group_df` <- function(x, i, j, value) {
  #-----------------------------------------------------------------------------
  # `[<-` does not drop any attributes, so we will need to do that manually.
  #-----------------------------------------------------------------------------
  attr(x, "class") <- "data.frame"
  attributes(x) <- attributes(x)[c("names", "row.names", "class")]

  # Return
  NextMethod()
}

#' @export
`[[<-.group_df` <- function(x, i, j, value) {
  #-----------------------------------------------------------------------------
  # `[[<-` does not drop any attributes, so we will need to do that manually.
  #-----------------------------------------------------------------------------
  attr(x, "class") <- "data.frame"
  attributes(x) <- attributes(x)[c("names", "row.names", "class")]

  # Return
  NextMethod()
}

#' @export
`$<-.group_df` <- function(x, name, value) {
  #-----------------------------------------------------------------------------
  # `$<-` does not drop any attributes, so we will need to do that manually.
  #-----------------------------------------------------------------------------
  attr(x, "class") <- "data.frame"
  attributes(x) <- attributes(x)[c("names", "row.names", "class")]

  # Return
  NextMethod()
}
