#===============================================================================
# general helpers
#===============================================================================
#-------------------------------------------------------------------------------
# Column names for different scenarios of by and suffix
# 1. 'by' argument is named (for joining by different variables)
# 2. 'suffix' for variables with the same name and not joined by.
# 3. a combination of the two previous conditions
#
# x,y: data.frame
# by: character vector
# suffix: length 2 character vector
#
# returns list with
# 1. names of original 'x', 'y'
# 2. names of original 'by'
# 3. names of join corrected 'by'
# 4. names of join corrected 'x', 'y'
# 5. names of returned join output.
#-------------------------------------------------------------------------------
join_names <- function(x, y, by, suffix) {
  # original names for 'x', 'y', and 'by'
  x_names <- names(x)
  y_names <- names(y)
  by_list <- by_names(by, x_names, y_names)
  by_x <- by_list[["by_x"]]
  by_y <- by_list[["by_y"]]

  # join corrected names for 'by', 'x', 'y'
  if(is.null(by_list[["by_x_jc"]])) {
    by_x_jc <- by_x
    by_y_jc <- by_y
    x_names_jc <- x_names
    y_names_jc <- y_names
  } else {
    by_x_jc <- by_list[["by_x_jc"]]
    by_y_jc <- by_list[["by_y_jc"]]
    x_names_jc <- x_names
    y_names_jc <- y_names
    x_names_jc[match(by_x, x_names_jc)] <- by_x_jc
    y_names_jc[match(by_y, y_names_jc)] <- by_y_jc
  }

  # add 'suffix' to join corrected 'x' and 'y' names
  x_names_jc_diff <- setdiff(x_names_jc, by_x_jc)
  y_names_jc_diff <- setdiff(y_names_jc, by_y_jc)
  are_x_names_jc_diff_shared <- x_names_jc_diff %in% y_names_jc_diff
  if(any(are_x_names_jc_diff_shared)) {
    x_names_jc_diff_shared <- x_names_jc_diff[are_x_names_jc_diff_shared]

    are_y_names_jc_diff_shared <- y_names_jc_diff %in% x_names_jc_diff
    y_names_jc_diff_shared <- y_names_jc_diff[are_y_names_jc_diff_shared]

    are_x_names_jc_shared <- x_names_jc %in% x_names_jc_diff_shared
    are_y_names_jc_shared <- y_names_jc %in% y_names_jc_diff_shared

    x_names_jc[are_x_names_jc_shared] <- paste0(x_names_jc[are_x_names_jc_shared], suffix[1L])
    while(any(x_names_jc[are_x_names_jc_shared] %in% x_names_jc[!are_x_names_jc_shared])) {
      x_names_jc[are_x_names_jc_shared] <- paste0(x_names_jc[are_x_names_jc_shared], suffix[1L])
    }
    y_names_jc[are_y_names_jc_shared] <- paste0(y_names_jc[are_y_names_jc_shared], suffix[2L])
    # need to check both y_names_jc[!are_y_names_jc_shared] and x_names_jc[!are_x_names_jc_shared]
    # otherwise name clashes will happen for test "avoid repetition".
    while(any(y_names_jc[are_y_names_jc_shared] %in% c(y_names_jc[!are_y_names_jc_shared], x_names_jc[!are_x_names_jc_shared]))) {
      y_names_jc[are_y_names_jc_shared] <- paste0(y_names_jc[are_y_names_jc_shared], suffix[2L])
    }
  }

  # add 'suffix' to join corrected 'y' names if joining on different variables
  # and there is still name clash between 'x' and 'y'
  are_by_x_in_y_names_jc_diff <- by_x %in% y_names_jc_diff
  if(any(are_by_x_in_y_names_jc_diff)) {
    are_y_names_jc_in_by_x <- y_names_jc %in% by_x[are_by_x_in_y_names_jc_diff]
    y_names_jc[are_y_names_jc_in_by_x] <- paste0(y_names_jc[are_y_names_jc_in_by_x], suffix[2L])
  }

  # Create names for the returned join
  x_names_out <- x_names_jc
  y_names_out <- y_names_jc
  x_names_out[match(by_x_jc, x_names_out)] <- by_x
  y_names_out[match(by_y_jc, y_names_out)] <- by_y

  # return
  list(
    x_names = x_names,
    y_names = y_names,
    by_x = by_x,
    by_y = by_y,
    x_names_jc = x_names_jc,
    y_names_jc = y_names_jc,
    by_x_jc = by_x_jc,
    by_y_jc = by_y_jc,
    out_names_jc = c(x_names_jc, setdiff(y_names_out, by_y)),
    x_names_out = x_names_out,
    y_names_out = y_names_out,
    out_names = c(x_names_out, setdiff(y_names_out, by_y))
  )
}

#-------------------------------------------------------------------------------
# Handle cases of named vectors when joining by different variables on x and y
#-------------------------------------------------------------------------------
by_names <- function(by, x_names, y_names) {
  if(is.null(by)) {
    by <- intersect(x_names, y_names)
  }
  by <- unlist(x = by, use.names = TRUE)
  by <- by[!duplicated(by)]
  by_x <- if_null_else(names(by), by)
  by_y <- unname(by)
  # If x partially named, assume unnamed are the same in both tables
  by_x[by_x == ""] <- by_y[by_x == ""]
  # New names for joining on different variables
  replace <- by_x != by_y
  if(any(replace)) {
    by_x_jc <- by_x
    by_y_jc <- by_y
    by_x_jc[replace] <- paste0(by_x[replace], "!!!replaced!!!")
    by_y_jc[replace] <- paste0(by_x[replace], "!!!replaced!!!")
  } else {
    by_x_jc <- NULL
    by_y_jc <- NULL
  }

  # Return
  list(
    by_x = by_x,
    by_y = by_y,
    by_x_jc = by_x_jc,
    by_y_jc = by_y_jc
  )
}

#-------------------------------------------------------------------------------
# Error checking for suffix argument
#-------------------------------------------------------------------------------
check_suffix <- function(suffix, fun) {
  if(anyNA(suffix)) {
    stop(
      "Check the 'suffix' argument in function ",
      paste0(fun),
      ". At least one of the parameters is NA (missing).",
      call. = FALSE
    )
  }
  if(all(suffix == "")) {
    stop(
      "Check the 'suffix' argument in function ",
      paste0(fun),
      ". Both paramers cannot be empty strings.",
      call. = FALSE
    )
  }
  if(!is.character(suffix) || length(suffix) != 2) {
    stop(
      "Check the 'suffix' argument in function ",
      paste0(fun),
      ". 'suffix' needs to be a character vector of length 2.",
      call. = FALSE
    )
  }
}

#-------------------------------------------------------------------------------
# Error checking for by argument
#-------------------------------------------------------------------------------
check_by <- function(by, x_names, y_names, fun) {
  if(is.null(by)) {
    by <- intersect(x_names, y_names)
    if(identical(by, character(0))) {
      stop(
        "Check the 'by' argument in function ",
        paste0(fun),
        ". There are no common names between 'x' and 'y'.",
        call. = FALSE
      )
    }
    message("Joining by: ", paste(by, collapse = ", "))
  }
  by_info <- by_names(by, x_names, y_names)
  are_by_in_x <- by_info[["by_x"]] %in% x_names
  are_by_in_y <- by_info[["by_y"]] %in% y_names
  if(!all(are_by_in_x) || !all(are_by_in_y)) {
    missing_x <- by_info[["by_x"]][!are_by_in_x]
    missing_y <- by_info[["by_y"]][!are_by_in_y]
    stop(
      "Check the 'by' argument in function ",
      paste0(fun),
      if(length(missing_x) > 0) {paste0(". The following join by columns are missing from data frame 'x': ",
      pretty_print(missing_x))},
      if(length(missing_y) > 0) {paste0(". The following join by columns are missing from data frame 'y': ",
      pretty_print(missing_y))},
      ".",
      call. = FALSE
    )
  }
  if(!is.character(by) && !is.list(by)) {
    stop(
      "Check the 'by' argument in function ",
      paste0(fun),
      ". 'by' needs to be a character vector or list.",
      call. = FALSE
    )
  }
  if(anyNA(by)) {
    stop(
      "Check the 'by' argument in function ",
      paste0(fun),
      ". At least one of the parameters is NA (missing).",
      call. = FALSE
    )
  }
  if(length(by) < 1) {
    stop(
      "Check the 'by' argument in function ",
      paste0(fun),
      ". 'by' is an empty string.",
      call. = FALSE
    )
  }
}

#===============================================================================
# plyr helpers
#===============================================================================
# Row indices to keep for join output
join_ids <- function(x, y, by, all = FALSE) {
  keys <- join.keys(x, y, by = by)

  ys <- split_indx(keys$y, keys$n)
  #length(ys) <- keys$n

  if (all) {
    # replace NULL with NA to preserve those x's without matching y's
    nulls <- vapply(ys, function(x) length(x) == 0, logical(1))
    ys[nulls] <- list(NA_real_)
  }

  ys <- ys[keys$x]
  xs <- rep(seq_along(keys$x), vapply(ys, length, numeric(1)))

  list(x = xs, y = unlist(ys))
}

# Should list be pre-allocated with integer(0) instead of NULL?
split_indx <- function(vec, n) {
  out <- vector(mode = "list", length = max(n, max(vec)))
  for(i in seq_along(vec)) {
    out[[vec[i]]] <- c(out[[vec[i]]], i)
  }
  out
}

# Join keys.
#
# Given two data frames, create a unique key for each row.
#
# @param x data frame
# @param y data frame
# @param by character vector of variable names to join by
join.keys <- function(x, y, by) {
  joint <- bind_rows(list(x[by], y[by]))
  keys <- id(joint, drop = TRUE)

  n_x <- nrow_df(x)
  n_y <- nrow_df(y)

  list(
    x = keys[seq_len(n_x)],
    y = keys[n_x + seq_len(n_y)],
    n = attr(keys, "n")
  )
}

#-------------------------------------------------------------------------------
# id() and id_var() are in ./utils.r
#-------------------------------------------------------------------------------
