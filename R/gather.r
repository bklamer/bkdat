#' Converts wide data frames into tall data frames
#'
#' Converts wide data frames into tall data frames, i.e. converts columns into
#' rows.
#'
#' `gather()` does not check the types of columns you are gathering. It is
#' the users responsibility to prevent unexpected type coercion.
#'
#' @param .data,data A data frame.
#' @param .key,key A character string for the new key column name.
#' @param .value,value A character string for the new value column name.
#' @param ... Unquoted column name separated by commas. Will be gathered and
#' used to populate the `key` column.
#' @param cols A character vector consisting of the column names that will
#' be gathered and used to populate the `key` column.
#'
#' @return data.frame
#'
#' @seealso [tidyr::gather()],
#'          [reshape2::melt()],
#'          [stats::reshape()],
#'          [utils::stack()],
#'          [data.table::melt()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # gather() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' stocks <- data.frame(
#'   time = as.Date('2018-01-01') + 0:9,
#'   X = rnorm(10, 0, 1),
#'   Y = rnorm(10, 0, 2),
#'   Z = rnorm(10, 0, 4),
#'   letters = letters[1:10],
#'   stringsAsFactors = FALSE
#' )
#'
#' gather_(data = stocks, key = "key", value = "value", cols = c("X", "Y", "Z"))
#' gather(.data = stocks, .key = "key", .value = "value", X, Y, Z)
#'
#' @export
gather <- function(.data, .key, .value, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(.data)
  check_string(.key)
  check_string(.value)
  cols <- dots_char(..., .check_names = FALSE)
  check_name_in_df(name = cols, df = .data)

  #-----------------------------------------------------------------------------
  # Gather the columns
  #-----------------------------------------------------------------------------
  data_names <- names(.data)
  df <- .data[, !(data_names %in% cols), drop = FALSE]
  df <- bind_rows(
    lapply(seq_along(cols), function(x) {
      df[[.key]] <- cols[x]
      df[[.value]] <- .data[[cols[x]]]
      df
    })
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  df
}
