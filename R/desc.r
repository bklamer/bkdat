#' Descending order
#'
#' Transforms input vector into a new numeric vector which will sort in
#' descending order. A better named `-`[base::xtfrm()].
#'
#' @param x Vector to be transformed.
#'
#' @return numeric vector of length x
#'
#' @seealso [base::xtfrm()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # desc() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' desc(1:10)
#' desc(letters[10:1])
#'
#' @export
desc <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  # xtfrm also works with POSIXlt, so allow that too.
  if(!is_atomic(x) && !inherits(x, "POSIXlt")) {
    stop("Check the 'x' argument in function bkdat::desc(). 'x' needs to be an atomic vector.")
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  -xtfrm(x)
}
