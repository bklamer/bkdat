#' Add new variables to data frame
#'
#' Adds new variables to existing data frame using a vector of character
#' expressions.
#'
#' @param .data,data A data frame.
#' @param ... Expressions to be evaluated.
#' @param x A named character vector (or named list of character vectors) of expressions.
#' @param .frame,frame A character vector of column names referred to in `x`. This
#' subsets the data frame to only specified columns before evaluation. This
#' provides safer evaluation by preventing accidental use of spurious global
#' variables in place of column names or accidental use of spurious column
#' names in place of global variables.
#' @param .env,env An environment to look for objects outside those in `data`.
#' @param .list_column,list_column `TRUE` or `FALSE`. If mutating on a `nest_df`,
#' then `TRUE` will evaluate and add the new variable(s) inside the
#' list-column. `FALSE` will evaluate and add the new variable(s) outside
#' the list-column.
#'
#' @return `data.frame`
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # mutate() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
#' mutate(df, c = -1*a, d = log(b))
#' mutate_(df, c(c = "-1*a", d = "log(b)"))
#'
#' @export
mutate <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
UseMethod("mutate")
}

#' @export
#' @rdname mutate
mutate.data.frame <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  dots <- dots(...)
  check_frame(.data, .frame, "bkdat::mutate()")

  #-----------------------------------------------------------------------------
  # Perform mutation
  #-----------------------------------------------------------------------------
  data_nrow <- nrow_df(.data)
  for(i in names(dots)) {
    new_col <- withCallingHandlers(
      eval(
        expr = dots[[i]],
        # Looping over subset operation is slow, but I don't have a faster solution.
        envir = if(is.null(.frame)) {.data} else {.data[, .frame, drop = FALSE]},
        enclos = .env
      ),
      error = function(e) stop(
        "Check the dots argument in function bkdat::mutate(). An error occured while evaluating the expression '",
        dots_char(...)[[i]],
        "'. You might have used non-existing column names or wrongly-formed functions.",
        call. = FALSE
      )
    )
    # We want to propogate constants, but not anything else that is not the
    # length of the dataframe.
    new_col_length <- length(new_col)
    if(new_col_length != data_nrow && new_col_length != 1) {
      stop(
        "Check the dots argument in function bkdat::mutate(). After evaluating '",
        dots_char(...)[[i]],
        "', the length of this new column doesn't match the number of rows in the data frame."
      )
    }
    .data[[i]] <- new_col
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  .data
}

#' @export
#' @rdname mutate
mutate.group_df <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  NextMethod()
}

#' @export
#' @rdname mutate
mutate.nest_df <- function(.data, ..., .frame = NULL, .env = parent.frame(), .list_column = TRUE) {
  if(list_column) {
    #-----------------------------------------------------------------------------
    # apply mutate.data.frame() to each list-column element.
    #-----------------------------------------------------------------------------
    nest_name <- attr(.data, "nest_name")
    label_df <- attr(.data, "group_labels")
    list_column <- bind_rows(
      lapply(
        .data[[nest_name]],
        mutate,
        ... = ...,
        .frame = .frame,
        .env = .env
      )
    )

    # Return
    cbind(label_df, list_column)
  } else {
    NextMethod()
  }
}
