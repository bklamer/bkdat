#' Add columns to a data frame
#'
#' Insert one or more columns into an existing data frame.
#'
#' @param data A data frame.
#' @param x A named list of values to insert. All values must have length
#' `nrow(data)`, or be of length 1.
#' @param before,after An **integer** vector for the column index or **character**
#' vector for the column name of where to add the new columns. Defaults to after
#' last column.
#'
#' @return `data.frame`
#'
#' @seealso [tibble::add_column()]
#'
#' @references Kirill Muller, Hadley Wickham, and Romain francois (Rstudio) (2017). tibble: Simple Data Frames. R package version 1.4.2. https://CRAN.R-project.org/package=tibble. Git commit 294c3cffad38f8f9f277c848742a90984fe239e3
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # add_column() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(x = 1:3, y = 3:1)
#' add_column(df, list(z = -1:1, w = 0))
#'
#' \dontrun{
#' # You can't overwrite existing columns
#' add_column(df, list(x = 4:6))
#'
#' # You can't increase the number of rows
#' add_column(df, list(z = 1:5))
#' }
#'
#' @export
add_column <- function(data, x, before = NULL, after = NULL) {
  # based on Git commit 294c3cffad38f8f9f277c848742a90984fe239e3 (C) RStudio, 2017, MIT
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data, check_missing = TRUE)
  check_named_vector(x, check_missing = TRUE)
  if(ncol(data) == 0L) {
    warning("Check the 'data' argument in function bkdat::add_column(). 'data' has zero columns.")
    return(data)
  }

  #-----------------------------------------------------------------------------
  # add columns
  #-----------------------------------------------------------------------------
  x_data <- as_df_rep(x)

  if(nrow_df(data) != nrow_df(x_data)) {
    if(nrow_df(x_data) == 1L) {
      x_data <- x_data[rep(1L, nrow_df(data)), , drop = FALSE]
    } else {
      stop(
        "Check the 'x' argument in function bkdat::add_column(). 'x' has length ",
        paste0(nrow(x_data)),
        " but there are ",
        paste0(nrow(data)),
        " rows in 'data'.",
        call. = FALSE
      )
    }
  }

  extra_vars <- intersect(names(x_data), names(data))
  if(length(extra_vars) > 0L) {
    stop(
      "Check the 'x' argument in function bkdat::add_column(). The column names ",
      pretty_print(x = extra_vars),
      " already exist in 'data'."
    )
  }

  pos <- pos_from_before_after_names(before, after, colnames(data))

  end_pos <- ncol(data) + seq_len(ncol(x_data))

  indexes_before <- seq2(1L, pos)
  indexes_after <- seq2(pos + 1L, ncol(data))
  indexes <- c(indexes_before, end_pos, indexes_after)

  data[end_pos] <- x_data

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data[indexes]
}

pos_from_before_after_names <- function(before, after, names) {
  before <- check_names_before_after(before, names)
  after <- check_names_before_after(after, names)
  pos_from_before_after(before, after, length(names))
}

check_names_before_after <- function(j, names) {
  if(is.numeric(j)) {
    return(j)
  }
  pos <- match(j, names)
  if(anyNA(pos)) {
    unknown_names <- j[is.na(pos)]
    stop(
      "Check the 'before' or 'after' arguments in function bkdat::add_column(). ",
      "The following names are not in 'data': ",
      pretty_print(unknown_names),
      ".",
      call. = FALSE
    )
  }
  pos
}

pos_from_before_after <- function(before, after, len) {
  if(length(before) == 0L) {
    if(length(after) == 0L) {
      len
    } else {
      max(0L, min(after, len))
    }
  } else {
    if(length(after) == 0L) {
      max(0L, min(before - 1L, len))
    } else {
      stop("Check the 'before' and 'after' arguments in function bkdat::add_column(). You cannot specify them both.")
    }
  }
}

# as_df() modified to accept recycling of length one values
as_df_rep <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  list_lengths <- unlist(lapply(x, NROW))
  unique_list_lengths <- sort(unique(list_lengths))
  if(length(unique_list_lengths) > 2L) {
    stop(
      "Check the 'x' argument in function bkdat::add_column(). The list elements in 'x' do not all have the same length.",
      call. = FALSE
    )
  }
  if(length(unique_list_lengths) == 2L) {
    first <- unique_list_lengths[[1L]]
    second <- unique_list_lengths[[2L]]
    if(first == 1L) {
      l1_index <- list_lengths == 1L
      x[l1_index] <- lapply(x[l1_index], function(x) {rep(x, second)})
      unique_list_lengths <- second
    } else if(first == 0L) {
      l0_index <- list_lengths == 0L
      x[l0_index] <- lapply(x[l0_index], function(x) {rep(x, second)})
      unique_list_lengths <- second
    } else {
      stop(
        "Check the 'x' argument in function bkdat::add_column(). The list elements in 'x' do not all have the same length.",
        call. = FALSE
      )
    }
  }

  #-----------------------------------------------------------------------------
  # Create column names
  #-----------------------------------------------------------------------------
  col_names <- create_col_names(
    col_names = names(x),
    n_cols = length(x),
    prefix = "V"
  )

  #-----------------------------------------------------------------------------
  # Set attributes
  #-----------------------------------------------------------------------------
  attr(x, "names") <- col_names
  attr(x, "row.names") <- .set_row_names(unique_list_lengths)
  attr(x, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}
