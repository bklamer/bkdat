#' Apply functions to grouped data frames
#'
#' Applies functions on the subsets within a grouped data frame.
#'
#' @param .data,data A grouped data frame.
#' @param ... Expressions to be evaluated. Use `.` to refer to the current group
#' data frame. If named, results will be stored in a list column. If unnamed,
#' and the expression returns a data frame, results will be inserted as is.
#' Otherwise a list column will be created with a default column name.
#' @param x A character vector (or list of character vectors) of expressions to
#' apply to each group. Use `.` to refer to the current group data frame. If named,
#' results will be stored in a list column. If unnamed, and the expression
#' returns a data frame, results will be inserted as is. Otherwise a list column
#' will be created with a default column name.
#' @param .frame,frame A character vector of column names referred to in `x`. This
#' subsets the data frame to only specified columns before evaluation. This
#' provides safer evaluation by preventing accidental use of spurious global
#' variables in place of column names or accidental use of spurious column
#' names in place of global variables.
#' @param .env,env An environment to look for objects outside those in `data`.
#'
#' @return `data.frame`
#'
#' @seealso [dplyr::do()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # do() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- group_by(mtcars, "cyl")
#' do_(df, c(hp_t_test = "t.test(.$hp)"))
#' do(df, hp_t_test = t.test(.$hp))
#'
#' @export
#' @rdname do
do <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_group_df(.data)
  dots <- dots(..., .check_names = FALSE)
  check_frame(.data, .frame)

  #-----------------------------------------------------------------------------
  # Apply functions on groups
  #-----------------------------------------------------------------------------
  # create new env so we can assign dot (.) as the current group df.
  env <- new.env(parent = .env)
  res <- attr(.data, "group_labels")
  idx <- attr(.data, "group_indices")
  comp <- replicate(length(dots), vector("list", nrow(res)), simplify = FALSE)

  # this indexing order should error out faster if there are issues with
  # evaluating the expressions?
  for(i in seq_along(idx)) {
    env$. <- if(is.null(.frame)) {.data[idx[[i]], , drop = FALSE]} else {.data[idx[[i]], .frame, drop = FALSE]}
    for(j in seq_along(dots)) {
      # need error catching here
      comp[[j]][[i]] <- withCallingHandlers(
        eval(
          expr = dots[[j]],
          envir = env
        ),
        error = function(e) stop(
          "Check the dots argument in bkdat::do(). An error occured while evaluating the expression '",
          dots_char(...)[[j]],
          "'.",
          call. = FALSE
        )
      )
    }
  }

  names(comp) <- names(dots)

  # Only un-named dataframe results should be cbind'd below. all others will
  # be stored in a list.
  for(i in seq_along(comp)) {
    col <- comp[[i]]
    if(any(!vapply(X = col, FUN = is.data.frame, FUN.VALUE = logical(1)))) {
      res[[names(comp)[i]]] <- comp[[i]]
    } else {
      if(names(comp[i]) == "" || is.null(names(comp[i]))) {
        res <- cbind(res, bind_rows(col))
      } else {
        res[[names(comp)[i]]] <- comp[[i]]
      }
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}
