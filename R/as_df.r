#' Fast `as.data.frame()`
#'
#' A faster function to replace `as.data.frame()`. Returns a plain
#' `data.frame`.
#'
#' Rownames are always integers. Speed benefits are mainly seen for converting
#' lists. `as_df.matrix()` is provided, but `as.data.frame()` should
#' probably be used instead as there is only a small speed benefit.
#' `as_df.default()` checks if the object is matrix-like (row and column
#' dimensions exist) but the object just so happens to not have class matrix
#' (an unusual scenario, mainly for use as a helper within this package).
#' `as_df()` supports creating list-column dataframes, although care is
#' needed to prevent R's automatic coercion rules from changing the input. Be
#' sure to test output before use.
#'
#' `group_df`s are ungrouped and `nest_df`s are unnested.
#'
#' @param x The data object to be converted to a `data.frame`.
#'
#' @return `data.frame`
#'
#' @seealso [base::as.data.frame()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # as_df() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' m <- matrix(c(1, 2, 3, 4, 5, 6), ncol = 2)
#' as_df(m)
#' as.data.frame(m)
#'
#' l <- list(a = c(1, 2, 3), b = c(4, 5, 6))
#' as_df(l)
#' as.data.frame(l)
#'
#' @export
as_df <- function(x) {
  UseMethod("as_df")
}

#' @export
#' @rdname as_df
as_df.list <- function(x){
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  unique_list_length <- unique(unlist(lapply(x, NROW)))
  if(length(unique_list_length) != 1L) {
    stop("Check the 'x' argument in function bkdat::as_df(). The list elements in 'x' do not all have the same length.")
  }

  #-----------------------------------------------------------------------------
  # Create column names
  #-----------------------------------------------------------------------------
  col_names <- create_col_names(
    col_names = names(x),
    n_cols = length(x),
    prefix = "V"
  )

  #-----------------------------------------------------------------------------
  # Set attributes
  #-----------------------------------------------------------------------------
  attr(x, "names") <- col_names
  attr(x, "row.names") <- .set_row_names(unique_list_length)
  attr(x, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}

#' @export
#' @rdname as_df
as_df.matrix <- function(x){
  #-----------------------------------------------------------------------------
  # Gather info and place matrix columns in a list.
  #-----------------------------------------------------------------------------
  dims <- dim(x)
  col_names <- dimnames(x)[[2L]]
  # Need to drop column names because as.vector() was removed from assignment
  # loop. as.vector() drops names from a vector. Slight speed improvement.
  dimnames(x) <- list(NULL, col_names)
  res <- vector("list", dims[2L])
  for(i in seq_len(dims[2L])) {
    # as.vector() was removed from this assigment.
    res[[i]] <- x[, i]
  }

  #-----------------------------------------------------------------------------
  # Create column names.
  #-----------------------------------------------------------------------------
  col_names <- create_col_names(
    col_names = col_names,
    n_cols = dims[2L],
    prefix = "V"
  )

  #-----------------------------------------------------------------------------
  # Set attributes.
  #-----------------------------------------------------------------------------
  attr(res, "names") <- as.vector(col_names)
  attr(res, "row.names") <- .set_row_names(dims[1L])
  attr(res, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' @export
#' @rdname as_df
as_df.group_df <- function(x){
  #-----------------------------------------------------------------------------
  # Create column and row names
  #-----------------------------------------------------------------------------
  col_names <- create_col_names(
    col_names = names(x),
    n_cols = ncol(x),
    prefix = "V"
  )
  row_names <- .set_row_names(nrow_df(x))

  #-----------------------------------------------------------------------------
  # Reset attributes.
  #-----------------------------------------------------------------------------
  attributes(x) <- NULL
  attr(x, "names") <- col_names
  attr(x, "row.names") <- row_names
  attr(x, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}

#' @export
#' @rdname as_df
as_df.nest_df <- function(x){
  #-----------------------------------------------------------------------------
  # Create column and row names
  #-----------------------------------------------------------------------------
  col_names <- create_col_names(
    col_names = names(x),
    n_cols = ncol(x),
    prefix = "V"
  )
  row_names <- .set_row_names(nrow_df(x))

  #-----------------------------------------------------------------------------
  # Reset attributes.
  #-----------------------------------------------------------------------------
  attributes(x) <- NULL
  attr(x, "names") <- col_names
  attr(x, "row.names") <- row_names
  attr(x, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}

# If the object is square (2D like a matrix or data frame), but doesn't have
# class matrix or data.frame, then this default method will convert it to a
# data frame. Otherwise return an error. (actually this only applies to
# matrix like objects. How to extend so this actually works?)
#' @export
#' @rdname as_df
as_df.default <- function(x){
  #---------------------------------------------------------------------------
  # Check arguments.
  #---------------------------------------------------------------------------
  # return x if already a data.frame
  if(inherits(x, "data.frame")){
    return(x)
  }
  # Make sure it's 2 dimensional
  dims <- dim(x)
  if(length(dims) != 2) {
    stop(
      "Check the x argument in function bkdat::as_df(). Cannot coerce object of class ",
      pretty_print(class(x)),
      " to a data.frame."
    )
  }

  #---------------------------------------------------------------------------
  # Gather info and place columns in a list.
  #---------------------------------------------------------------------------
  col_names <- dimnames(x)[[2L]]
  # Need to drop column names because as.vector() was removed from assignment
  # loop. as.vector() drops names from a vector. Slight speed improvement.
  dimnames(x) <- list(NULL, col_names)
  res <- vector("list", dims[2L])
  for(i in seq_len(dims[2L])) {
    # as.vector() was removed from this assigment.
    res[[i]] <- x[, i]
  }

  #---------------------------------------------------------------------------
  # Create column names.
  #---------------------------------------------------------------------------
  col_names <- create_col_names(
    col_names = col_names,
    n_cols = dims[2L],
    prefix = "V"
  )

  #---------------------------------------------------------------------------
  # Set attributes.
  #---------------------------------------------------------------------------
  attr(res, "names") <- as.vector(col_names)
  attr(res, "row.names") <- .set_row_names(dims[1L])
  attr(res, "class") <- "data.frame"

  #---------------------------------------------------------------------------
  # Return
  #---------------------------------------------------------------------------
  res
}

# A convenience function relevant mostly for this scenario.
create_col_names <- function(col_names, n_cols, prefix) {
  if(is.null(col_names)) {
    col_names <- rep.int("", n_cols)
  }
  # Return true if blank OR if NA
  are_blank_col_names <- !nzchar(col_names) | is.na(col_names)
  index_blank_col_names <- which(are_blank_col_names)
  col_names[are_blank_col_names] <- paste0(prefix, index_blank_col_names)
  col_names
}
