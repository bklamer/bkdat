#' Separate one column into multiple columns.
#'
#' Separate one column into multiple columns. May be slow on large data frames.
#'
#' @param data A data frame.
#' @param col A string for the column name.
#' @param into A character vector of names for the new variables.
#' @param sep A string to be evaluated as a regular expression for separating between columns.
#' @param remove `TRUE` or `FALSE`. If `TRUE`, remove input column from the output data frame.
#' @param convert `TRUE` or `FALSE`. If `TRUE`, will run `type.convert()` with `as.is = TRUE` on new columns.
#' @param extra Specify a string to control what happens when there are too many separated values: `"error"` (the default) returns error. `"drop"` drop extra values. `"merge"` splits at most `length(into)` times.
#'
#' @return `data.frame`
#'
#' @seealso [tidyr::separate()]
#'
#' @references Hadley Wickham and Lionel Henry (2017). tidyr: Easily Tidy Data with 'spread()' and 'gather()' Functions. R package version 0.2.0. https://CRAN.R-project.org/package=tidyr. Git commit 0cdc67ab9a4ae92ce7316dbf3f5eaf4a9afffd5b
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # separate() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(x = c(NA, "a.b", "a.d", "b.c"))
#'
#' df
#' separate(df, "x", c("A", "B"))
#'
#' # If every row doesn't split into the same number of pieces, use
#' # the extra and file arguments to control what happens
#' df <- data.frame(x = c("a", "a b", "a b c", NA))
#'
#' df
#' # this errors
#' #separate(df, "x", c("a", "b"), extra = "error")
#' # this drops
#' separate(df, "x", c("a", "b"), extra = "drop")
#' # this merges
#' separate(df, "x", c("a", "b"), extra = "merge")
#'
#' # again use merge to only separate once
#' df <- data.frame(x = c("x: 123", "y: error: 7"))
#'
#' df
#' separate(df, "x", c("key", "value"), ": ", extra = "merge")
#'
#' @export
separate <- function(data, col, into, sep = "[^[:alnum:]]+", remove = TRUE,
                     convert = FALSE, extra = "error") {
  # based on Git commit 0cdc67ab9a4ae92ce7316dbf3f5eaf4a9afffd5b (C) Hadley Wickham and RStudio, 2014, MIT
  # fill argument is currently dropped as it depends on simplifypieces().
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_string(col)
  check_character(into)
  check_string_or_numeric(sep)

  #-----------------------------------------------------------------------------
  # Separate
  #-----------------------------------------------------------------------------
  value <- as.character(data[[col]])

  if(is.numeric(sep)) {
    l <- strsep(value, sep)
  } else if(is.character(sep)) {
    l <- str_split_fixed(value, sep, length(into), extra = extra)
  } else {
    stop("'sep' must be either numeric or character.", .call = FALSE)
  }

  names(l) <- into
  if(convert) {
    l[] <- lapply(l, utils::type.convert, as.is = TRUE)
  }

  # Insert into existing data frame
  data <- append_df(data, l, which(names(data) == col))
  if(remove) {
    data[[col]] <- NULL
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}

append_df <- function(x, values, after = length(x)) {
  y <- append(x, values, after = after)
  class(y) <- class(x)
  attr(y, "row.names") <- attr(x, "row.names")

  y
}

strsep <- function(x, sep) {
  nchar <- nchar(x)
  pos <- lapply(sep, function(i) {
    if(i >= 0) return(i)
    pmax(0, nchar + i)
  })

  pos <- c(list(0), pos, list(nchar))

  lapply(1:(length(pos) - 1), function(i) {
    substr(x, pos[[i]] + 1, pos[[i + 1]])
  })
}

str_split_fixed <- function(value, sep, n, extra = "error") {
  extra <- match.arg(extra, c("error", "merge", "drop"))

  n_max <- if(extra == "merge") n else Inf

  pieces <- str_split(value, sep, n_max)
  pieces <- lapply(pieces, function(x) {
    # fix for missing values
    if(is.na(x[[1]])) {rep(NA_character_, n)} else {x}
  })
  ns <- vapply(pieces, length, integer(1))

  if(any(ns != n & !is.na(value))) {
    if(extra == "error") {
      stop(
        "Values not split into ", n, " pieces at index ",
        pretty_print(which(ns != n)),
        call. = FALSE
      )
    } else {
      pieces <- lapply(pieces, function(x) x[seq_len(n)])
    }
  }

  # Convert into a list of columns
  #mat <- stringi::stri_list2matrix(pieces, n_min = n, byrow = TRUE)
  mat <- matrix(unlist(pieces), byrow = TRUE, nrow = length(pieces))

  lapply(1:ncol(mat), function(i) mat[, i])
}

str_split <- function(string, pattern, n = Inf) {
  if(length(string) == 0) return(list())

  if(n == 1) {
    as.list(string)
  } else {
    locations <- str_locate_all(string, pattern)
    pieces <- function(mat, string) {
      cut <- mat[seq_len(min(n - 1, nrow(mat))), , drop = FALSE]
      keep <- invert_match(cut)
      str_sub(string, keep[, 1], keep[, 2])
    }
    mapply(
      pieces,
      locations,
      string,
      SIMPLIFY = FALSE,
      USE.NAMES = FALSE
    )
  }
}

str_locate_all <- function(string, pattern) {
  matches <- gregexpr(
    pattern = pattern,
    text = string,
    fixed = FALSE,
    ignore.case = FALSE,
    perl = FALSE
  )
  lapply(matches, match_to_matrix, global = TRUE)
}

match_to_matrix <- function(match, global = FALSE) {
  if(global && length(match) == 1 && (is.na(match) || match == -1)) {
    null <- matrix(0, nrow = 0, ncol = 2)
    colnames(null) <- c("start", "end")
    return(null)
  }

  start <- as.vector(match)
  start[start == -1] <- NA
  end <- start + attr(match, "match.length") - 1L
  cbind(start = start, end = end)
}

invert_match <- function(loc) {
  cbind(
    start = c(0L, loc[, "end"] + 1L),
    end = c(loc[, "start"] - 1L, -1L)
  )
}

str_sub <- function(string, start = 1L, end = -1L) {
  if(length(string) == 0L || length(start) == 0L || length(end) == 0L) {
    return(vector("character", 0L))
  }

  n <- max(length(string), length(start), length(end))
  string <- rep(string, length = n)
  start <- rep(start, length = n)
  end <- rep(end, length = n)

  # Convert negative values into actual positions
  len <- nchar(string)

  neg_start <- !is.na(start) & start < 0L
  start[neg_start] <- start[neg_start] + len[neg_start] + 1L

  neg_end <- !is.na(end) & end < 0L
  end[neg_end] <- end[neg_end] + len[neg_end] + 1L

  substring(string, start, end)
}
