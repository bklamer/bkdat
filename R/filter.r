#' Subset rows of a data frame
#'
#' Subset rows of a data frame. A combination of [dplyr::slice()] and
#' [dplyr::filter()]
#'
#' The evaluation of `x` is applied to each group in a `nest_df` or
#' `group_df` or to the entire data frame in a `data.frame`.
#' There are three allowed outcomes when evaluating `x`:
#' \enumerate{
#'   \item A logical vector with length `nrow(data)`.
#'   \item An integer vector of row positions to keep.
#'   \item A logical vector of length 1 indicating to keep (`TRUE`) or remove
#'   (`FALSE`) the entire data frame for a `data.frame` or individual group
#'   for a `nest_df` or `group_df`. For a `data.frame`, `filter()` will return
#'   a `data.frame` with zero rows if `x` evaluates to `FALSE`.
#' }
#'
#' `x` can also be an integer vector or logical vector of length `nrow(data)`
#' for traditional-like subsetting as `data[x, , drop = FALSE]`.
#'
#' @param .data,data A data frame.
#' @param ... Expressions to be evaluated. Needs to result in a logical vector or
#' integer vector representing the rows to keep.
#' @param x \describe{
#' \item{Character vector}{when evaluated, needs to result in a logical
#' vector or integer vector representing the rows to keep. See details for more
#' information.}
#' \item{Integer vector}{for row positions to keep or remove.}
#' \item{Logical vector}{of length `nrow(data)` for row positions to keep or remove.}
#' }
#' @param .frame,frame A character vector of column names referred to in `x`. This
#' subsets the data frame to only specified columns before evaluation. This
#' provides safer evaluation by preventing accidental use of spurious global
#' variables in place of column names or accidental use of spurious column
#' names in place of global variables.
#' @param .env,env An environment to look for objects outside those in `data`.
#'
#' @return data.frame
#'
#' @seealso [dplyr::filter()],
#'          [dplyr::slice()],
#'          [base::subset()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # filter() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
#'
#' # These expressions will be evaluated to a logical vector of row positions
#' # to keep.
#' filter_(df, "a > 2 | b < 5")
#' filter_(df, "a == 3")
#'
#' filter(df, a > 2 | b < 5)
#' filter(df, a == 3)
#'
#' # These expressions will be evaluated to a numeric vector of row positions
#' # to keep.
#' filter_(df, "which(a == 3)")
#' filter_(df, "a")
#' filter_(df, "1:2")
#' filter_(df, "1")
#' filter_(df, "c(1, 3)")
#' filter_(df, "sample(a, length(a), replace = TRUE)")
#'
#' filter(df, which(a == 3))
#' filter(df, a)
#' filter(df, 1:2)
#' filter(df, 1)
#' filter(df, c(1, 3))
#' filter(df, sample(a, length(a), replace = TRUE))
#'
#' # No evaluation
#' filter_(df, 1)
#' filter_(df, -1)
#' filter_(df, c(TRUE, FALSE, FALSE))
#'
#' filter(df, 1)
#' filter(df, -1)
#' filter(df, c(TRUE, FALSE, FALSE))
#'
#' # For group_df
#' df <- group_by(mtcars, c("cyl", "carb"))
#' filter_(df, "wt < 3.2 | mpg > 20")
#'
#' df <- group_by(mtcars, c("cyl", "carb"))
#' filter(df, wt < 3.2 | mpg > 20)
#'
#' # For nest_df
#' df <- nest_by(mtcars, c("cyl", "carb"))
#' filter_(df, "wt < 3.2 | mpg > 20")
#'
#' df <- nest_by(mtcars, c("cyl", "carb"))
#' filter(df, wt < 3.2 | mpg > 20)
#'
#' @export
filter <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  UseMethod("filter")
}

#' @export
#' @rdname filter
filter.data.frame <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  check_frame(.data, .frame, "bkdat::filter()")
  dots <- dots(..., .check_names = FALSE)
  dots_length <- length(dots)

  #-----------------------------------------------------------------------------
  # evaluate expressions and create rows to keep.
  #-----------------------------------------------------------------------------
  rows <- vector(mode = "list", length = dots_length)
  for(i in seq_len(dots_length)) {
    rows[[i]] <- withCallingHandlers(
      eval(
        expr = dots[[i]],
        envir = if(is.null(.frame)) {.data} else {.data[, .frame, drop = FALSE]},
        enclos = .env
      ),
      error = function(e) stop(
        "Check the dots argument in bkdat::filter(). An error occured while evaluating the expression '",
        dots_char(...)[[i]],
        "'.",
        call. = FALSE
      )
    )
  }

  #-----------------------------------------------------------------------------
  # Check rows for validity.
  #-----------------------------------------------------------------------------
  # If multiple expressions, we need to reduce to a single vector.
  if(dots_length > 1) {
    rows_type <- vapply(X = rows, FUN = class, FUN.VALUE = NA_character_, USE.NAMES = FALSE)
    if(all(rows_type == "logical")) {
      if(length(unique(lengths(rows))) == 1) {
        rows <- Reduce("&", rows)
      } else {
        stop("Check the dots argument in function bkdat::filter(). Multiple expressions were evaluated as logical, but they were not the same length!")
      }
    } else if(all(rows_type %in% c("integer", "numeric"))) {
      rows <- Reduce("intersect", rows)
    } else {
      stop("Check the dots argument in function bkdat::filter(). All expressions need to evaluate to logical or numeric results")
    }
  }

  # Rows are valid if
  #    1. logical length one
  #    2. logical length nrow(data)
  #    3. numeric and less than length data
  #    4, class numeric, integer, or logical.
  rows <- unlist(rows)
  ok_rows <- function(.data, rows) {
    # need to check for valid row type before comparisons.
    if(!inherits(rows, c("numeric", "integer", "logical"))) {
      return(FALSE)
    }
    data_n_rows <- nrow_df(.data)
    rows_length <- length(rows)
    (is.logical(rows) && (rows_length == data_n_rows || rows_length == 1L)) ||
    (is.numeric(rows) && all(rows <= data_n_rows))
  }
  if(!ok_rows(.data, rows)) {
    stop(
      "Check the dots argument in function bkdat::filter(). It needs to return one of the following: 1. logical vector of length one, 2. logical vector of length nrow(data), or 3. Numeric vector with values in range of nrow(data). Try evaluating this manually to check what is wrong."
    )
  }

  # Set NAs to FALSE from a logical evaluation.
  # also handle case where rows is length 1
  if(is.logical(rows)) {
    rows[is.na(rows)] <- FALSE
    if(length(rows) == 1) {
      rows <- rep(rows, nrow_df(.data))
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  .data[rows, , drop = FALSE]
}

#' @export
#' @rdname filter
filter.group_df <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  check_frame(.data, .frame, "bkdat::filter()")
  dots <- dots(...)
  dots_length <- length(dots)

  #-----------------------------------------------------------------------------
  # Evaluate expressions and create rows/groups to keep.
  # Need to evaluate on a group by group basis for the case that dots is a
  # logical of length one. This lets you filter based on groups.
  #-----------------------------------------------------------------------------
  rows <- vector(mode = "list", length = nrow(attr(.data, "group_labels")))
  idx <- attr(.data, "group_indices")
  for(j in seq_along(idx)) {
    for(i in seq_len(dots_length)) {
      rows[[j]][[i]] <- withCallingHandlers(
        eval(
          expr = dots[[i]],
          envir = if(is.null(.frame)) {.data[idx[[j]], ]} else {.data[idx[[j]], .frame, drop = FALSE]},
          enclos = .env
        ),
        error = function(e) stop(
          "Check the dots argument in bkdat::filter(). An error occured while evaluating the expression '",
          dots_char(...)[[i]],
          "'.",
          call. = FALSE
        )
      )
    }
  }

  #-----------------------------------------------------------------------------
  # Check rows for validity.
  #-----------------------------------------------------------------------------
  # If multiple expressions, we need to reduce to a single vector per group.
  if(dots_length > 1) {
    rows_type <- vapply(X = rows[[1]], FUN = class, FUN.VALUE = NA_character_, USE.NAMES = FALSE)
    if(all(rows_type == "logical")) {
      if(length(unique(lengths(rows))) == 1) {
        rows <- lapply(rows, function(x) {Reduce("&", x)})
      } else {
        stop("Check the dots argument in function bkdat::filter(). Multiple expressions were evaluated as logical, but they were not the same length!")
      }
    } else if(all(rows_type %in% c("integer", "numeric"))) {
      rows <- lapply(rows, function(x) {Reduce("intersect", x)})
    } else {
      stop("Check the dots argument in function bkdat::filter(). All expressions need to evaluate to logical or numeric results")
    }
  }

  rows <- lapply(rows, unlist)
  keep_rows <- function(rows) {
    # row type and length (could be logical, integer, numeric, or NULL)
    row_type <- class(rows[[1]])
    if(is.null(row_type)) {
      return(NULL)
    }
    row_len_vec <- lengths(rows)

    # handle the logical case
    if(row_type == "logical") {
      # fix this; all() is fragile for logical returns
      if(all(row_len_vec == 1)) {
        for(i in seq_along(rows)) {
          rows[[i]] <- rep(rows[[i]], length(idx[[i]]))
        }
      }
      rows <- sort.int(unlist(idx)[unlist(rows)])
      return(rows)
    }

    # handle the integer case
    if(row_type %in% c("integer", "numeric")) {
      for(i in seq_along(rows)) {
        rows[[i]] <- idx[[i]][rows[[i]]]
      }
      rows <- sort.int(unlist(rows))
      return(rows)
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  .data[keep_rows(rows), , drop = FALSE]
}

#' @export
#' @rdname filter
filter.nest_df <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # apply filter.data.frame() to each list-column element.
  #-----------------------------------------------------------------------------
  nest_name <- attr(.data, "nest_name")
  .data[[nest_name]] <- lapply(.data[[nest_name]], filter, ..., .frame = .frame, .env = .env)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  .data
}
