#-------------------------------------------------------------------------------
# Functions copied over from bkmisc package
# Can't import them because
#     - it would create circular dependency
#     - the goal of this package it to have minimal dependencies
# Check bkmisc for documentation.
#-------------------------------------------------------------------------------
# Return list of calls
# changed warning to stop here...
dots <- function(..., .check_names = TRUE) {
  # Capture the dots
  dots <- as.list(substitute(list(...)))[-1]
  # Handle cases where names are missing
  if(.check_names) {
    dots_names <- names(dots)
    if(any(dots_names == "") || is.null(dots_names)) {
      are_missing <- if(is.null(dots_names)) {
        rep(TRUE, length(dots))
      } else {
        dots_names == ""
      }
      dots_char <- vapply(
        X = dots,
        FUN = deparse,
        FUN.VALUE = NA_character_,
        USE.NAMES = FALSE
      )
      dots_names[are_missing] <- dots_char[are_missing]
      names(dots) <- dots_names
    }
    # Stop or Warning if duplicate names?
    if(any(duplicated.default(dots_names))) {
      stop("The dots argument results in duplicated names!")
    }
  }
  # Return
  dots
}

# Return character vector
# changed warning to stop here...
dots_char <- function(..., .check_names = TRUE) {
  # Capture the dots
  dots <- vapply(
    X = as.list(substitute(list(...)))[-1],
    FUN = deparse,
    FUN.VALUE = NA_character_,
    USE.NAMES = TRUE
  )
  # Handle cases where names are missing
  if(.check_names) {
    # If names will be used/matter, we'll process them here
    dots_names <- names(dots)
    if(any(dots_names == "") || is.null(dots_names)) {
      are_missing <- if(is.null(dots_names)) {
        rep(TRUE, length(dots))
      } else {
        dots_names == ""
      }
      dots_names[are_missing] <- dots[are_missing]
      names(dots) <- dots_names
    }
    # Stop or Warning if duplicate names?
    if(any(duplicated.default(dots_names))) {
      stop("The dots argument results in duplicated names!")
    }
  }
  # Return
  dots
}

seq2 <- function(from, to) {
  if(from > to) {
    return(integer(0))
  }
  seq.int(from = from, to = to)
}

all_true <- function(x) {
  # Often use >, <, or == inside all(). But they don't handle NAs well.
  isTRUE(all(x))
}

nrow_df <- function(x) {
  .row_names_info(x, 2L)
}

ncol_df <- function(x) {
  length(x)
}

if_null_else <- function(x, y) {if(is.null(x)) {y} else {x}}

pretty_print <- function(x, max = 6, quote = TRUE) {
  if(quote) {
    x <- shQuote(x)
  }
  if(length(x) > max) {
    x <- c(x[seq_len(max)], "...")
  }
  if(quote && length(x) > 1) {
    shQuote(paste0(x, collapse = ", "))
  } else {
    paste0(x, collapse = ", ")
  }
}

clean_list <- function(x){
  #-----------------------------------------------------------------------------
  # - Since this function is recursive, just return x if it is not a list.
  # This saves dataframes and other types from being converted to lists
  # by the functions below.
  # - Could also surround the lapply function below with this if statement.
  # - Any other way to do argument checks here???
  #-----------------------------------------------------------------------------
  if(!inherits(x, "list")) {
    return(x)
  }

  #-----------------------------------------------------------------------------
  # Determines if the element should be removed.
  #-----------------------------------------------------------------------------
  is_bad <- function(y) {
    elements <- unlist(y, use.names = FALSE)
    is.null(elements) || (length(elements) == 0)
  }
  x <- Filter(Negate(is_bad), x)

  #-----------------------------------------------------------------------------
  # Recursively remove bad elements.
  # https://stackoverflow.com/q/26539441
  #-----------------------------------------------------------------------------
  x <- lapply(x, function(y) {
    Filter(length, clean_list(y))
  })

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}

#-------------------------------------------------------------------------------
# A safe environment to be used for arrange.
#-------------------------------------------------------------------------------
arrange_env <- new.env(parent = emptyenv())
arrange_env[["desc"]] <- get("desc", envir = asNamespace("bkdat"))
arrange_env[["order"]] <- get("order", pos = "package:base")

#-------------------------------------------------------------------------------
# id() and id_var() for join() and spread().
#-------------------------------------------------------------------------------
# Compute a unique numeric id for each unique row in a data frame.
#
# Properties:
# \itemize{
#   \item \code{order(id)} is equivalent to \code{do.call(order, df)}
#   \item rows containing the same data have the same value
#   \item if \code{drop = FALSE} then room for all possibilites
# }
#
# @param .variables list of variables
# @param drop drop unusued factor levels?
#
# @return a numeric vector with attribute n, giving total number of possibilities
id <- function(.variables, drop = FALSE) {
  # Removed code to drop all zero length .variables since spread with drop = FALSE
  # keeps missing combinations of 0-length factors

  if(length(.variables) == 0) {
    n <- if(is.null(nrow(.variables))) {
      0L
    } else {
      nrow(.variables)
    }
    return(structure(seq_len(n), n = n))
  }

  # Special case for single variable
  if(length(.variables) == 1L) {
    return(id_var(.variables[[1]], drop = drop))
  }

  # Calculate individual ids
  ids <- rev(lapply(.variables, id_var, drop = drop))
  p <- length(ids)

  # Calculate dimensions
  ndistinct <- vapply(
    X = ids,
    FUN = attr,
    which = "n",
    FUN.VALUE = integer(1L),
    USE.NAMES = FALSE
  )

  n <- prod(ndistinct)
  if(n > 2 ^ 31) {
    # Too big for integers, have to use strings, which will be much slower :(
    char_id <- do.call("paste", c(ids, sep = "\r"))
    res <- match(char_id, unique(char_id))
  } else {
    combs <- c(1, cumprod(ndistinct[-p]))
    mat <- do.call("cbind", ids)
    res <- c((mat - 1L) %*% combs + 1L)
  }

  attr(res, "n") <- n
  if(drop) {
    id_var(res, drop = TRUE)
  } else {
    structure(as.integer(res), n = attr(res, "n"))
  }
}

# Numeric id for a vector.
id_var <- function(x, drop = FALSE) {
  if(!is.null(attr(x, "n")) && !drop) return(x)

  if(is.factor(x) && !drop) {
    x <- addNA(x, ifany = TRUE)
    id <- as.integer(x)
    n <- length(levels(x))
  } else if(length(x) == 0L) {
    id <- integer()
    n <- 0L
  } else if(is.list(x) || is.raw(x)) {
    # Sorting lists or raw is not supported
    levels <- unique(x)
    id <- match(x, levels)
    n <- max(id)
  } else {
    levels <- sort(unique(x), na.last = TRUE)
    id <- match(x, levels)
    n <- max(id)
  }

  # Return
  structure(id, n = n)
}

# set names of an object
set_names <- function(x, names) {
  names(x) <- names
  x
}
