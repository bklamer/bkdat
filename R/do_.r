#' @export
#' @rdname do
do_ <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_group_df(data)
  check_character(x, check_missing = TRUE)
  check_frame(data, frame)

  #-----------------------------------------------------------------------------
  # Apply functions on groups
  #-----------------------------------------------------------------------------
  # create new env so we can assign dot (.) as the current group df.
  env <- new.env(parent = env)
  res <- attr(data, "group_labels")
  idx <- attr(data, "group_indices")
  comp <- replicate(length(x), vector("list", nrow_df(res)), simplify = FALSE)

  # this indexing order should error out faster if there are issues with
  # evaluating the expressions?
  for(i in seq_along(idx)) {
    env$. <- if(is.null(frame)) {data[idx[[i]], , drop = FALSE]} else {data[idx[[i]], frame, drop = FALSE]}
    for(j in seq_along(x)) {
      # need error catching here
      comp[[j]][[i]] <- withCallingHandlers(
        eval(
          expr = parse(
            file = stdin(),
            n = NULL,
            text = x[[j]],
            prompt = "?",
            keep.source = FALSE,
            srcfile = NULL,
            encoding = "unknown"
          ),
          envir = env
        ),
        error = function(e) stop(
          "Check the 'x' argument in function bkdat::do(). An error occured while evaluating the expression '",
          x[[j]],
          "'.",
          call. = FALSE
        )
      )
    }
  }
  names(comp) <- names(x)

  # Only un-named dataframe results should be cbind'd below. all others will
  # be stored in a list.
  for(i in seq_along(comp)) {
    col <- comp[[i]]
    if(any(!vapply(X = col, FUN = is.data.frame, FUN.VALUE = logical(1)))) {
      res[[names(comp)[i]]] <- comp[[i]]
    } else {
      if(names(comp[i]) == "" || is.null(names(comp[i]))) {
        res <- cbind(res, bind_rows(col))
      } else {
        res[[names(comp)[i]]] <- comp[[i]]
      }
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}
