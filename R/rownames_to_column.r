#' Move row names to a column
#'
#' Moves row names to a column in the data frame.
#'
#' @param x A data frame
#' @param name A string for the row names new column name.
#' @param before,after An **integer** for the column index or **string**
#' for the column name of where to move the columns. Defaults to after
#' last column. Only specify one of these arguments.
#'
#' @return `data.frame`
#'
#' @seealso [tibble::rownames_to_column()]
#'
#' @export
rownames_to_column <- function(
    x,
    name,
    before = NULL,
    after = NULL
) {
  rn <- set_names(list(rownames(x)), name)

  x |>
    add_column(rn, before = before, after = after) |>
    reset_row_names()
}
