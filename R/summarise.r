#' Summarise columns of a data frame
#'
#' Summarise creates a new data frame that will have one row per group.
#'
#' @param .data,data A data frame.
#' @param ... Expressions to be evaluated.
#' @param x A character vector with name-value pairs of expressions.
#' @param .frame,frame A character vector of column names referred to in `x`.
#' This subsets the data frame to only specified columns before evaluation. This
#' provides safer evaluation by preventing accidental use of spurious global
#' variables in place of column names or accidental use of spurious column
#' names in place of global variables.
#' @param .env,env An environment to look for objects outside those in `data`.
#'
#' @return `data.frame`
#'
#' @seealso [dplyr::summarise()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # summarise() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
#' summarise(df, mean = mean(a), median = median(b))
#' summarise_(df, c(mean = "mean(a)", median = "median(b)"))
#'
#' @export
summarise <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  UseMethod("summarise")
}

#' @export
#' @rdname summarise
summarise.data.frame <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  dots <- dots(...)
  check_frame(.data, .frame, "bkdat::summarise()")
  dots_names <- names(dots)
  if(any(duplicated.default(dots_names))) {
    stop(
      "Check the dots argument in bkdat::summarise(). The names need to be unique (no recycling or progressively applying functions)."
    )
  }

  #-----------------------------------------------------------------------------
  # Evaluate summary values and put them in a data frame.
  #-----------------------------------------------------------------------------
  res <- vector(mode = "list", length = length(dots_names))
  names(res) <- dots_names
  for(i in dots_names) {
    res[[i]] <- withCallingHandlers(
      eval(
        expr = dots[[i]],
        envir = if(is.null(.frame)) {.data} else {.data[, .frame, drop = FALSE]},
        enclos = .env
      ),
      error = function(e) stop(
        "Check the dots argument in function bkdat::summarise(). An error occured while evaluating the expression '",
        dots_char(...)[[i]],
        "'. You might have used non-existing column names or wrongly-formed functions.",
        call. = FALSE
      )
    )
  }
  res <- as_df(res)

  #-----------------------------------------------------------------------------
  # Check for length 1 summary values
  #-----------------------------------------------------------------------------
  if(nrow_df(res) > 1) {
    stop(
      "Check the dots argument in function bkdat::summarise(). After evaluatiion, the returned data frame had more than one row per summary."
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' @export
#' @rdname summarise
summarise.group_df <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_frame(.data, .frame, "bkdat::summarise()")
  dots <- dots(...)
  dots_names <- names(dots)
  if(any(duplicated.default(dots_names))) {
    stop(
      "Check the dots argument in bkdat::summarise(). The names need to be unique (no recycling or progressively applying functions)."
    )
  }

  #-----------------------------------------------------------------------------
  # Evaluate summary values and put them in a data frame.
  #-----------------------------------------------------------------------------
  res <- attr(.data, "group_labels")
  tmp_col <- vector(mode = "list", length = nrow_df(res))
  idx <- attr(.data, "group_indices")

  for(i in dots_names) {
    for(j in seq_along(idx)) {
      tmp_col[[j]] <- withCallingHandlers(
        eval(
          expr = dots[[i]],
          envir = if(is.null(.frame)) {.data[idx[[j]], ]} else {.data[idx[[j]], .frame, drop = FALSE]},
          enclos = .env
        ),
        error = function(e) stop(
          "Check the dots argument in function bkdat::summarise(). An error occured while evaluating the expression '",
          dots_char(...)[[i]],
          "'. You might have used non-existing column names or wrongly-formed functions.",
          call. = FALSE
        )
      )
    }
    res[[i]] <- unlist(tmp_col)
  }

  #-----------------------------------------------------------------------------
  # Check for length 1 summary values
  #-----------------------------------------------------------------------------
  if(nrow_df(res) != nrow_df(attr(.data, "group_labels"))) {
    stop(
      "Check the dots argument in function bkdat::summarise(). After evaluation the returned data frame had more than one row per summary."
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' @export
#' @rdname summarise
summarise.nest_df <- function(.data, ..., .frame = NULL, .env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # apply summarise.data.frame() to each list-column element.
  #-----------------------------------------------------------------------------
  nest_name <- attr(.data, "nest_name")
  res <- attr(.data, "group_labels")
  summary <- bind_rows(
    lapply(
      .data[[nest_name]],
      summarise,
      ... = ...,
      .frame = .frame,
      .env = .env
    )
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  bind_cols(list(res, summary))
}
