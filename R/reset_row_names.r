#' Reset data frame row names
#'
#' Quickly reset data frame row names to integers.
#'
#' @param data A data frame.
#'
#' @return `data.frame`
#'
#' @seealso [tibble::remove_rownames()]
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # reset_row_names() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = 1:5)
#' rownames(df) <- 5:1
#' rownames(df)
#'
#' df <- reset_row_names(df)
#' rownames(df)
#'
#' @export
reset_row_names <- function(data) {
  # rni > 0 excludes cases of data=NULL or data=NA
  rni <- .row_names_info(data, 2L)
  if(rni > 0) {
    attr(data, "row.names") <- .set_row_names(rni)
    data
  } else {
    data
  }
}
