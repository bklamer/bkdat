#' Join two data frames together
#'
#' Join two data frames together.
#'
#' Rownames are dropped. Attributes are dropped. Be careful joining by columns
#' that have different classes or factor levels. If mixed character/factor
#' columns are joined by, then output should be coerced to character. If columns
#' with different factor levels are joined by, then will be automatically
#' reordered.
#'
#' @param x,y A data frame.
#' @param by A character vector of variables to join by. Defaults to using the common names across the two data frames.
#' @param suffix A character vector of length 2. Duplicated variables that are not joined by will have this suffix added to their names.
#' @param backend A character string of the codebase to use for joining. Options are "merge" ([base::merge()]) or "plyr" ([plyr::join()]). Defaults to "merge".
#'
#' @return `data.frame`
#'
#' @seealso [base::merge()],
#'          [plyr::join()],
#'          [dplyr::join()]
#'
#' @references Hadley Wickham (2014). plyr: Tools for Splitting, Applying and Combining Data. R package version 1.8.4. https://CRAN.R-project.org/package=plyr. Git commit fe192412748d1ccb6d524caeb5bcf2b5c474fd46
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # join_*() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df1 <- data.frame(
#'   a = letters[1:3],
#'   b = letters[4:6],
#'   c = letters[7:9],
#'   stringsAsFactors = FALSE
#' )
#' df2 <- data.frame(
#'   a = letters[2:4],
#'   b = letters[10:12],
#'   d = letters[13:15],
#'   stringsAsFactors = FALSE
#' )
#'
#' df1
#' df2
#'
#' join_left(df1, df2, by = "a")
#' join_right(df1, df2, by = "a")
#' join_inner(df1, df2, by = "a")
#' join_full(df1, df2, by = "a")
#'
#' @name join
NULL

#' @export
#' @rdname join
join_left <- function(x, y, by = NULL, suffix = c(".x", ".y"), backend = "merge") {
  backend <- match.arg(backend, c("merge", "plyr"))
  switch(
    backend,
    "merge" = join_left_merge(x, y, by, suffix),
    "plyr" = join_left_plyr(x, y, by, suffix)
  )
}

join_left_merge <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_left()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_left()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join and re-order columns
  #-----------------------------------------------------------------------------
  out <- merge(x, y, by = by, all.x = TRUE, suffixes = suffix, sort = FALSE)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }

  col_order <- name_info$out_names_jc

  out <- out[, col_order, drop = FALSE]

  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}

join_left_plyr <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_left()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_left()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join
  #-----------------------------------------------------------------------------
  y.cols <- setdiff(names(y), by)
  ids <- join_ids(x, y, by, all = TRUE)
  out <- cbind(x[ids$x, , drop = FALSE], y[ids$y, y.cols, drop = FALSE])
  out <- reset_row_names(out)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }
  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # return
  #-----------------------------------------------------------------------------
  out
}

#' @export
#' @rdname join
join_right <- function(x, y, by = NULL, suffix = c(".x", ".y"), backend = "merge") {
  backend <- match.arg(backend, c("merge", "plyr"))
  switch(
    backend,
    "merge" = join_right_merge(x, y, by, suffix),
    "plyr" = join_right_plyr(x, y, by, suffix)
  )
}

join_right_merge <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_right()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_right()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join and re-order columns
  #-----------------------------------------------------------------------------
  out <- merge(x, y, by = by, all.y = TRUE, suffixes = suffix, sort = FALSE)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }

  col_order <- name_info$out_names_jc

  out <- out[, col_order, drop = FALSE]

  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}

join_right_plyr <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_right()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_right()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join
  #-----------------------------------------------------------------------------
  x.cols <- setdiff(names(x), by)
  y.cols <- setdiff(names(y), by)
  ids <- join_ids(y, x, by, all = TRUE)
  out <- cbind(
    y[ids$x, by, drop = FALSE],
    x[ids$y, x.cols, drop = FALSE],
    y[ids$x, y.cols, drop = FALSE]
  )
  out <- reset_row_names(out)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }
  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # return
  #-----------------------------------------------------------------------------
  out
}

#' @export
#' @rdname join
join_inner <- function(x, y, by = NULL, suffix = c(".x", ".y"), backend = "merge") {
  backend <- match.arg(backend, c("merge", "plyr"))
  switch(
    backend,
    "merge" = join_inner_merge(x, y, by, suffix),
    "plyr" = join_inner_plyr(x, y, by, suffix)
  )
}

join_inner_merge <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_inner()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_inner()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join and re-order columns
  #-----------------------------------------------------------------------------
  out <- merge(x, y, by = by, all.x = FALSE, all.y = FALSE, suffixes = suffix, sort = FALSE)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }

  col_order <- name_info$out_names_jc

  out <- out[, col_order, drop = FALSE]

  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}

join_inner_plyr <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_inner()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_inner()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join
  #-----------------------------------------------------------------------------
  y.cols <- setdiff(names(y), by)
  ids <- join_ids(x, y, by)
  out <- cbind(x[ids$x, , drop = FALSE], y[ids$y, y.cols, drop = FALSE])
  out <- reset_row_names(out)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }
  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # return
  #-----------------------------------------------------------------------------
  out
}

#' @export
#' @rdname join
join_full <- function(x, y, by = NULL, suffix = c(".x", ".y"), backend = "merge") {
  backend <- match.arg(backend, c("merge", "plyr"))
  switch(
    backend,
    "merge" = join_full_merge(x, y, by, suffix),
    "plyr" = join_full_plyr(x, y, by, suffix)
  )
}

join_full_merge <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_full()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_full()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join and re-order columns
  #-----------------------------------------------------------------------------
  out <- merge(x, y, by = by, all.x = TRUE, all.y = TRUE, suffixes = suffix, sort = FALSE)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }

  col_order <- name_info$out_names_jc

  out <- out[, col_order, drop = FALSE]

  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}

join_full_plyr <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(x)
  check_data_frame(y)
  check_suffix(suffix, fun = "bkdat::join_full()")
  x_names <- names(x)
  y_names <- names(y)
  check_by(by, x_names, y_names, fun = "bkdat::join_full()")

  #-----------------------------------------------------------------------------
  # Handle column names for by and suffix scenarios
  #-----------------------------------------------------------------------------
  name_info <- join_names(x, y, by, suffix)
  if(!identical(name_info$x_names, name_info$x_names_jc) || !identical(name_info$y_names, name_info$y_names_jc)) {
    names(x) <- name_info[["x_names_jc"]]
    names(y) <- name_info[["y_names_jc"]]
    by <- name_info[["by_x_jc"]]
  } else {
    by <- name_info[["by_x"]]
  }

  #-----------------------------------------------------------------------------
  # Join
  #-----------------------------------------------------------------------------
  y.cols <- setdiff(names(y), by)
  ids <- join_ids(x, y, by, all = TRUE)
  matched <- cbind(x[ids$x, , drop = FALSE],
                   y[ids$y, y.cols, drop = FALSE])
  unmatched <- y[setdiff(seq_len(nrow_df(y)), ids$y), , drop = FALSE]
  out <- bind_fill_rows(list(matched, unmatched))
  out <- reset_row_names(out)

  if(!all(names(out) %in% name_info$out_names_jc)) {
    stop("join names are wrong.")
  }
  names(out) <- name_info$out_names

  #-----------------------------------------------------------------------------
  # return
  #-----------------------------------------------------------------------------
  out
}

# keeps all observations in x that have a match in y
# an inner join without Y variable values?
semi_join <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  if(is.null(by)) {
    by <- intersect(names(x), names(y))
    message("Joining by: ", paste(by, collapse = ", "))
  }

  NULL
}

# https://stackoverflow.com/questions/28702960/find-complement-of-a-data-frame-anti-join
# drops all observations in x that have a match in y. inverse of semi join.
anti_join <- function(x, y, by = NULL, suffix = c(".x", ".y")) {
  if(is.null(by)) {
    by <- intersect(names(x), names(y))
    message("Joining by: ", paste(by, collapse = ", "))
  }

  NULL
}
