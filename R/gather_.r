#' @export
#' @rdname gather
gather_ <- function(data, key, value, cols) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(arg = data)
  check_string(arg = key)
  check_string(arg = value)
  check_character(arg = cols)
  check_name_in_df(name = cols, df = data)

  #-----------------------------------------------------------------------------
  # Gather the columns
  #-----------------------------------------------------------------------------
  data_names <- names(data)
  df <- data[, !(data_names %in% cols), drop = FALSE]
  df <- bind_rows(
    lapply(seq_along(cols), function(x) {
      df[[key]] <- cols[x]
      df[[value]] <- data[[cols[x]]]
      df
    })
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  df
}
