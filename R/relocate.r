#' Move columns within a data frame
#'
#' Move one or more columns within an existing data frame.
#'
#' @param data A data frame.
#' @param x A character vector of column names, integer vector of positions, or
#' logical vector of positions that will be moved.
#' @param before,after An **integer** for the column index or **string**
#' for the column name of where to move the columns. Defaults to after
#' last column. Only specify one of these arguments.
#'
#' @return `data.frame`
#'
#' @seealso [dplyr::relocate()]
#'
#' @references Hadley Wickham, Romain François, Lionel Henry and Kirill Müller (2020). dplyr: A Grammar of Data Manipulation. R package version 1.0.2. https://CRAN.R-project.org/package=dplyr
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # relocate() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = 1, b = 1, c = 1, d = "a", e = "a", f = "a")
#' relocate(data = df, x = "b", before = "e")
#' relocate(df, "a", after = "c")
#' relocate(df, "f", before = "b")
#' relocate(df, "a")
#'
#' # Can also select variables based on their type
#' relocate(df, x = unlist(lapply(df, is.character)))
#' relocate(df, x = unlist(lapply(df, is.numeric)))
#'
#' relocate(df, x = unlist(lapply(df, is.character)), after = "b")
#' relocate(df, x = unlist(lapply(df, is.numeric)), before = "e")
#'
#' @export
relocate <- function(data, x, before = NULL, after = NULL) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  # data arg
  check_data_frame(data, check_missing = TRUE)

  # x arg
  if(is.character(x)) {
    are_ok <- x %in% names(data)
    if(!all(are_ok)) {
      bad_names <- x[!(are_ok)]
      stop(
        "Check the 'x' argument in function bkdat::relocate(). Unknown column names: ",
        pretty_print(bad_names),
        "."
      )
    }

    x <- which(names(data) %in% x)
  } else if(is.numeric(x)) {
    are_bad <- abs(x) > ncol_df(data)
    if(any(are_bad)) {
      bad_idx <- x[are_bad]
      stop(
        "Check the 'x' argument in function bkdat::relocate(). 'data' only has ",
        ncol(data),
        " columns, but 'x' is pointing to column positions ",
        pretty_print(bad_idx),
        "."
      )
    }
  } else if(is.logical(x)){
    if(length(x) != ncol_df(data)) {
      stop(
        "Check the 'x' argument in function bkdat::relocate(). 'data' has ",
        ncol(data),
        " columns, but 'x' is a logical vector of length ",
        length(x),
        ". 'x' needs to be the same length as the number of columns in 'data'."
      )
    }

    x <- which(x)
  } else {
    stop(
      "Check the 'x' argument in function bkdat::relocate(). 'x' has class ",
      pretty_print(class(x)),
      " but needs to be a character, numeric, or logical vector."
    )
  }

  # before and after arg
  has_before <- !is.null(before)
  has_after <- !is.null(after)
  if(has_before & has_after) (
    stop(
      "Check the 'before' and 'after' arguments in function bkdat::relocate(). Specify only one of them."
    )
  )

  pos <- c(before, after)

  if(is.character(pos)) {
    are_ok <- pos %in% names(data)
    if(!all(are_ok)) {
      bad_names <- pos[!(are_ok)]
      stop(
        "Check the 'before' and 'after' arguments in function bkdat::relocate(). Unknown column names: ",
        pretty_print(bad_names),
        "."
      )
    }

    pos <- which(names(data) %in% pos)
  } else if(is.numeric(pos)) {
    are_bad <- abs(x) > ncol_df(data)
    if(any(are_bad)) {
      bad_idx <- x[are_bad]
      stop(
        "Check the 'before' and 'after' arguments in function bkdat::relocate(). 'data' only has ",
        ncol(data),
        " columns, but you are pointing to position ",
        pretty_print(bad_idx),
        "."
      )
    }
  } else {
    # last column if not specified
    pos <- ncol(data)
  }

  if(has_before) {
    if (!pos %in% x) {
      x <- c(x, pos)
    }
  } else if(has_after) {
    if (!pos %in% x) {
      x <- c(pos, x)
    }
  } else {
    if (!pos %in% x) {
      x <- c(pos, x)
    }
  }

  lhs <- setdiff(seq2(from = 1, to = pos - 1), x)
  rhs <- setdiff(seq2(from = pos + 1, to = ncol(data)), x)

  pos <- unique(c(lhs, x, rhs))
  out <- data[pos]
  new_names <- names(data)[pos]

  if (!is.null(new_names)) {
    names(out)[new_names != ""] <- new_names[new_names != ""]
  }
  out
}
