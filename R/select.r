#' Subset a data frame by column name or column position
#'
#' Subsets a data frame to only columns specified. Will not 'drop' to vector
#' type. This is only a convenience function that replicates the standard
#' `data[, x, drop = FALSE]`.
#'
#' @param data A data frame.
#' @param x A character, numeric, or logical vector for specified columns.
#'
#' @return `data.frame`
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # select() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
#' select(df, "a")
#' select(df, 2)
#' select(df, c(TRUE, FALSE))
#'
#' @export
select <- function(data, x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)

  if(is.character(x)) {
    are_ok <- x %in% names(data)
    if(!all(are_ok)) {
      bad_names <- x[!(are_ok)]
      stop(
        "Check the 'x' argument in function bkdat::select(). Unknown column names: ",
        pretty_print(bad_names),
        "."
      )
    }
  } else if(is.numeric(x)) {
    are_bad <- abs(x) > ncol_df(data)
    if(any(are_bad)) {
      bad_idx <- x[are_bad]
      stop(
        "Check the 'x' argument in function bkdat::select(). 'data' only has ",
        ncol(data),
        " columns, but 'x' is trying to select column positions ",
        pretty_print(bad_idx),
        "."
      )
    }
  } else if(is.logical(x)){
    if(length(x) != ncol_df(data)) {
      stop(
        "Check the 'x' argument in function bkdat::select(). 'data' has ",
        ncol(data),
        " columns, but 'x' is a logical vector of length ",
        length(x),
        ". 'x' needs to be the same length as the number of columns in 'data'."
      )
    }
  } else {
    stop(
      "Check the 'x' argument in function bkdat::select(). 'x' has class ",
      pretty_print(class(x)),
      " but needs to be a character, numeric, or logical vector."
    )
  }

  #-----------------------------------------------------------------------------
  # Return data
  #-----------------------------------------------------------------------------
  data[, x, drop = FALSE]
}
