#' Pull out a single column
#'
#' Extract a single column from a data frame and return as an atomic vector.
#' Defaults to pulling the last column from a data frame.
#'
#' @param data A data frame.
#' @param x A character string (name) or integer (position) for specified variable.
#'
#' @return Atomic vector
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # pull() examples
#' #----------------------------------------------------------------------------
#' library(bkdat)
#'
#' df <- data.frame(a = c(1, 2, 3), b = c(4, 5, 6))
#' pull(df, "a")
#' pull(df, 2)
#'
#' @export
pull <- function(data, x = NULL) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  if(is.null(x)) {
    # grab last column
    x <- ncol(data)
  } else {
    # check for problems
    if(is.numeric(x)) {
      if((x < 1 || x > ncol(data))) {
        stop(
          "Check the 'x' argument in function bkdat::pull(). ",
          pretty_print(x),
          " is not a valid column position.",
          call. = FALSE
        )
      }
    } else if(is_string(x)) {
      if(!(x %in% names(data))) {
        stop(
          "Check the 'x' argument in function bkdat::pull(). ",
          pretty_print(x),
          " is not a valid column name.",
          call. = FALSE
        )
      }
    } else {
      stop(
        "Check the 'x' argument in function bkdat::pull(). 'x' has class ",
        pretty_print(class(x)),
        " but needs to be a string or integer.",
        call. = FALSE
      )
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data[[x]]
}
