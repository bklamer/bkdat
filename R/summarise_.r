#' @export
#' @rdname summarise
summarise_ <- function(data, x, frame = NULL, env = parent.frame()) {
  UseMethod("summarise_")
}

#' @export
#' @rdname summarise
summarise_.data.frame <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_named_character(x)
  check_frame(data, frame)
  x_names <- names(x)
  if(any(duplicated.default(x_names))) {
    stop(
      "Check the 'x' argument in bkdat::summarise_(). The names in 'x' need to be unique (no recycling or progressively applying functions)."
    )
  }

  #-----------------------------------------------------------------------------
  # Evaluate summary values and put them in a data frame.
  #-----------------------------------------------------------------------------
  res <- vector(mode = "list", length = length(x))
  names(res) <- x_names
  for(i in x_names) {
    res[[i]] <- withCallingHandlers(
      eval(
        expr = parse(
          file = stdin(),
          n = NULL,
          text = x[i],
          prompt = "?",
          keep.source = FALSE,
          srcfile = NULL,
          encoding = "unknown"
        ),
        envir = if(is.null(frame)) {data} else {data[, frame, drop = FALSE]},
        enclos = env
      ),
      error = function(e) stop(
        "Check the 'x' argument in function bkdat::summarise_(). An error occured while evaluating the expression '",
        paste0(x[i]),
        "'. You might have used non-existing column names or wrongly-formed functions.",
        call. = FALSE
      )
    )
  }
  res <- as_df(res)

  #-----------------------------------------------------------------------------
  # Check for length 1 summary values
  #-----------------------------------------------------------------------------
  if(nrow_df(res) > 1) {
    stop(
      "Check the 'x' argument in function bkdat::summarise_(). After evaluating '",
      paste0(x),
      "', the returned data frame had more than one row per summary."
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' @export
#' @rdname summarise
summarise_.group_df <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_named_character(x)
  check_frame(data, frame)
  x_names <- names(x)
  if(any(duplicated.default(x_names))) {
    stop(
      "Check the 'x' argument in bkdat::summarise_(). The names in 'x' need to be unique (no recycling or progressively applying functions)."
    )
  }

  #-----------------------------------------------------------------------------
  # Evaluate summary values and put them in a data frame.
  #-----------------------------------------------------------------------------
  res <- attr(data, "group_labels")
  tmp_col <- vector(mode = "list", length = nrow_df(res))
  idx <- attr(data, "group_indices")

  for(i in x_names) {
    for(j in seq_along(idx)) {
      tmp_col[[j]] <- withCallingHandlers(
        eval(
          expr = parse(
            file = stdin(),
            n = NULL,
            text = x[i],
            prompt = "?",
            keep.source = FALSE,
            srcfile = NULL,
            encoding = "unknown"
          ),
          envir = if(is.null(frame)) {data[idx[[j]], ]} else {data[idx[[j]], frame, drop = FALSE]},
          enclos = env
        ),
        error = function(e) stop(
          "Check the 'x' argument in function bkdat::summarise_(). An error occured while evaluating the expression '",
          paste0(x[i]),
          "'. You might have used non-existing column names or wrongly-formed functions.",
          call. = FALSE
        )
      )
    }
    res[[i]] <- unlist(tmp_col)
  }

  #-----------------------------------------------------------------------------
  # Check for length 1 summary values
  #-----------------------------------------------------------------------------
  if(nrow_df(res) != nrow_df(attr(data, "group_labels"))) {
    stop(
      "Check the 'x' argument in function bkdat::summarise_(). After evaluating '",
      paste0(x),
      "', the returned data frame had more than one row per summary."
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}

#' @export
#' @rdname summarise
summarise_.nest_df <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # apply summarise_.data.frame() to each list-column element.
  #-----------------------------------------------------------------------------
  nest_name <- attr(data, "nest_name")
  res <- attr(data, "group_labels")
  summary <- bind_rows(
    lapply(
      data[[nest_name]],
      summarise_,
      x = x,
      frame = frame,
      env = env
    )
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  bind_cols(list(res, summary))
}
