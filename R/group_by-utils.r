#-------------------------------------------------------------------------------
# base::unique.data.frame() was re-written so that it strips row names and
# replaces them in compact form.
# base::duplicated.data.frame() was re-written for a small speed benefit
# and to simplify the arguments.
#-------------------------------------------------------------------------------
# based on base::duplicated()
# x must be a dataframe.
# returns a logical vector.
group_duplicated_df <- function(x) {
  if(ncol_df(x) > 1L) {
    # row-wise uniqueness
    duplicated(do.call("paste", c(x, sep = "\\r")), fromLast = FALSE)
  } else {
    # vector uniqueness
    duplicated(x[[1L]], fromLast = FALSE)
  }
}

# based on base::unique()
# x must be a dataframe.
# returns a data.frame.
group_unique_df <- function(x) {
  x <- x[!group_duplicated_df(x), , drop = FALSE]
  attr(x, "row.names") <- seq_len(nrow_df(x))
  x
}

#-------------------------------------------------------------------------------
# base::interaction() was originally used to create the group_levels() factor
# vector. However, the automatic ordering of factor levels created an issue in
# nest_by(). So base::interaction() and base::factor() are re-written below to
# allow either unordered factor levels or lexically ordered factor levels.
# The current logic behind using all these functions should probably be
# changed to something else.
#-------------------------------------------------------------------------------
# based on base::factor()
# x is a vector (will be converted to character in function).
# returns a factor vector.
group_factor <- function(x, labels = levels, arrange = TRUE) {
  # checks
  if(is.null(x)) {
    x <- character()
  }

  # levels
  y <- unique(x)
  if(arrange) {
    idx <- sort.list(y)
  } else {
    idx <- seq_along(y)
  }
  levels <- unique(as.character(y)[idx])

  # values
  if(!is.character(x)) {
    x <- as.character(x)
  }
  f <- match(x, levels)

  # return
  nl <- length(labels)
  nL <- length(levels)
  if (!any(nl == c(1L, nL)))
    stop(gettextf("invalid 'labels'; length %d should be 1 or %d",
                  nl, nL), domain = NA)
  levels(f) <- if (nl == nL)
    as.character(labels)
  else paste0(labels, seq_along(levels))
  class(f) <- "factor"
  f
}

# based on base::factor()
# group_labels must be data.frame.
# returns a factor vector.
group_interaction <- function(group_labels, drop = TRUE, sep = "|", arrange = TRUE) {
  n_cols <- ncol_df(group_labels)

  for(i in n_cols:1L) {
    f <- group_factor(group_labels[[i]], arrange = arrange)[, drop = drop]
    l <- attr(f, "levels")
    if1 <- as.integer(f) - 1L
    if(i == n_cols) {
      ans <- if1
      lvs <- l
    } else {
      if(arrange) {
        ll <- length(lvs)
        ans <- ans + ll * if1
        lvs <- paste(rep(l, each = ll), rep(lvs, length(l)), sep = sep)
      } else {
        ans <- ans * length(l) + if1
        lvs <- paste(rep(l, length(lvs)), rep(lvs, each = length(l)), sep = sep)
      }
      if(drop) {
        olvs <- lvs
        if(arrange) {
          lvs <- lvs[sort(unique(ans + 1L))]
        } else {
          lvs <- lvs[unique(ans + 1L)]
        }
        ans <- match(olvs[ans + 1L], lvs) - 1L
      }
    }
  }

  # return
  structure(as.integer(ans + 1L), levels = lvs, class = "factor")
}
