#' @export
#' @rdname filter
filter_ <- function(data, x, frame = NULL, env = parent.frame()) {
  UseMethod("filter_")
}

#' @export
#' @rdname filter
filter_.data.frame <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  if(is.numeric(x) || is.logical(x)) {
    return(filter2(data, x))
  }
  check_character(x, check_missing = TRUE)
  check_frame(data, frame)

  #-----------------------------------------------------------------------------
  # evaluate expressions and create rows to keep.
  #-----------------------------------------------------------------------------
  rows <- withCallingHandlers(
    eval(
      expr = parse(
        file = stdin(),
        n = NULL,
        text = x,
        prompt = "?",
        keep.source = FALSE,
        srcfile = NULL,
        encoding = "unknown"
      ),
      envir = if(is.null(frame)) {data} else {data[, frame, drop = FALSE]},
      enclos = env
    ),
    error = function(e) stop(
      "Check the 'x' argument in function bkdat::filter(). An error occured while evaluating the expression '",
      paste0(x),
      "'. You might have used non-existing column names or wrongly-formed functions.",
      call. = FALSE
    )
  )

  #-----------------------------------------------------------------------------
  # Check rows for validity.
  #-----------------------------------------------------------------------------
  # rows are valid if logical length one, logical length nrow(data), numeric and
  # less than length data, and must be of class numeric, integer, or logical.
  ok_rows <- function(data, rows) {
    # need to check for valid row type before comparisons.
    if(!inherits(rows, c("numeric", "integer", "logical"))) {
      return(FALSE)
    }
    data_n_rows <- nrow_df(data)
    rows_length <- length(rows)
    (is.logical(rows) && (rows_length == data_n_rows || rows_length == 1L)) ||
    (is.numeric(rows) && all(rows <= data_n_rows))
  }
  if(!ok_rows(data, rows)) {
    stop(
      "Check the 'x' argument in function bkdat::filter(). The argument x = '",
      paste0(x),
      "'. Needs to return one of the following: 1. logical vector of length one, 2. logical vector of length nrow(data), or 3. Numeric vector with values in range of nrow(data). Try evaluating this manually to check what is wrong."
    )
  }
  # Set NAs to FALSE from a logical evaluation.
  # also handle case where rows is length 1
  if(is.logical(rows)) {
    rows[is.na(rows)] <- FALSE
    if(length(rows) == 1) {
      rows <- rep(rows, nrow_df(data))
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data[rows, , drop = FALSE]
}

#' @export
#' @rdname filter
filter_.group_df <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  if(is.numeric(x) || is.logical(x)) {
    return(filter2(data, x))
  }
  check_character(x, check_missing = TRUE)
  check_frame(data, frame)

  #-----------------------------------------------------------------------------
  # Evaluate expressions and create rows/groups to keep.
  # Need to evaluate on a group by group basis for the case that 'x' is a
  # logical of length one. This lets you filter based on groups.
  #-----------------------------------------------------------------------------
  rows <- vector(mode = "list", length = nrow_df(attr(data, "group_labels")))
  idx <- attr(data, "group_indices")
  for(j in seq_along(idx)) {
    rows[[j]] <- withCallingHandlers(
      eval(
        expr = parse(
          file = stdin(),
          n = NULL,
          text = x,
          prompt = "?",
          keep.source = FALSE,
          srcfile = NULL,
          encoding = "unknown"
        ),
        envir = if(is.null(frame)) {data[idx[[j]], ]} else {data[idx[[j]], frame, drop = FALSE]},
        enclos = env
      ),
      error = function(e) stop(
        "Check the 'x' argument in function bkdat::filter(). An error occured while evaluating the expression '",
        paste0(x),
        "'. You might have used non-existing column names or wrongly-formed functions.",
        call. = FALSE
      )
    )
  }

  #-----------------------------------------------------------------------------
  # Create a vector of data frame row positions to keep for the filter()
  # function
  # There are three row evaluation cases to consider:
  #     1. logical and length 1
  #     2. logical and length nrow(data)
  #     2. integer and <= length nrow(data)
  #-----------------------------------------------------------------------------
  keep_rows <- function(rows) {
    # row type and length (could be logical, integer, numeric, or NULL)
    row_type <- typeof(rows[[1]])
    if(is.null(row_type)) {
      return(NULL)
    }
    row_len_vec <- vapply(
      rows,
      FUN = length,
      FUN.VALUE = integer(1),
      USE.NAMES = FALSE
    )

    # handle the logical case
    if(row_type == "logical") {
      # fix this; all() is fragile for logical returns
      if(all(row_len_vec == 1)) {
        for(i in seq_along(rows)) {
          rows[[i]] <- rep(rows[[i]], length(idx[[i]]))
        }
      }
      rows <- sort.int(unlist(idx)[unlist(rows)])
      return(rows)
    }

    # handle the integer case
    if(row_type %in% c("integer", "numeric")) {
      for(i in seq_along(rows)) {
        rows[[i]] <- idx[[i]][rows[[i]]]
      }
      rows <- sort.int(unlist(rows))
      return(rows)
    }
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data[keep_rows(rows), , drop = FALSE]
}

#' @export
#' @rdname filter
filter_.nest_df <- function(data, x, frame = NULL, env = parent.frame()) {
  #-----------------------------------------------------------------------------
  # apply filter.data.frame() to each list-column element.
  #-----------------------------------------------------------------------------
  if(is.numeric(x) || is.logical(x)) {
    return(filter2(data, x))
  }
  nest_name <- attr(data, "nest_name")
  data[[nest_name]] <- lapply(data[[nest_name]], filter_, x = x, frame = frame, env = env)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}

#-------------------------------------------------------------------------------
# filter2
#-------------------------------------------------------------------------------
filter2 <- function(data, x) {
  if(is.numeric(x)) {
    are_bad <- abs(x) > nrow_df(data)
    if(any(are_bad)) {
      bad_x <- x[are_bad]
      stop(
        "Check the 'x' argument in function bkdat::filter_(). 'data' only has ",
        nrow(data),
        " rows, but 'x' is trying to filter row positions ",
        pretty_print(bad_x),
        "."
      )
    }
  } else if(is.logical(x)){
    x_length <- length(x)
    is_bad <- x_length != nrow_df(data) && x_length != 1
    if(is_bad) {
      stop(
        "Check the 'x' argument in function bkdat::filter_(). 'data' has ",
        nrow(data),
        " rows, but 'x' is a logical vector of length ",
        length(x),
        ". 'x' needs to be the same length as the number of rows in 'data'."
      )
    }
  } else {
    stop(
      "Check the 'x' argument in bkdat::filter(). 'x' has class ",
      pretty_print(class(x)),
      " but needs to be an integer or logical vector."
    )
  }

  #-----------------------------------------------------------------------------
  # Return data
  #-----------------------------------------------------------------------------
  data[x, , drop = FALSE]
}
