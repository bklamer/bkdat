---
output:
  md_document:
    variant: markdown_github
editor_options: 
  chunk_output_type: console
---
<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
options(
  width = 88,
  scipen = 4
)
```

# bkdat

Base R functions that provide a grammar of data manipulation similar to the functions found in the tidyverse. Think of it as the namespace of dplyr, but the internal code of plyr. 

## Installation

See the code at <https://bitbucket.org/bklamer/bkdat>.

```{r install, eval = FALSE}
devtools::install_bitbucket("bklamer/bkdat")
```

## Examples

### arrange() - ordering rows of a data frame
```{r arrange-example}
library(bkdat)
arrange_(data = mtcars, x = "cyl")
```

### filter() - subsetting rows of a data frame
```{r filter-example}
filter(.data = mtcars, cyl == 4)
```

### select() - subsetting columns of a data frame
```{r select-example}
select(data = mtcars, x = c("mpg", "cyl"))
```

### mutate() - adding new variables to a data frame
```{r mutate-example}
mutate(.data = mtcars, wt_pounds = wt*1000)
```

### summarise() - grouped summaries of a data frame
```{r summarise-example}
mtcars |>
  group_by(group_cols = "cyl") |>
  summarise(mean_hp = mean(hp))
```

## Functions

The following functions are implemented:

| Tidyverse function               | bkdat function     |
|:---------------------------------|:-------------------|
| tibble::add_column()             | add_column()       |
| tibble::add_row()                | add_row()          |
| dplyr::arrange()                 | arrange()          |
| NA                               | as_df()            |
| dplyr::bind_cols()               | bind_cols()        |
| dplyr::bind_rows()               | bind_fill_rows()   |
| dplyr::bind_rows()               | bind_rows()        |
| dplyr::desc()                    | desc()             |
| dplyr::do()                      | do()               |
| dplyr::slice() & dplyr::filter() | filter()           |
| tidyr::gather()                  | gather()           |
| dplyr::group_by()                | group_by()         |
| rlang::is_atomic()               | is_atomic()        |
| NA                               | is_date()          |
| rlang::is_integer()              | is_integer()       |
| rlang::is_string()               | is_string()        |
| dplyr::full_join()               | join_full()        |
| dplyr::inner_join()              | join_inner()       |
| dplyr::left_join()               | join_left()        |
| dplyr::right_join()              | join_right()       |
| purrr::map()                     | map()              |
| purrr::map_chr()                 | map_chr()          |
| purrr::map_dbl()                 | map_dbl()          |
| purrr::map_df()                  | map_df()           |
| purrr::map_int()                 | map_int()          |
| dplyr::mutate()                  | mutate()           |
| dplyr::nest_by()                 | nest_by()          |
| dplyr::pull()                    | pull()             |
| dplyr::rename()                  | rename()           |
| tibble::remove_rownames()        | reset_row_names()  |
| tibble::rownames_to_column()     | ronames_to_column()|
| dplyr::select()                  | select()           |
| tidyr::separate()                | separate()         |
| tidyr::spread()                  | spread()           |
| dplyr::summarise()               | summarise()        |
| tidyr::unite()                   | unite()            |

Changes were made to unify the naming scheme and remove base R naming conflicts:

- The dplyr join functions are renamed to `join_*()` instead of `*_join()`. 
- `bkdat::filter()` provides the same functionality as the combination of 
`dplyr::filter()` and `dplyr::slice()`.
