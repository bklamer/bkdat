## Release summary

This is the first release to CRAN.

## Test environments

- local ubuntu 18.04, R 3.5.1
- local Windows 10, R 3.5.1

## R CMD check results

There were no ERRORs, WARNINGs, or NOTEs.

## Downstream dependencies

There are currently no downstream dependencies for this package
